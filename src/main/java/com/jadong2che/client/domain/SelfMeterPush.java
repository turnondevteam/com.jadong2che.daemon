package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;


public class SelfMeterPush extends Common {
    private int selfmeterPushIndex;

    private int companyIndex;

    private String companyName;

    private int userIndex;

    private String managementTitle;

    private String sendTitle;

    private String sendContents;

    private String requestStatus;

    private String reason;

    private String sendDatetime;

    private String registId;

    private String registDatetime;

    private String successYn;

    private String appToken;

    private String userId;

    private String userName;

    private String email;

    private String userNumber;

    private String searchGbn;

    private String searchWord;

    private String userIndexArray;

    private String sendDate;

    private String sendTime;

    private String sendMinute;

    private String pushType;

    private String pushDetailType;

    private String sendReservationDatetime;

    private int userCount;

    private int pushGroupIndex;

    private String pushGroupName;

    private int groupUserIndex;

    private String billStatus;

    private int goodsIndex;

    private String cellphone;

    private String goodsName;

    private int count;

    private String description;

    private String selfmeterStartDatetime;

    private String selfmeterEndDatetime;

    private int selfmeterDetailIndex;

    public int getSelfmeterPushIndex() {
        return selfmeterPushIndex;
    }

    public void setSelfmeterPushIndex(int selfmeterPushIndex) {
        this.selfmeterPushIndex = selfmeterPushIndex;
    }

    public int getCompanyIndex() {
        return companyIndex;
    }

    public void setCompanyIndex(int companyIndex) {
        this.companyIndex = companyIndex;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getUserIndex() {
        return userIndex;
    }

    public void setUserIndex(int userIndex) {
        this.userIndex = userIndex;
    }

    public String getManagementTitle() {
        return managementTitle;
    }

    public void setManagementTitle(String managementTitle) {
        this.managementTitle = managementTitle;
    }

    public String getSendTitle() {
        return sendTitle;
    }

    public void setSendTitle(String sendTitle) {
        this.sendTitle = sendTitle;
    }

    public String getSendContents() {
        return sendContents;
    }

    public void setSendContents(String sendContents) {
        this.sendContents = sendContents;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getSendDatetime() {
        return sendDatetime;
    }

    public void setSendDatetime(String sendDatetime) {
        this.sendDatetime = sendDatetime;
    }

    public String getRegistId() {
        return registId;
    }

    public void setRegistId(String registId) {
        this.registId = registId;
    }

    public String getRegistDatetime() {
        return registDatetime;
    }

    public void setRegistDatetime(String registDatetime) {
        this.registDatetime = registDatetime;
    }

    public String getSuccessYn() {
        return successYn;
    }

    public void setSuccessYn(String successYn) {
        this.successYn = successYn;
    }

    public String getAppToken() {
        return appToken;
    }

    public void setAppToken(String appToken) {
        this.appToken = appToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getSearchGbn() {
        return searchGbn;
    }

    public void setSearchGbn(String searchGbn) {
        this.searchGbn = searchGbn;
    }

    public String getSearchWord() {
        return searchWord;
    }

    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }

    public String getUserIndexArray() {
        return userIndexArray;
    }

    public void setUserIndexArray(String userIndexArray) {
        this.userIndexArray = userIndexArray;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getSendMinute() {
        return sendMinute;
    }

    public void setSendMinute(String sendMinute) {
        this.sendMinute = sendMinute;
    }

    public String getPushType() {
        return pushType;
    }

    public void setPushType(String pushType) {
        this.pushType = pushType;
    }

    public String getPushDetailType() {
        return pushDetailType;
    }

    public void setPushDetailType(String pushDetailType) {
        this.pushDetailType = pushDetailType;
    }

    public String getSendReservationDatetime() {
        return sendReservationDatetime;
    }

    public void setSendReservationDatetime(String sendReservationDatetime) {
        this.sendReservationDatetime = sendReservationDatetime;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public int getPushGroupIndex() {
        return pushGroupIndex;
    }

    public void setPushGroupIndex(int pushGroupIndex) {
        this.pushGroupIndex = pushGroupIndex;
    }

    public String getPushGroupName() {
        return pushGroupName;
    }

    public void setPushGroupName(String pushGroupName) {
        this.pushGroupName = pushGroupName;
    }

    public int getGroupUserIndex() {
        return groupUserIndex;
    }

    public void setGroupUserIndex(int groupUserIndex) {
        this.groupUserIndex = groupUserIndex;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public int getGoodsIndex() {
        return goodsIndex;
    }

    public void setGoodsIndex(int goodsIndex) {
        this.goodsIndex = goodsIndex;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSelfmeterStartDatetime() {
        return selfmeterStartDatetime;
    }

    public void setSelfmeterStartDatetime(String selfmeterStartDatetime) {
        this.selfmeterStartDatetime = selfmeterStartDatetime;
    }

    public String getSelfmeterEndDatetime() {
        return selfmeterEndDatetime;
    }

    public void setSelfmeterEndDatetime(String selfmeterEndDatetime) {
        this.selfmeterEndDatetime = selfmeterEndDatetime;
    }

    public int getSelfmeterDetailIndex() {
        return selfmeterDetailIndex;
    }

    public void setSelfmeterDetailIndex(int selfmeterDetailIndex) {
        this.selfmeterDetailIndex = selfmeterDetailIndex;
    }
}
