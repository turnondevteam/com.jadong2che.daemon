package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;


public class GoodsReq extends Common {

	private int goodsIndex;

	private int paymentMethodIndex;

	private int requestIndex;

	private int requestQuantity;

	private String requestStatus;

	private String requestDivision;

	private String requestDatetime;

	private String closeDatetime;

	private String paymentDate;

	private int deliveryAddressIndex;

	private String userName;

	private String regionCode;

	private String regionName;

	private String cellphone;

	private int amount;

	private String statusChangeDatetime;

	private int userNumber;

	private String email;

	private String birthday;

	private String billRequestYn;

	private int companyIndex;

	private String goodsName;

	private String paymentDatetime;

	private String registDatetime;

	private String billStatus;

	private String accountNumber;

	private String modifyDatetime;

	private String startDate;

	private String endDate;

	private int userIndex;

	private String paymentMethodName;

	private String billName;

	private String userId;

	private int chargeCost;

	private String chargeDatetime;

	private String companyName;

	private String joinDatetime;

	private int userInPaymentMethodIndex;

	private String fileName;

	private String filePath;

	private String fileRealName;

	private String closeStatus;

	private String startDatetime;

	private String endDatetime;

	private String requestConfirmImgPath;

	private int requestTempIndex;

	private String requestTempGroup;

	private String recvName;

	private String recvCustomerNumber;

	private String recvCustomerBirthday;

	private String address;

	private String option;

	private int goodsRequestHistoryIndex;

	private int billIndex;

	private String acountNumber;

	private int requestInfoIndex;

	private String companyJoinDatetime;

	private int discountCost;

	private int cost;

	private String pushRecvYn;

	private String emailRecvYn;

	private String smsRecvYn;

	private String leaveDatetime;

	private String financialCompanyName;

	private String subscriberRelationship;

	private String optionDivision;

	private String optionContents;

	private String oldContents;

	private String newContents;

	private String status;

	private String code;

	private String codeName;

	private int changeHistoryIndex;

	private String changeName;

	private String companyGoodsCode;

	private String statusName;

	private Integer requestTempSaveIndex;

	private int userInPaymentMethodDetailIndex;

	private String financialCompanyCode;

	public int getGoodsIndex() {
		return goodsIndex;
	}

	public void setGoodsIndex(int goodsIndex) {
		this.goodsIndex = goodsIndex;
	}

	public int getPaymentMethodIndex() {
		return paymentMethodIndex;
	}

	public void setPaymentMethodIndex(int paymentMethodIndex) {
		this.paymentMethodIndex = paymentMethodIndex;
	}

	public int getRequestIndex() {
		return requestIndex;
	}

	public void setRequestIndex(int requestIndex) {
		this.requestIndex = requestIndex;
	}

	public int getRequestQuantity() {
		return requestQuantity;
	}

	public void setRequestQuantity(int requestQuantity) {
		this.requestQuantity = requestQuantity;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getRequestDivision() {
		return requestDivision;
	}

	public void setRequestDivision(String requestDivision) {
		this.requestDivision = requestDivision;
	}

	public String getRequestDatetime() {
		return requestDatetime;
	}

	public void setRequestDatetime(String requestDatetime) {
		this.requestDatetime = requestDatetime;
	}

	public String getCloseDatetime() {
		return closeDatetime;
	}

	public void setCloseDatetime(String closeDatetime) {
		this.closeDatetime = closeDatetime;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public int getDeliveryAddressIndex() {
		return deliveryAddressIndex;
	}

	public void setDeliveryAddressIndex(int deliveryAddressIndex) {
		this.deliveryAddressIndex = deliveryAddressIndex;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getStatusChangeDatetime() {
		return statusChangeDatetime;
	}

	public void setStatusChangeDatetime(String statusChangeDatetime) {
		this.statusChangeDatetime = statusChangeDatetime;
	}

	public int getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(int userNumber) {
		this.userNumber = userNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getBillRequestYn() {
		return billRequestYn;
	}

	public void setBillRequestYn(String billRequestYn) {
		this.billRequestYn = billRequestYn;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getPaymentDatetime() {
		return paymentDatetime;
	}

	public void setPaymentDatetime(String paymentDatetime) {
		this.paymentDatetime = paymentDatetime;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(String modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getUserIndex() {
		return userIndex;
	}

	public void setUserIndex(int userIndex) {
		this.userIndex = userIndex;
	}

	public String getPaymentMethodName() {
		return paymentMethodName;
	}

	public void setPaymentMethodName(String paymentMethodName) {
		this.paymentMethodName = paymentMethodName;
	}

	public String getBillName() {
		return billName;
	}

	public void setBillName(String billName) {
		this.billName = billName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getChargeCost() {
		return chargeCost;
	}

	public void setChargeCost(int chargeCost) {
		this.chargeCost = chargeCost;
	}

	public String getChargeDatetime() {
		return chargeDatetime;
	}

	public void setChargeDatetime(String chargeDatetime) {
		this.chargeDatetime = chargeDatetime;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getJoinDatetime() {
		return joinDatetime;
	}

	public void setJoinDatetime(String joinDatetime) {
		this.joinDatetime = joinDatetime;
	}

	public int getUserInPaymentMethodIndex() {
		return userInPaymentMethodIndex;
	}

	public void setUserInPaymentMethodIndex(int userInPaymentMethodIndex) {
		this.userInPaymentMethodIndex = userInPaymentMethodIndex;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileRealName() {
		return fileRealName;
	}

	public void setFileRealName(String fileRealName) {
		this.fileRealName = fileRealName;
	}

	public String getCloseStatus() {
		return closeStatus;
	}

	public void setCloseStatus(String closeStatus) {
		this.closeStatus = closeStatus;
	}

	public String getStartDatetime() {
		return startDatetime;
	}

	public void setStartDatetime(String startDatetime) {
		this.startDatetime = startDatetime;
	}

	public String getEndDatetime() {
		return endDatetime;
	}

	public void setEndDatetime(String endDatetime) {
		this.endDatetime = endDatetime;
	}

	public String getRequestConfirmImgPath() {
		return requestConfirmImgPath;
	}

	public void setRequestConfirmImgPath(String requestConfirmImgPath) {
		this.requestConfirmImgPath = requestConfirmImgPath;
	}

	public int getRequestTempIndex() {
		return requestTempIndex;
	}

	public void setRequestTempIndex(int requestTempIndex) {
		this.requestTempIndex = requestTempIndex;
	}

	public String getRequestTempGroup() {
		return requestTempGroup;
	}

	public void setRequestTempGroup(String requestTempGroup) {
		this.requestTempGroup = requestTempGroup;
	}

	public String getRecvName() {
		return recvName;
	}

	public void setRecvName(String recvName) {
		this.recvName = recvName;
	}

	public String getRecvCustomerNumber() {
		return recvCustomerNumber;
	}

	public void setRecvCustomerNumber(String recvCustomerNumber) {
		this.recvCustomerNumber = recvCustomerNumber;
	}

	public String getRecvCustomerBirthday() {
		return recvCustomerBirthday;
	}

	public void setRecvCustomerBirthday(String recvCustomerBirthday) {
		this.recvCustomerBirthday = recvCustomerBirthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public int getGoodsRequestHistoryIndex() {
		return goodsRequestHistoryIndex;
	}

	public void setGoodsRequestHistoryIndex(int goodsRequestHistoryIndex) {
		this.goodsRequestHistoryIndex = goodsRequestHistoryIndex;
	}

	public int getBillIndex() {
		return billIndex;
	}

	public void setBillIndex(int billIndex) {
		this.billIndex = billIndex;
	}

	public String getAcountNumber() {
		return acountNumber;
	}

	public void setAcountNumber(String acountNumber) {
		this.acountNumber = acountNumber;
	}

	public int getRequestInfoIndex() {
		return requestInfoIndex;
	}

	public void setRequestInfoIndex(int requestInfoIndex) {
		this.requestInfoIndex = requestInfoIndex;
	}

	public String getCompanyJoinDatetime() {
		return companyJoinDatetime;
	}

	public void setCompanyJoinDatetime(String companyJoinDatetime) {
		this.companyJoinDatetime = companyJoinDatetime;
	}

	public int getDiscountCost() {
		return discountCost;
	}

	public void setDiscountCost(int discountCost) {
		this.discountCost = discountCost;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getPushRecvYn() {
		return pushRecvYn;
	}

	public void setPushRecvYn(String pushRecvYn) {
		this.pushRecvYn = pushRecvYn;
	}

	public String getEmailRecvYn() {
		return emailRecvYn;
	}

	public void setEmailRecvYn(String emailRecvYn) {
		this.emailRecvYn = emailRecvYn;
	}

	public String getSmsRecvYn() {
		return smsRecvYn;
	}

	public void setSmsRecvYn(String smsRecvYn) {
		this.smsRecvYn = smsRecvYn;
	}

	public String getLeaveDatetime() {
		return leaveDatetime;
	}

	public void setLeaveDatetime(String leaveDatetime) {
		this.leaveDatetime = leaveDatetime;
	}

	public String getFinancialCompanyName() {
		return financialCompanyName;
	}

	public void setFinancialCompanyName(String financialCompanyName) {
		this.financialCompanyName = financialCompanyName;
	}

	public String getSubscriberRelationship() {
		return subscriberRelationship;
	}

	public void setSubscriberRelationship(String subscriberRelationship) {
		this.subscriberRelationship = subscriberRelationship;
	}

	public String getOptionDivision() {
		return optionDivision;
	}

	public void setOptionDivision(String optionDivision) {
		this.optionDivision = optionDivision;
	}

	public String getOptionContents() {
		return optionContents;
	}

	public void setOptionContents(String optionContents) {
		this.optionContents = optionContents;
	}

	public String getOldContents() {
		return oldContents;
	}

	public void setOldContents(String oldContents) {
		this.oldContents = oldContents;
	}

	public String getNewContents() {
		return newContents;
	}

	public void setNewContents(String newContents) {
		this.newContents = newContents;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public int getChangeHistoryIndex() {
		return changeHistoryIndex;
	}

	public void setChangeHistoryIndex(int changeHistoryIndex) {
		this.changeHistoryIndex = changeHistoryIndex;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getCompanyGoodsCode() {
		return companyGoodsCode;
	}

	public void setCompanyGoodsCode(String companyGoodsCode) {
		this.companyGoodsCode = companyGoodsCode;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Integer getRequestTempSaveIndex() {
		return requestTempSaveIndex;
	}

	public void setRequestTempSaveIndex(Integer requestTempSaveIndex) {
		this.requestTempSaveIndex = requestTempSaveIndex;
	}

	public int getUserInPaymentMethodDetailIndex() {
		return userInPaymentMethodDetailIndex;
	}

	public void setUserInPaymentMethodDetailIndex(int userInPaymentMethodDetailIndex) {
		this.userInPaymentMethodDetailIndex = userInPaymentMethodDetailIndex;
	}

	public String getFinancialCompanyCode() {
		return financialCompanyCode;
	}

	public void setFinancialCompanyCode(String financialCompanyCode) {
		this.financialCompanyCode = financialCompanyCode;
	}
}
