package com.jadong2che.client.domain;



public class CsAgent {

	// qna 일련번호
	private int csAgentIndex;
	
	// 고객사 일련번호
	private int companyIndex;
	
	// 문의구분
	private String division;
	
	// 제목
	private String title;
	
	// 내용
	private String contents;
	
	// 파일경로
	private String filePath;
	
	// 상태
	private String status;

	// 등록일
	private String registDatetime;
	
	// 등록id
	private String registId;
	
	// 답변등록일
	private String answerRegistDatetime;
	
	// 이름
	private String name;
	
	// 핸드폰
	private String cellphone;

	private int count;

	private int csAgentDetailIndex;

	private String tel;

	public int getCsAgentIndex() {
		return csAgentIndex;
	}

	public void setCsAgentIndex(int csAgentIndex) {
		this.csAgentIndex = csAgentIndex;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public String getRegistId() {
		return registId;
	}

	public void setRegistId(String registId) {
		this.registId = registId;
	}

	public String getAnswerRegistDatetime() {
		return answerRegistDatetime;
	}

	public void setAnswerRegistDatetime(String answerRegistDatetime) {
		this.answerRegistDatetime = answerRegistDatetime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCsAgentDetailIndex() {
		return csAgentDetailIndex;
	}

	public void setCsAgentDetailIndex(int csAgentDetailIndex) {
		this.csAgentDetailIndex = csAgentDetailIndex;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
}

