package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;


public class SelfMeter extends Common {

	// 사용자 일련번호
	private int userIndex;
	 
	private int companyIndex;

	private int userNumber;

	private String userName;

	private String address;

	private String meterNumber;

	private int selfmeterCount;

	private int selfmeterNumber;

	private String selfmeterDatetime;

	private int selfmeterIndex;

	private String customerNumber;

	private String contractNumber;

	private String possibilityStartDate;

	private String possibilityEndDate;

	private String registDatetime;

	private String usage;

	private String monthly;

	private int selfmeterDetailTempIndex;

	private String selfmeterTempGroup;

	private String subscriberRelationship;

	private String lastmonthSelfmeterCount;

	private String usePeriodStartDate;

	private String usePeriodEndDate;

	private int singleSellPrice;

	public int getUserIndex() {
		return userIndex;
	}

	public void setUserIndex(int userIndex) {
		this.userIndex = userIndex;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public int getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(int userNumber) {
		this.userNumber = userNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMeterNumber() {
		return meterNumber;
	}

	public void setMeterNumber(String meterNumber) {
		this.meterNumber = meterNumber;
	}

	public int getSelfmeterCount() {
		return selfmeterCount;
	}

	public void setSelfmeterCount(int selfmeterCount) {
		this.selfmeterCount = selfmeterCount;
	}

	public int getSelfmeterNumber() {
		return selfmeterNumber;
	}

	public void setSelfmeterNumber(int selfmeterNumber) {
		this.selfmeterNumber = selfmeterNumber;
	}

	public String getSelfmeterDatetime() {
		return selfmeterDatetime;
	}

	public void setSelfmeterDatetime(String selfmeterDatetime) {
		this.selfmeterDatetime = selfmeterDatetime;
	}

	public int getSelfmeterIndex() {
		return selfmeterIndex;
	}

	public void setSelfmeterIndex(int selfmeterIndex) {
		this.selfmeterIndex = selfmeterIndex;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getPossibilityStartDate() {
		return possibilityStartDate;
	}

	public void setPossibilityStartDate(String possibilityStartDate) {
		this.possibilityStartDate = possibilityStartDate;
	}

	public String getPossibilityEndDate() {
		return possibilityEndDate;
	}

	public void setPossibilityEndDate(String possibilityEndDate) {
		this.possibilityEndDate = possibilityEndDate;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public String getMonthly() {
		return monthly;
	}

	public void setMonthly(String monthly) {
		this.monthly = monthly;
	}

	public int getSelfmeterDetailTempIndex() {
		return selfmeterDetailTempIndex;
	}

	public void setSelfmeterDetailTempIndex(int selfmeterDetailTempIndex) {
		this.selfmeterDetailTempIndex = selfmeterDetailTempIndex;
	}

	public String getSelfmeterTempGroup() {
		return selfmeterTempGroup;
	}

	public void setSelfmeterTempGroup(String selfmeterTempGroup) {
		this.selfmeterTempGroup = selfmeterTempGroup;
	}

	public String getSubscriberRelationship() {
		return subscriberRelationship;
	}

	public void setSubscriberRelationship(String subscriberRelationship) {
		this.subscriberRelationship = subscriberRelationship;
	}

	public String getLastmonthSelfmeterCount() {
		return lastmonthSelfmeterCount;
	}

	public void setLastmonthSelfmeterCount(String lastmonthSelfmeterCount) {
		this.lastmonthSelfmeterCount = lastmonthSelfmeterCount;
	}

	public String getUsePeriodStartDate() {
		return usePeriodStartDate;
	}

	public void setUsePeriodStartDate(String usePeriodStartDate) {
		this.usePeriodStartDate = usePeriodStartDate;
	}

	public String getUsePeriodEndDate() {
		return usePeriodEndDate;
	}

	public void setUsePeriodEndDate(String usePeriodEndDate) {
		this.usePeriodEndDate = usePeriodEndDate;
	}

	public int getSingleSellPrice() {
		return singleSellPrice;
	}

	public void setSingleSellPrice(int singleSellPrice) {
		this.singleSellPrice = singleSellPrice;
	}
}
