package com.jadong2che.client.domain;



public class CompanyAdditional {

	private String deliveryText;

	private String userScreenText;

	private String userScreenImgPath;

	private String companyImgPath;

	private String companyThumbnailPath;

	private String termsRequest1;

	private String termsRequest2;

	private String termsRequest3;

	public String getDeliveryText() {
		return deliveryText;
	}

	public void setDeliveryText(String deliveryText) {
		this.deliveryText = deliveryText;
	}

	public String getUserScreenText() {
		return userScreenText;
	}

	public void setUserScreenText(String userScreenText) {
		this.userScreenText = userScreenText;
	}

	public String getUserScreenImgPath() {
		return userScreenImgPath;
	}

	public void setUserScreenImgPath(String userScreenImgPath) {
		this.userScreenImgPath = userScreenImgPath;
	}

	public String getCompanyImgPath() {
		return companyImgPath;
	}

	public void setCompanyImgPath(String companyImgPath) {
		this.companyImgPath = companyImgPath;
	}

	public String getCompanyThumbnailPath() {
		return companyThumbnailPath;
	}

	public void setCompanyThumbnailPath(String companyThumbnailPath) {
		this.companyThumbnailPath = companyThumbnailPath;
	}

	public String getTermsRequest1() {
		return termsRequest1;
	}

	public void setTermsRequest1(String termsRequest1) {
		this.termsRequest1 = termsRequest1;
	}

	public String getTermsRequest2() {
		return termsRequest2;
	}

	public void setTermsRequest2(String termsRequest2) {
		this.termsRequest2 = termsRequest2;
	}

	public String getTermsRequest3() {
		return termsRequest3;
	}

	public void setTermsRequest3(String termsRequest3) {
		this.termsRequest3 = termsRequest3;
	}
}
