package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;


public class BillGroup extends Common {

    private int billGroupIndex;

    private String billGroupCode;

    private int goods_index;

    private String costOptionContents;

    private int cost;

    private String paymentDatetime;

    private String period;

    private String issueDate;

    private String companyName;

    private String goodsName;

    private int count;

    private String companyGoodsCode;

    private String userName;

    private String userNumber;

    private String cellphone;

    private int requestInfoIndex;

    private int billGroupOptionIndex;

    private String title;

    private String contents;

    public int getBillGroupIndex() {
        return billGroupIndex;
    }

    public void setBillGroupIndex(int billGroupIndex) {
        this.billGroupIndex = billGroupIndex;
    }

    public String getBillGroupCode() {
        return billGroupCode;
    }

    public void setBillGroupCode(String billGroupCode) {
        this.billGroupCode = billGroupCode;
    }

    public int getGoods_index() {
        return goods_index;
    }

    public void setGoods_index(int goods_index) {
        this.goods_index = goods_index;
    }

    public String getCostOptionContents() {
        return costOptionContents;
    }

    public void setCostOptionContents(String costOptionContents) {
        this.costOptionContents = costOptionContents;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getPaymentDatetime() {
        return paymentDatetime;
    }

    public void setPaymentDatetime(String paymentDatetime) {
        this.paymentDatetime = paymentDatetime;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getCompanyGoodsCode() {
        return companyGoodsCode;
    }

    public void setCompanyGoodsCode(String companyGoodsCode) {
        this.companyGoodsCode = companyGoodsCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public int getRequestInfoIndex() {
        return requestInfoIndex;
    }

    public void setRequestInfoIndex(int requestInfoIndex) {
        this.requestInfoIndex = requestInfoIndex;
    }

    public int getBillGroupOptionIndex() {
        return billGroupOptionIndex;
    }

    public void setBillGroupOptionIndex(int billGroupOptionIndex) {
        this.billGroupOptionIndex = billGroupOptionIndex;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }
}