package com.jadong2che.client.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResultDataVO {

    //TODO 공통 메시지 부분 정해서 어떻게 사용할지 수정.
    public static final String SUCCESS_TYPE = "success";
    public static final String ERROR_TYPE = "error";
    public static final String EXIST_TYPE = "error-exist";

    String status;
    String message;
    Object data;
    List<?> list;

    public ResultDataVO() {
        this("success", "success");
    }

    public ResultDataVO(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public ResultDataVO(Object data) {
        this.data = data;
    }

    public ResultDataVO(List list) {
        this.list = list;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }
}
