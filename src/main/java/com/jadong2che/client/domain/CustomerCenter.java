package com.jadong2che.client.domain;



public class CustomerCenter {


	private Integer messageIndex;

	private Integer userIndex;

	private String title;

	private String message;

	private Integer companyIndex;

	private Integer goodsIndex;
	
	private Integer billIndex;

	private Integer billMonthlyIndex;

	private String registDatetime;
	
	private String messageCode;

	private Integer contentsIndex;
	
	private String thumbnailPath;

	private String companyName;

	private String userName;

	private String cellphone;

	private String registId;

	private String url;

	private String user;

	public Integer getMessageIndex() {
		return messageIndex;
	}

	public void setMessageIndex(Integer messageIndex) {
		this.messageIndex = messageIndex;
	}

	public Integer getUserIndex() {
		return userIndex;
	}

	public void setUserIndex(Integer userIndex) {
		this.userIndex = userIndex;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(Integer companyIndex) {
		this.companyIndex = companyIndex;
	}

	public Integer getGoodsIndex() {
		return goodsIndex;
	}

	public void setGoodsIndex(Integer goodsIndex) {
		this.goodsIndex = goodsIndex;
	}

	public Integer getBillIndex() {
		return billIndex;
	}

	public void setBillIndex(Integer billIndex) {
		this.billIndex = billIndex;
	}

	public Integer getBillMonthlyIndex() {
		return billMonthlyIndex;
	}

	public void setBillMonthlyIndex(Integer billMonthlyIndex) {
		this.billMonthlyIndex = billMonthlyIndex;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public Integer getContentsIndex() {
		return contentsIndex;
	}

	public void setContentsIndex(Integer contentsIndex) {
		this.contentsIndex = contentsIndex;
	}

	public String getThumbnailPath() {
		return thumbnailPath;
	}

	public void setThumbnailPath(String thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
	}


	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getUserName() {
		return userName;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getRegistId() {
		return registId;
	}

	public void setRegistId(String registId) {
		this.registId = registId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
}

