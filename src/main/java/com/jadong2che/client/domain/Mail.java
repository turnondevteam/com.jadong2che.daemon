package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;


public class Mail extends Common {

    private int memberIndex;

    private int companyIndex;

    private String email;

    public int getCompanyIndex() {
        return companyIndex;
    }

    public void setCompanyIndex(int companyIndex) {
        this.companyIndex = companyIndex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
