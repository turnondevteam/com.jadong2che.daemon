package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CompanyBillCategory extends Common {

    private Integer companyBillCategoryIndex;

    private Integer companyIndex;

    private String categoryName;

}