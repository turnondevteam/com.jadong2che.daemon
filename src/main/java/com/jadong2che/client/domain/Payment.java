package com.jadong2che.client.domain;


import com.jadong2che.common.domain.Common;


public class Payment extends Common {

	private int userIndex;
	
	private int userNumber;
	
	private String userName;
	
	private String userId;
	
	private int payCost;
	
	private int payIndex;

	private String payYn;
	
	private int requestIndex;
	
	private String email;
	
	private String birthday;
	
	private String cellphone;
	
	private int regionCode;
	
	private int goodsIndex;
	
	private String startDate;
	
	private String endDate;
	
	private int companyIndex;

	private String billName;

	private String goodsName;

	private int chargeCost;

	private String chargeDatetime;

	private String billStatus;

	private String payMethod;

	private int cost;

	private int requestQuantity;

	private String paymentDate;

	private String payDatetime;

	private int costSum;

	private int discountCost;

	private int billMonthlyIndex;

	private String payDetailInfo;

	private String paymentMethodName;

	private String financialCompanyName;

	private String approvalNumber;

	private int totalCost;

	private int goodsRequestOptionIndex;

	private String optionContents;

	private String costOptionContents;

	private String paymentDatetimeOptionContents;

	private String memo;

	private String subscriberRelationship;

	private String recvName;

	private int useCost;

	private int basicCost;

	private int cuttingCost;

	private int overdueCost;

	private int VAT;

	private int userQuantity;

	private String jsonData;

	private String status;

	private String address;

	private String startDatetime;

	private String endDatetime;

	private int useQuantity;

	private int billIndex;

	private String charge_datetime;

	private String companyName;

	private int requestInfoIndex;

	private String billSendStatus;

	public int getUserIndex() {
		return userIndex;
	}

	public void setUserIndex(int userIndex) {
		this.userIndex = userIndex;
	}

	public int getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(int userNumber) {
		this.userNumber = userNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getPayCost() {
		return payCost;
	}

	public void setPayCost(int payCost) {
		this.payCost = payCost;
	}

	public int getPayIndex() {
		return payIndex;
	}

	public void setPayIndex(int payIndex) {
		this.payIndex = payIndex;
	}

	public String getPayYn() {
		return payYn;
	}

	public void setPayYn(String payYn) {
		this.payYn = payYn;
	}

	public int getRequestIndex() {
		return requestIndex;
	}

	public void setRequestIndex(int requestIndex) {
		this.requestIndex = requestIndex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public int getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(int regionCode) {
		this.regionCode = regionCode;
	}

	public int getGoodsIndex() {
		return goodsIndex;
	}

	public void setGoodsIndex(int goodsIndex) {
		this.goodsIndex = goodsIndex;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public String getBillName() {
		return billName;
	}

	public void setBillName(String billName) {
		this.billName = billName;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public int getChargeCost() {
		return chargeCost;
	}

	public void setChargeCost(int chargeCost) {
		this.chargeCost = chargeCost;
	}

	public String getChargeDatetime() {
		return chargeDatetime;
	}

	public void setChargeDatetime(String chargeDatetime) {
		this.chargeDatetime = chargeDatetime;
	}

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getRequestQuantity() {
		return requestQuantity;
	}

	public void setRequestQuantity(int requestQuantity) {
		this.requestQuantity = requestQuantity;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPayDatetime() {
		return payDatetime;
	}

	public void setPayDatetime(String payDatetime) {
		this.payDatetime = payDatetime;
	}

	public int getCostSum() {
		return costSum;
	}

	public void setCostSum(int costSum) {
		this.costSum = costSum;
	}

	public int getDiscountCost() {
		return discountCost;
	}

	public void setDiscountCost(int discountCost) {
		this.discountCost = discountCost;
	}

	public int getBillMonthlyIndex() {
		return billMonthlyIndex;
	}

	public void setBillMonthlyIndex(int billMonthlyIndex) {
		this.billMonthlyIndex = billMonthlyIndex;
	}

	public String getPayDetailInfo() {
		return payDetailInfo;
	}

	public void setPayDetailInfo(String payDetailInfo) {
		this.payDetailInfo = payDetailInfo;
	}

	public String getPaymentMethodName() {
		return paymentMethodName;
	}

	public void setPaymentMethodName(String paymentMethodName) {
		this.paymentMethodName = paymentMethodName;
	}

	public String getFinancialCompanyName() {
		return financialCompanyName;
	}

	public void setFinancialCompanyName(String financialCompanyName) {
		this.financialCompanyName = financialCompanyName;
	}

	public String getApprovalNumber() {
		return approvalNumber;
	}

	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}

	public int getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}

	public int getGoodsRequestOptionIndex() {
		return goodsRequestOptionIndex;
	}

	public void setGoodsRequestOptionIndex(int goodsRequestOptionIndex) {
		this.goodsRequestOptionIndex = goodsRequestOptionIndex;
	}

	public String getOptionContents() {
		return optionContents;
	}

	public void setOptionContents(String optionContents) {
		this.optionContents = optionContents;
	}

	public String getCostOptionContents() {
		return costOptionContents;
	}

	public void setCostOptionContents(String costOptionContents) {
		this.costOptionContents = costOptionContents;
	}

	public String getPaymentDatetimeOptionContents() {
		return paymentDatetimeOptionContents;
	}

	public void setPaymentDatetimeOptionContents(String paymentDatetimeOptionContents) {
		this.paymentDatetimeOptionContents = paymentDatetimeOptionContents;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getSubscriberRelationship() {
		return subscriberRelationship;
	}

	public void setSubscriberRelationship(String subscriberRelationship) {
		this.subscriberRelationship = subscriberRelationship;
	}

	public String getRecvName() {
		return recvName;
	}

	public void setRecvName(String recvName) {
		this.recvName = recvName;
	}

	public int getUseCost() {
		return useCost;
	}

	public void setUseCost(int useCost) {
		this.useCost = useCost;
	}

	public int getBasicCost() {
		return basicCost;
	}

	public void setBasicCost(int basicCost) {
		this.basicCost = basicCost;
	}

	public int getCuttingCost() {
		return cuttingCost;
	}

	public void setCuttingCost(int cuttingCost) {
		this.cuttingCost = cuttingCost;
	}

	public int getOverdueCost() {
		return overdueCost;
	}

	public void setOverdueCost(int overdueCost) {
		this.overdueCost = overdueCost;
	}

	public int getVAT() {
		return VAT;
	}

	public void setVAT(int VAT) {
		this.VAT = VAT;
	}

	public int getUserQuantity() {
		return userQuantity;
	}

	public void setUserQuantity(int userQuantity) {
		this.userQuantity = userQuantity;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStartDatetime() {
		return startDatetime;
	}

	public void setStartDatetime(String startDatetime) {
		this.startDatetime = startDatetime;
	}

	public String getEndDatetime() {
		return endDatetime;
	}

	public void setEndDatetime(String endDatetime) {
		this.endDatetime = endDatetime;
	}

	public int getUseQuantity() {
		return useQuantity;
	}

	public void setUseQuantity(int useQuantity) {
		this.useQuantity = useQuantity;
	}

	public int getBillIndex() {
		return billIndex;
	}

	public void setBillIndex(int billIndex) {
		this.billIndex = billIndex;
	}

	public String getCharge_datetime() {
		return charge_datetime;
	}

	public void setCharge_datetime(String charge_datetime) {
		this.charge_datetime = charge_datetime;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getRequestInfoIndex() {
		return requestInfoIndex;
	}

	public void setRequestInfoIndex(int requestInfoIndex) {
		this.requestInfoIndex = requestInfoIndex;
	}

	public String getBillSendStatus() {
		return billSendStatus;
	}

	public void setBillSendStatus(String billSendStatus) {
		this.billSendStatus = billSendStatus;
	}
}
