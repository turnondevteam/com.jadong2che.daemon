package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;


public class Request extends Common {

	private int requestIndex;
	private String registerNumber;
	private String requestStatus;
	private int companyIndex;
	private String requestType;
	private int userIndex;
	private String userNumber;
	private String userName;
	private String cellphone;
	private String bizNumber;
	private int goodsIndex;
	private String goodsName;
	private String goodsDescription;
	private String goodsOption;
	private String postcode;
	private String address;
	private String addressDetail;
	private int cost;
	private String costType;
	private String issueDate;
	private String financialCompanyName;
	private String financialCompanyCode;
	private String paymentMethodType;
	private String paymentMethodNumber;
	private String paymentDatetime;
	private String deliveryDatetime;
	private String requestConfirmImgPath;
	private String requestDatetime;
	private String registId;
	private String registDatetime;
	private String modify_id;
	private String modifyRegistDatetime;
	private Integer billIndex;
	private String requestStatusName;

	private String modifyDatetime;

	public int getRequestIndex() {
		return requestIndex;
	}

	public void setRequestIndex(int requestIndex) {
		this.requestIndex = requestIndex;
	}

	public String getRegisterNumber() {
		return registerNumber;
	}

	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public int getUserIndex() {
		return userIndex;
	}

	public void setUserIndex(int userIndex) {
		this.userIndex = userIndex;
	}

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getBizNumber() {
		return bizNumber;
	}

	public void setBizNumber(String bizNumber) {
		this.bizNumber = bizNumber;
	}

	public int getGoodsIndex() {
		return goodsIndex;
	}

	public void setGoodsIndex(int goodsIndex) {
		this.goodsIndex = goodsIndex;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsDescription() {
		return goodsDescription;
	}

	public void setGoodsDescription(String goodsDescription) {
		this.goodsDescription = goodsDescription;
	}

	public String getGoodsOption() {
		return goodsOption;
	}

	public void setGoodsOption(String goodsOption) {
		this.goodsOption = goodsOption;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressDetail() {
		return addressDetail;
	}

	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getCostType() {
		return costType;
	}

	public void setCostType(String costType) {
		this.costType = costType;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getFinancialCompanyName() {
		return financialCompanyName;
	}

	public void setFinancialCompanyName(String financialCompanyName) {
		this.financialCompanyName = financialCompanyName;
	}

	public String getFinancialCompanyCode() {
		return financialCompanyCode;
	}

	public void setFinancialCompanyCode(String financialCompanyCode) {
		this.financialCompanyCode = financialCompanyCode;
	}

	public String getPaymentMethodType() {
		return paymentMethodType;
	}

	public void setPaymentMethodType(String paymentMethodType) {
		this.paymentMethodType = paymentMethodType;
	}

	public String getPaymentMethodNumber() {
		return paymentMethodNumber;
	}

	public void setPaymentMethodNumber(String paymentMethodNumber) {
		this.paymentMethodNumber = paymentMethodNumber;
	}

	public String getPaymentDatetime() {
		return paymentDatetime;
	}

	public void setPaymentDatetime(String paymentDatetime) {
		this.paymentDatetime = paymentDatetime;
	}

	public String getDeliveryDatetime() {
		return deliveryDatetime;
	}

	public void setDeliveryDatetime(String deliveryDatetime) {
		this.deliveryDatetime = deliveryDatetime;
	}

	public String getRequestConfirmImgPath() {
		return requestConfirmImgPath;
	}

	public void setRequestConfirmImgPath(String requestConfirmImgPath) {
		this.requestConfirmImgPath = requestConfirmImgPath;
	}

	public String getRequestDatetime() {
		return requestDatetime;
	}

	public void setRequestDatetime(String requestDatetime) {
		this.requestDatetime = requestDatetime;
	}

	public String getRegistId() {
		return registId;
	}

	public void setRegistId(String registId) {
		this.registId = registId;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public String getModify_id() {
		return modify_id;
	}

	public void setModify_id(String modify_id) {
		this.modify_id = modify_id;
	}

	public String getModifyRegistDatetime() {
		return modifyRegistDatetime;
	}

	public void setModifyRegistDatetime(String modifyRegistDatetime) {
		this.modifyRegistDatetime = modifyRegistDatetime;
	}

	public Integer getBillIndex() {
		return billIndex;
	}

	public void setBillIndex(Integer billIndex) {
		this.billIndex = billIndex;
	}

	public String getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(String modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public String getRequestStatusName() {
		return requestStatusName;
	}

	public void setRequestStatusName(String requestStatusName) {
		this.requestStatusName = requestStatusName;
	}
}
