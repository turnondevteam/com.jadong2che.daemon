package com.jadong2che.client.domain;



public class PaymentMethod {

	//결제수단 일련번호
	private Integer paymentMethodIndex;
	
	//결제수단 유형
	private String type;

	private String financialCompanyCode;

	private String financialCompanyName;

	public Integer getPaymentMethodIndex() {
		return paymentMethodIndex;
	}

	public void setPaymentMethodIndex(Integer paymentMethodIndex) {
		this.paymentMethodIndex = paymentMethodIndex;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFinancialCompanyCode() {
		return financialCompanyCode;
	}

	public void setFinancialCompanyCode(String financialCompanyCode) {
		this.financialCompanyCode = financialCompanyCode;
	}

	public String getFinancialCompanyName() {
		return financialCompanyName;
	}

	public void setFinancialCompanyName(String financialCompanyName) {
		this.financialCompanyName = financialCompanyName;
	}
}
