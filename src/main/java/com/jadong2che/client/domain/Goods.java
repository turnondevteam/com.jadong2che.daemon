package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;


import java.util.List;

public class Goods extends Common {

	// 상품 일련번호
	private int goodsIndex;
	
	// 고객사 일련번호
	private int companyIndex;
	
	// 상품구분
	private String goodsDivision;
	
	// 상품분류
	private String goodsCategory;
	
	// 이름
	private String goodsName;
	
	// 상품 코드
	private String goodsCode;
	
	// 상품이미지
	private String goodsImg;
	
	// 상품이미지썸네일
	private String goodsImgThumbnail;

	// 상품이미지썸네일
	private String goodsImgCircle;

	// 간략내용
	private String miniContent;
	
	// 내용
	private String contents;
	
	// 노출 여부
	private Yn displayYn;
	
	// 사용 여부
	private Yn useYn;
	
	// 고객정보사용여부
	private Yxn customerInfoUseYn;
	
	// 과세여부
	private Yn taxYn;
	
	// 유형
	private int type;
	
	// 신청 상태
	private String requestStatus;
	
	// 등록일시
	private String registDatetime;
	
	// 등록id
	private int registId;
	
	// 수정일시
	private String modifyDatetime;
	
	// 수정id
	private int modifyId;
	
	private int discountCost;

	private int cost;
	
	private int sellCost;

	private List<String> goodsIndexArray;

	private List<String> useYnArray;
	
	private String paymentMethod;

	private int termsIndex;

	private String termsTitle;

	private String customerInfoNameUseYn;

	private String customerInfoBirthdayUseYn;

	private String customerInfoCellphoneUseYn;

	private String customerInfoUserNumberUseYn;

	private String customerInfoUserIdUseYn;

	private String displayNewYn;

	private String displayDiscountRate;

	private String displayCostContents;

	private String displayDiscountCostContents;

	private String deliveryText;

	private String deliverySelectYn;

	private String companyName;

	private int order;

	private String tags;

	private String companyGoodsCode;

	private String costContents;

	private String paymentDatetimeContents;

	private String customerInfo;

	private int goodsCategoryIndex;

	private String goodsInfo;

	private String serviceInfo;

	private String requestInfo;

	private String popularityYn;

	private String goodsCostInfo;

	private String gasYn;


	public int getGoodsIndex() {
		return goodsIndex;
	}

	public void setGoodsIndex(int goodsIndex) {
		this.goodsIndex = goodsIndex;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public String getGoodsDivision() {
		return goodsDivision;
	}

	public void setGoodsDivision(String goodsDivision) {
		this.goodsDivision = goodsDivision;
	}

	public String getGoodsCategory() {
		return goodsCategory;
	}

	public void setGoodsCategory(String goodsCategory) {
		this.goodsCategory = goodsCategory;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getGoodsImg() {
		return goodsImg;
	}

	public void setGoodsImg(String goodsImg) {
		this.goodsImg = goodsImg;
	}

	public String getGoodsImgThumbnail() {
		return goodsImgThumbnail;
	}

	public void setGoodsImgThumbnail(String goodsImgThumbnail) {
		this.goodsImgThumbnail = goodsImgThumbnail;
	}

	public String getGoodsImgCircle() {
		return goodsImgCircle;
	}

	public void setGoodsImgCircle(String goodsImgCircle) {
		this.goodsImgCircle = goodsImgCircle;
	}

	public String getMiniContent() {
		return miniContent;
	}

	public void setMiniContent(String miniContent) {
		this.miniContent = miniContent;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Yn getDisplayYn() {
		return displayYn;
	}

	public void setDisplayYn(Yn displayYn) {
		this.displayYn = displayYn;
	}

	public Yn getUseYn() {
		return useYn;
	}

	public void setUseYn(Yn useYn) {
		this.useYn = useYn;
	}

	public Yxn getCustomerInfoUseYn() {
		return customerInfoUseYn;
	}

	public void setCustomerInfoUseYn(Yxn customerInfoUseYn) {
		this.customerInfoUseYn = customerInfoUseYn;
	}

	public Yn getTaxYn() {
		return taxYn;
	}

	public void setTaxYn(Yn taxYn) {
		this.taxYn = taxYn;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public int getRegistId() {
		return registId;
	}

	public void setRegistId(int registId) {
		this.registId = registId;
	}

	public String getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(String modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public int getModifyId() {
		return modifyId;
	}

	public void setModifyId(int modifyId) {
		this.modifyId = modifyId;
	}

	public int getDiscountCost() {
		return discountCost;
	}

	public void setDiscountCost(int discountCost) {
		this.discountCost = discountCost;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getSellCost() {
		return sellCost;
	}

	public void setSellCost(int sellCost) {
		this.sellCost = sellCost;
	}

	public List<String> getGoodsIndexArray() {
		return goodsIndexArray;
	}

	public void setGoodsIndexArray(List<String> goodsIndexArray) {
		this.goodsIndexArray = goodsIndexArray;
	}

	public List<String> getUseYnArray() {
		return useYnArray;
	}

	public void setUseYnArray(List<String> useYnArray) {
		this.useYnArray = useYnArray;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public int getTermsIndex() {
		return termsIndex;
	}

	public void setTermsIndex(int termsIndex) {
		this.termsIndex = termsIndex;
	}

	public String getTermsTitle() {
		return termsTitle;
	}

	public void setTermsTitle(String termsTitle) {
		this.termsTitle = termsTitle;
	}

	public String getCustomerInfoNameUseYn() {
		return customerInfoNameUseYn;
	}

	public void setCustomerInfoNameUseYn(String customerInfoNameUseYn) {
		this.customerInfoNameUseYn = customerInfoNameUseYn;
	}

	public String getCustomerInfoBirthdayUseYn() {
		return customerInfoBirthdayUseYn;
	}

	public void setCustomerInfoBirthdayUseYn(String customerInfoBirthdayUseYn) {
		this.customerInfoBirthdayUseYn = customerInfoBirthdayUseYn;
	}

	public String getCustomerInfoCellphoneUseYn() {
		return customerInfoCellphoneUseYn;
	}

	public void setCustomerInfoCellphoneUseYn(String customerInfoCellphoneUseYn) {
		this.customerInfoCellphoneUseYn = customerInfoCellphoneUseYn;
	}

	public String getCustomerInfoUserNumberUseYn() {
		return customerInfoUserNumberUseYn;
	}

	public void setCustomerInfoUserNumberUseYn(String customerInfoUserNumberUseYn) {
		this.customerInfoUserNumberUseYn = customerInfoUserNumberUseYn;
	}

	public String getCustomerInfoUserIdUseYn() {
		return customerInfoUserIdUseYn;
	}

	public void setCustomerInfoUserIdUseYn(String customerInfoUserIdUseYn) {
		this.customerInfoUserIdUseYn = customerInfoUserIdUseYn;
	}

	public String getDisplayNewYn() {
		return displayNewYn;
	}

	public void setDisplayNewYn(String displayNewYn) {
		this.displayNewYn = displayNewYn;
	}

	public String getDisplayDiscountRate() {
		return displayDiscountRate;
	}

	public void setDisplayDiscountRate(String displayDiscountRate) {
		this.displayDiscountRate = displayDiscountRate;
	}

	public String getDisplayCostContents() {
		return displayCostContents;
	}

	public void setDisplayCostContents(String displayCostContents) {
		this.displayCostContents = displayCostContents;
	}

	public String getDisplayDiscountCostContents() {
		return displayDiscountCostContents;
	}

	public void setDisplayDiscountCostContents(String displayDiscountCostContents) {
		this.displayDiscountCostContents = displayDiscountCostContents;
	}

	public String getDeliveryText() {
		return deliveryText;
	}

	public void setDeliveryText(String deliveryText) {
		this.deliveryText = deliveryText;
	}

	public String getDeliverySelectYn() {
		return deliverySelectYn;
	}

	public void setDeliverySelectYn(String deliverySelectYn) {
		this.deliverySelectYn = deliverySelectYn;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getCompanyGoodsCode() {
		return companyGoodsCode;
	}

	public void setCompanyGoodsCode(String companyGoodsCode) {
		this.companyGoodsCode = companyGoodsCode;
	}

	public String getCostContents() {
		return costContents;
	}

	public void setCostContents(String costContents) {
		this.costContents = costContents;
	}

	public String getPaymentDatetimeContents() {
		return paymentDatetimeContents;
	}

	public void setPaymentDatetimeContents(String paymentDatetimeContents) {
		this.paymentDatetimeContents = paymentDatetimeContents;
	}

	public String getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(String customerInfo) {
		this.customerInfo = customerInfo;
	}

	public int getGoodsCategoryIndex() {
		return goodsCategoryIndex;
	}

	public void setGoodsCategoryIndex(int goodsCategoryIndex) {
		this.goodsCategoryIndex = goodsCategoryIndex;
	}

	public String getGoodsInfo() {
		return goodsInfo;
	}

	public void setGoodsInfo(String goodsInfo) {
		this.goodsInfo = goodsInfo;
	}

	public String getServiceInfo() {
		return serviceInfo;
	}

	public void setServiceInfo(String serviceInfo) {
		this.serviceInfo = serviceInfo;
	}

	public String getRequestInfo() {
		return requestInfo;
	}

	public void setRequestInfo(String requestInfo) {
		this.requestInfo = requestInfo;
	}

	public String getPopularityYn() {
		return popularityYn;
	}

	public void setPopularityYn(String popularityYn) {
		this.popularityYn = popularityYn;
	}

	public String getGoodsCostInfo() {
		return goodsCostInfo;
	}

	public void setGoodsCostInfo(String goodsCostInfo) {
		this.goodsCostInfo = goodsCostInfo;
	}


	public String getGasYn() {
		return gasYn;
	}

	public void setGasYn(String gasYn) {
		this.gasYn = gasYn;
	}
}
