package com.jadong2che.client.domain;



public class GoodsEtcInfo {

	private int goodsEtcInfoIndex;

	// 상품 일련번호
	private int goodsIndex;

	// 옵션 구분
	private String etcDivision;


	// 옵션 내용
	private String etcContents;

	public int getGoodsEtcInfoIndex() {
		return goodsEtcInfoIndex;
	}

	public void setGoodsEtcInfoIndex(int goodsEtcInfoIndex) {
		this.goodsEtcInfoIndex = goodsEtcInfoIndex;
	}

	public int getGoodsIndex() {
		return goodsIndex;
	}

	public void setGoodsIndex(int goodsIndex) {
		this.goodsIndex = goodsIndex;
	}

	public String getEtcDivision() {
		return etcDivision;
	}

	public void setEtcDivision(String etcDivision) {
		this.etcDivision = etcDivision;
	}

	public String getEtcContents() {
		return etcContents;
	}

	public void setEtcContents(String etcContents) {
		this.etcContents = etcContents;
	}
}
