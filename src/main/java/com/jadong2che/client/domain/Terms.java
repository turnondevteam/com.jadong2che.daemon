package com.jadong2che.client.domain;



public class Terms {

	// 약관 일련번호
	private int termsIndex;
	
	// 고객사 일련번호
	private int companyIndex;
	
	// 구분코드
	private String division;
	
	// 구분명
	private String divisionNm;
	
	// 제목
	private String title;
	
	// 내용
	private String contents;
	
	// 등록일시
	private String registDatetime;
	
	private String memberName;
	
	private String companyName;
	
	private String filePath1;
	
	private String filePath2;
	
	private String version;

	private String affiliateName;

	private int termsCount;

	private String displayYn;

	private String requestDisplayYn;

	public int getTermsIndex() {
		return termsIndex;
	}

	public void setTermsIndex(int termsIndex) {
		this.termsIndex = termsIndex;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getDivisionNm() {
		return divisionNm;
	}

	public void setDivisionNm(String divisionNm) {
		this.divisionNm = divisionNm;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFilePath1() {
		return filePath1;
	}

	public void setFilePath1(String filePath1) {
		this.filePath1 = filePath1;
	}

	public String getFilePath2() {
		return filePath2;
	}

	public void setFilePath2(String filePath2) {
		this.filePath2 = filePath2;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getAffiliateName() {
		return affiliateName;
	}

	public void setAffiliateName(String affiliateName) {
		this.affiliateName = affiliateName;
	}

	public int getTermsCount() {
		return termsCount;
	}

	public void setTermsCount(int termsCount) {
		this.termsCount = termsCount;
	}

	public String getDisplayYn() {
		return displayYn;
	}

	public void setDisplayYn(String displayYn) {
		this.displayYn = displayYn;
	}

	public String getRequestDisplayYn() {
		return requestDisplayYn;
	}

	public void setRequestDisplayYn(String requestDisplayYn) {
		this.requestDisplayYn = requestDisplayYn;
	}
}
