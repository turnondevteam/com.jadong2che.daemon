package com.jadong2che.client.domain;



public class Region {

	// 시도 코드
	private String codeSido;
	
	// 시도
	private String sido;

	public String getCodeSido() {
		return codeSido;
	}

	public void setCodeSido(String codeSido) {
		this.codeSido = codeSido;
	}

	public String getSido() {
		return sido;
	}

	public void setSido(String sido) {
		this.sido = sido;
	}
}
