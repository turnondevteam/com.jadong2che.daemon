package com.jadong2che.client.domain;


public class OutUser{

    private int outUserIndex;

    private int companyIndex;

    private String customerNumber;

    private String customerName;

    private String address;

    private String payNumber;

    private String meterNumber;

    private String registDatetime;

    private String registId;

    private String modifyDatetime;

    private String modifyId;

    private String companyName;

    private String tel;

    private String id;

    private String birthday;

    private String email;

    private String billNumber;

    private String contractNumber;

    private int outUserTempIndex;

    private String outUserTempGroup;

    private int goodsIndex;

    private String goodsName;

    private String jsonData;

    private int cost;

    private int outUserGoodsIndex;

    private int goodsCount;

    private String costOptionContents;

    private String paymentDatetime;

    private String requestDatetime;

    private String period;

    private String issueDate;

    private String addressDetail;

    private String paymentType;

    private String financialCompanyName;

    private String accoutNumber;

    private String paymentNubmer;

    public int getOutUserIndex() {
        return outUserIndex;
    }

    public void setOutUserIndex(int outUserIndex) {
        this.outUserIndex = outUserIndex;
    }

    public int getCompanyIndex() {
        return companyIndex;
    }

    public void setCompanyIndex(int companyIndex) {
        this.companyIndex = companyIndex;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPayNumber() {
        return payNumber;
    }

    public void setPayNumber(String payNumber) {
        this.payNumber = payNumber;
    }

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public String getRegistDatetime() {
        return registDatetime;
    }

    public void setRegistDatetime(String registDatetime) {
        this.registDatetime = registDatetime;
    }

    public String getRegistId() {
        return registId;
    }

    public void setRegistId(String registId) {
        this.registId = registId;
    }

    public String getModifyDatetime() {
        return modifyDatetime;
    }

    public void setModifyDatetime(String modifyDatetime) {
        this.modifyDatetime = modifyDatetime;
    }

    public String getModifyId() {
        return modifyId;
    }

    public void setModifyId(String modifyId) {
        this.modifyId = modifyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public int getOutUserTempIndex() {
        return outUserTempIndex;
    }

    public void setOutUserTempIndex(int outUserTempIndex) {
        this.outUserTempIndex = outUserTempIndex;
    }

    public String getOutUserTempGroup() {
        return outUserTempGroup;
    }

    public void setOutUserTempGroup(String outUserTempGroup) {
        this.outUserTempGroup = outUserTempGroup;
    }

    public int getGoodsIndex() {
        return goodsIndex;
    }

    public void setGoodsIndex(int goodsIndex) {
        this.goodsIndex = goodsIndex;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getOutUserGoodsIndex() {
        return outUserGoodsIndex;
    }

    public void setOutUserGoodsIndex(int outUserGoodsIndex) {
        this.outUserGoodsIndex = outUserGoodsIndex;
    }

    public int getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(int goodsCount) {
        this.goodsCount = goodsCount;
    }

    public String getCostOptionContents() {
        return costOptionContents;
    }

    public void setCostOptionContents(String costOptionContents) {
        this.costOptionContents = costOptionContents;
    }

    public String getPaymentDatetime() {
        return paymentDatetime;
    }

    public void setPaymentDatetime(String paymentDatetime) {
        this.paymentDatetime = paymentDatetime;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getFinancialCompanyName() {
        return financialCompanyName;
    }

    public void setFinancialCompanyName(String financialCompanyName) {
        this.financialCompanyName = financialCompanyName;
    }

    public String getAccoutNumber() {
        return accoutNumber;
    }

    public void setAccoutNumber(String accoutNumber) {
        this.accoutNumber = accoutNumber;
    }

    public String getRequestDatetime() {
        return requestDatetime;
    }

    public void setRequestDatetime(String requestDatetime) {
        this.requestDatetime = requestDatetime;
    }

    public String getPaymentNubmer() {
        return paymentNubmer;
    }

    public void setPaymentNubmer(String paymentNubmer) {
        this.paymentNubmer = paymentNubmer;
    }
}