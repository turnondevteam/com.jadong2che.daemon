package com.jadong2che.client.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CompanyHistory {

	private String userName;

	private String userNumber;

	private int commonIndex;

	private String sendTitle;

	private String successYn;

	private String sendDatetime;

	private String companyName;

	private Integer pushIndex;

}
