package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;

public class User extends Common {

	// 사용자 일련번호
	private int userIndex;
	 
	private String userId;
	
	private String userPassword;
	
	private String userCi;
	
	private String certificationYn;
	
	private String certificationType;
	
	private String userName;
	
	private String birthday;
	
	private int gender;
	
	private String cellphone;
	
	private String regionCode;
	
	private String address;
	
	private String address2;
	
	private String zipcode;
	
	private String pushRecvYn;
	
	private String joinDatetime;
	
	private String leaveDatetime;
	
	private int companyIndex;
	
	private String userNumber;
	
	private int userGoodsCount;
	
	private int companyGoodsCount;
	
	private int paymentMethodIndex;
	
	private String paymentMethodType;
	
	private String paymentMethodText;
	
	private int goodsIndex;
	
	private String goodsName;
	
	private int requestIndex;
	
	private String paymentDatetime;
	
	private int cost;

	private String email;
	
	private String dateType;
	
	private String startDate;
	
	private String endDate;
	
	private String carrier;
	
	private String emailRecvYn;
	
	private String smsRecvYn;
	
	private String appJoinStatus;
	
	private String requestStatus;

	private int deliveryAddressIndex;

	private String newZipcode;

	private String newAddress;

	private int paymentMethodDetailIndex;

	private String registDatetime;

	private int paymentMethodCount;

	private int billCount;

	private int csCount;

	private String appJoinDatetime;

	private String paymentMethodName;

	private String financialCompanyCode;

	private String financialCompanyName;

	private String cardNumber;

	private String expirationDate;

	private String accountNumber;

	private String nickname;

	private int userTempIndex;

	private String userTempGroup;

	private int requestCount;

	private int selfmeterCount;

	private String appToken;

	private String accessToken;

	private String recvName;

	private String authYn;

	private String joinType;

	public int getUserIndex() {
		return userIndex;
	}

	public void setUserIndex(int userIndex) {
		this.userIndex = userIndex;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserCi() {
		return userCi;
	}

	public void setUserCi(String userCi) {
		this.userCi = userCi;
	}

	public String getCertificationYn() {
		return certificationYn;
	}

	public void setCertificationYn(String certificationYn) {
		this.certificationYn = certificationYn;
	}

	public String getCertificationType() {
		return certificationType;
	}

	public void setCertificationType(String certificationType) {
		this.certificationType = certificationType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getPushRecvYn() {
		return pushRecvYn;
	}

	public void setPushRecvYn(String pushRecvYn) {
		this.pushRecvYn = pushRecvYn;
	}

	public String getJoinDatetime() {
		return joinDatetime;
	}

	public void setJoinDatetime(String joinDatetime) {
		this.joinDatetime = joinDatetime;
	}

	public String getLeaveDatetime() {
		return leaveDatetime;
	}

	public void setLeaveDatetime(String leaveDatetime) {
		this.leaveDatetime = leaveDatetime;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	public int getUserGoodsCount() {
		return userGoodsCount;
	}

	public void setUserGoodsCount(int userGoodsCount) {
		this.userGoodsCount = userGoodsCount;
	}

	public int getCompanyGoodsCount() {
		return companyGoodsCount;
	}

	public void setCompanyGoodsCount(int companyGoodsCount) {
		this.companyGoodsCount = companyGoodsCount;
	}

	public int getPaymentMethodIndex() {
		return paymentMethodIndex;
	}

	public void setPaymentMethodIndex(int paymentMethodIndex) {
		this.paymentMethodIndex = paymentMethodIndex;
	}

	public String getPaymentMethodType() {
		return paymentMethodType;
	}

	public void setPaymentMethodType(String paymentMethodType) {
		this.paymentMethodType = paymentMethodType;
	}

	public String getPaymentMethodText() {
		return paymentMethodText;
	}

	public void setPaymentMethodText(String paymentMethodText) {
		this.paymentMethodText = paymentMethodText;
	}

	public int getGoodsIndex() {
		return goodsIndex;
	}

	public void setGoodsIndex(int goodsIndex) {
		this.goodsIndex = goodsIndex;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public int getRequestIndex() {
		return requestIndex;
	}

	public void setRequestIndex(int requestIndex) {
		this.requestIndex = requestIndex;
	}

	public String getPaymentDatetime() {
		return paymentDatetime;
	}

	public void setPaymentDatetime(String paymentDatetime) {
		this.paymentDatetime = paymentDatetime;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDateType() {
		return dateType;
	}

	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getEmailRecvYn() {
		return emailRecvYn;
	}

	public void setEmailRecvYn(String emailRecvYn) {
		this.emailRecvYn = emailRecvYn;
	}

	public String getSmsRecvYn() {
		return smsRecvYn;
	}

	public void setSmsRecvYn(String smsRecvYn) {
		this.smsRecvYn = smsRecvYn;
	}

	public String getAppJoinStatus() {
		return appJoinStatus;
	}

	public void setAppJoinStatus(String appJoinStatus) {
		this.appJoinStatus = appJoinStatus;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public int getDeliveryAddressIndex() {
		return deliveryAddressIndex;
	}

	public void setDeliveryAddressIndex(int deliveryAddressIndex) {
		this.deliveryAddressIndex = deliveryAddressIndex;
	}

	public String getNewZipcode() {
		return newZipcode;
	}

	public void setNewZipcode(String newZipcode) {
		this.newZipcode = newZipcode;
	}

	public String getNewAddress() {
		return newAddress;
	}

	public void setNewAddress(String newAddress) {
		this.newAddress = newAddress;
	}

	public int getPaymentMethodDetailIndex() {
		return paymentMethodDetailIndex;
	}

	public void setPaymentMethodDetailIndex(int paymentMethodDetailIndex) {
		this.paymentMethodDetailIndex = paymentMethodDetailIndex;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public int getPaymentMethodCount() {
		return paymentMethodCount;
	}

	public void setPaymentMethodCount(int paymentMethodCount) {
		this.paymentMethodCount = paymentMethodCount;
	}

	public int getBillCount() {
		return billCount;
	}

	public void setBillCount(int billCount) {
		this.billCount = billCount;
	}

	public int getCsCount() {
		return csCount;
	}

	public void setCsCount(int csCount) {
		this.csCount = csCount;
	}

	public String getAppJoinDatetime() {
		return appJoinDatetime;
	}

	public void setAppJoinDatetime(String appJoinDatetime) {
		this.appJoinDatetime = appJoinDatetime;
	}

	public String getPaymentMethodName() {
		return paymentMethodName;
	}

	public void setPaymentMethodName(String paymentMethodName) {
		this.paymentMethodName = paymentMethodName;
	}

	public String getFinancialCompanyCode() {
		return financialCompanyCode;
	}

	public void setFinancialCompanyCode(String financialCompanyCode) {
		this.financialCompanyCode = financialCompanyCode;
	}

	public String getFinancialCompanyName() {
		return financialCompanyName;
	}

	public void setFinancialCompanyName(String financialCompanyName) {
		this.financialCompanyName = financialCompanyName;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getUserTempIndex() {
		return userTempIndex;
	}

	public void setUserTempIndex(int userTempIndex) {
		this.userTempIndex = userTempIndex;
	}

	public String getUserTempGroup() {
		return userTempGroup;
	}

	public void setUserTempGroup(String userTempGroup) {
		this.userTempGroup = userTempGroup;
	}

	public int getRequestCount() {
		return requestCount;
	}

	public void setRequestCount(int requestCount) {
		this.requestCount = requestCount;
	}

	public int getSelfmeterCount() {
		return selfmeterCount;
	}

	public void setSelfmeterCount(int selfmeterCount) {
		this.selfmeterCount = selfmeterCount;
	}

	public String getAppToken() {
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRecvName() {
		return recvName;
	}

	public void setRecvName(String recvName) {
		this.recvName = recvName;
	}

	public String getAuthYn() {
		return authYn;
	}

	public void setAuthYn(String authYn) {
		this.authYn = authYn;
	}

	public String getJoinType() {
		return joinType;
	}

	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}
}
