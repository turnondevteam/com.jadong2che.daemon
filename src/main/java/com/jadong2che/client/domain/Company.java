package com.jadong2che.client.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Company {
	// 일련번호
	private int companyIndex;

	// 기타관리코드
	private String etcManagementCode;

    // 업체명
	private String name;

    // 사업자등록번호
	private String bizNumber;

    // 법인등록번호
	private String corpRegistNumber;

    // 대표자명
	private String ceoName;

    // 대표 연락처
	private String ceoCellphone;

    // 사업 업태
	private String bizType;

    // 사업 종목
	private String bizType2;

    // 연락처
	private String ceoTel;

    // 팩스
	private String faxNumber;

    // 우편번호
	private String zipcode;

    // 주소
	private String address;

    // 계산서 발급 이메일
	private String ceoEmail;

    // 담당자명
	private String staffName;

    // 연락처
	private String staffCellphone;

    // 이메일
	private String staffEmail;

    // 계약일
	private String registDatetime;

    // 기타정보
	private String etcInfo;

	// 계약 여부
	private String contractYn;

	// 계약 일
	private String contractDatetime;

	private String type;

	private String pgShopCode;

	private String customerCenterTel;

	private String reportEmail;

	private String customerInqueryCode;

	private String customerInqueryText;

    private String oldNewUseYn;

    private String termsRequest1;

    private String termsRequest2;

    private String termsRequest3;

    private String paymentMonth;

    private String paymentBill;

    private String paymentRelationshipShow;

    private String paymentDelivery;

    private String paymentPeriod;

    private String paymentGoodsName;

    private String paymentCostType;

    private String paymentCost;

    private String paymentCostFixedType;

    private String paymentUser;

    private String apiServerYn;

    private String apiServerUrl;


}
