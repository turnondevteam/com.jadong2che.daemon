package com.jadong2che.client.domain;



public class PaymentMethodDetail {

	//결제수단 일련번호
	private Integer paymentMethodIndex;

	//결제수단 유형
	private String type;

	//결제수단 상세 일련번호
	private Integer paymentMethodDetailIndex;

	//금융 회사 이름
	private String financialCompanyName;

	//금융 회사 코드
	private String financialCompanyCode;

	//회사 이미지 경로
	private String companyImgPath;

	//사용 여부
	private String useYn;

	//정렬순서
	private int sortOrder;

	//시작일
	private String startDate;

	//등록 여부
	private String registYn;

	//회사 일련번호
	private Integer companyIndex;

	public Integer getPaymentMethodIndex() {
		return paymentMethodIndex;
	}

	public void setPaymentMethodIndex(Integer paymentMethodIndex) {
		this.paymentMethodIndex = paymentMethodIndex;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getPaymentMethodDetailIndex() {
		return paymentMethodDetailIndex;
	}

	public void setPaymentMethodDetailIndex(Integer paymentMethodDetailIndex) {
		this.paymentMethodDetailIndex = paymentMethodDetailIndex;
	}

	public String getFinancialCompanyName() {
		return financialCompanyName;
	}

	public void setFinancialCompanyName(String financialCompanyName) {
		this.financialCompanyName = financialCompanyName;
	}

	public String getFinancialCompanyCode() {
		return financialCompanyCode;
	}

	public void setFinancialCompanyCode(String financialCompanyCode) {
		this.financialCompanyCode = financialCompanyCode;
	}

	public String getCompanyImgPath() {
		return companyImgPath;
	}

	public void setCompanyImgPath(String companyImgPath) {
		this.companyImgPath = companyImgPath;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getRegistYn() {
		return registYn;
	}

	public void setRegistYn(String registYn) {
		this.registYn = registYn;
	}

	public Integer getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(Integer companyIndex) {
		this.companyIndex = companyIndex;
	}
}
