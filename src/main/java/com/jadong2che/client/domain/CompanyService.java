package com.jadong2che.client.domain;



public class CompanyService {
	private int companyServiceIndex;

	private int companyIndex;

	private String title;

	private String contents;

	private String status;

	private String registId;

	private String registDatetime;

	private String modifyId;

	private String modifyDatetime;

	private String companyName;

	private String companyType;

	private int companyServiceChargeIndex;

	private String chargeCode;

	private String displayCharge;

	private int charge;

	private String applyDatetime;

	private String chargeCodeName;

	public int getCompanyServiceIndex() {
		return companyServiceIndex;
	}

	public void setCompanyServiceIndex(int companyServiceIndex) {
		this.companyServiceIndex = companyServiceIndex;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRegistId() {
		return registId;
	}

	public void setRegistId(String registId) {
		this.registId = registId;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public String getModifyId() {
		return modifyId;
	}

	public void setModifyId(String modifyId) {
		this.modifyId = modifyId;
	}

	public String getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(String modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public int getCompanyServiceChargeIndex() {
		return companyServiceChargeIndex;
	}

	public void setCompanyServiceChargeIndex(int companyServiceChargeIndex) {
		this.companyServiceChargeIndex = companyServiceChargeIndex;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getDisplayCharge() {
		return displayCharge;
	}

	public void setDisplayCharge(String displayCharge) {
		this.displayCharge = displayCharge;
	}

	public int getCharge() {
		return charge;
	}

	public void setCharge(int charge) {
		this.charge = charge;
	}

	public String getApplyDatetime() {
		return applyDatetime;
	}

	public void setApplyDatetime(String applyDatetime) {
		this.applyDatetime = applyDatetime;
	}

	public String getChargeCodeName() {
		return chargeCodeName;
	}

	public void setChargeCodeName(String chargeCodeName) {
		this.chargeCodeName = chargeCodeName;
	}
}
