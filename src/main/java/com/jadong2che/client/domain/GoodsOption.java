package com.jadong2che.client.domain;



public class GoodsOption {

	// 상품 일련번호
	private int goodsIndex;


	// 옵션 구분
	private String division;

	// 필수 여부
	private Yn essentialYn;


	// 옵션 일련번호
	private int optionIndex;
	
	// 옵션 내용
	private String contents;


	private int costOptionIndex;

	private int paymentDatetimeOptionIndex;

	private String expressionCode;

	private int periodOptionIndex;

	private String periodContents;

	private String paymentDatetimeContents;

	public int getGoodsIndex() {
		return goodsIndex;
	}

	public void setGoodsIndex(int goodsIndex) {
		this.goodsIndex = goodsIndex;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public Yn getEssentialYn() {
		return essentialYn;
	}

	public void setEssentialYn(Yn essentialYn) {
		this.essentialYn = essentialYn;
	}

	public int getOptionIndex() {
		return optionIndex;
	}

	public void setOptionIndex(int optionIndex) {
		this.optionIndex = optionIndex;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public int getCostOptionIndex() {
		return costOptionIndex;
	}

	public void setCostOptionIndex(int costOptionIndex) {
		this.costOptionIndex = costOptionIndex;
	}

	public int getPaymentDatetimeOptionIndex() {
		return paymentDatetimeOptionIndex;
	}

	public void setPaymentDatetimeOptionIndex(int paymentDatetimeOptionIndex) {
		this.paymentDatetimeOptionIndex = paymentDatetimeOptionIndex;
	}

	public String getExpressionCode() {
		return expressionCode;
	}

	public void setExpressionCode(String expressionCode) {
		this.expressionCode = expressionCode;
	}

	public int getPeriodOptionIndex() {
		return periodOptionIndex;
	}

	public void setPeriodOptionIndex(int periodOptionIndex) {
		this.periodOptionIndex = periodOptionIndex;
	}

	public String getPeriodContents() {
		return periodContents;
	}

	public void setPeriodContents(String periodContents) {
		this.periodContents = periodContents;
	}

	public String getPaymentDatetimeContents() {
		return paymentDatetimeContents;
	}

	public void setPaymentDatetimeContents(String paymentDatetimeContents) {
		this.paymentDatetimeContents = paymentDatetimeContents;
	}
}
