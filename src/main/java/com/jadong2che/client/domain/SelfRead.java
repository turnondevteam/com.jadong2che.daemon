package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;


public class SelfRead extends Common {

	// 사용자 일련번호
	private int userIndex;
	 
	private int companyIndex;
	
	private int selfReadIndex;
	
	private int selfReadCount;
	
	private String registDatetime;
	
	private int userNumber;

	public int getUserIndex() {
		return userIndex;
	}

	public void setUserIndex(int userIndex) {
		this.userIndex = userIndex;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public int getSelfReadIndex() {
		return selfReadIndex;
	}

	public void setSelfReadIndex(int selfReadIndex) {
		this.selfReadIndex = selfReadIndex;
	}

	public int getSelfReadCount() {
		return selfReadCount;
	}

	public void setSelfReadCount(int selfReadCount) {
		this.selfReadCount = selfReadCount;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public int getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(int userNumber) {
		this.userNumber = userNumber;
	}
}
