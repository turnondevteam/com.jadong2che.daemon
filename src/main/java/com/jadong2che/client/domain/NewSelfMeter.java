package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NewSelfMeter extends Common {

	private Integer selfmeterIndex;

	private Integer companyIndex;

	private Integer userIndex;

	private String registDatetime;

	private String registId;

	private Integer selfmeterMonthlyIndex;

	private String yyyymm;

	private Integer figure;

	private String imagePath;

	private Integer selfmeterPossibilityDateIndex;

	private String yyyy;

	private String mm;

	private String startDate;

	private String endDate;

	private String userName;

}
