package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;


import java.util.List;

public class GoodsTerms extends Common {

	// 상품 일련번호
	private int goodsIndex;

	private int termsIndex;

	private String registDatetime;

	private String registId;

	public int getGoodsIndex() {
		return goodsIndex;
	}

	public void setGoodsIndex(int goodsIndex) {
		this.goodsIndex = goodsIndex;
	}

	public int getTermsIndex() {
		return termsIndex;
	}

	public void setTermsIndex(int termsIndex) {
		this.termsIndex = termsIndex;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public String getRegistId() {
		return registId;
	}

	public void setRegistId(String registId) {
		this.registId = registId;
	}
}
