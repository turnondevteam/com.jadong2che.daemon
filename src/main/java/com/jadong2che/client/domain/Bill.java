package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;


public class Bill extends Common {

    //Common
    private String regisdId;
    private String registDatetime;
    private String modifyId;
    private String modfiyDatetime;
    private String companyName;
    private String goodsName;
    private String userName;
    private String userNumber;
    private String cellphone;
    private String appJoinDatetime;
    private String joinDatetime;
    private String id;
    private String birthday;


    //bill
    private int billIndex;
    private int requestInfoIndex;
    private int companyIndex;
    private int userIndex;
    private int goodsIndex;
    private String billStatus;
    private String billName;
    private String address;
    private String customerNumber;
    private String leaveRequestDatetime;
    private String leaveDatetime;
    private String companyGoodsCode;

    //bill_montly
    private int billMonthlyIndex;
    private int billMonthlyTempIndex;
    private String companyBillNumber;
    private String status;
    private String issueDate;
    private String startDatetime;
    private String endDatetime;
    private String paymentMethod;
    private String paymentMethodNumber;
    private String chargeDatetime;
    private String chargeCost;
    private String chargeYear;
    private String chargeMonth;
    private String chargeDay;

    private String round;
    private String paymentDatetime;

    //billGroup

    private String costOptionContents;
    private int billGroupIndex;
    private String billGroupCode;
    private String cost;
    private int period;
    private String issueDatetime;
    private String billGroupIssueDate;
    private int billGroupCost;
    private String billIssueDate;

    //payment
    private String paymentNumber;
    private int payIndex;
    private int nonPaymentCost;
    private int paymentCost;
    private String paymentStatus;
    private String paymentMethodName;
    private String financialCompanyName;
    private String approvalNumber;
    private String paymentRound;
    private int paymentDetailIndex;

    private int requestIndex;

    private String displayAccountNo;


    public String getRegisdId() {
        return regisdId;
    }

    public void setRegisdId(String regisdId) {
        this.regisdId = regisdId;
    }

    public String getRegistDatetime() {
        return registDatetime;
    }

    public void setRegistDatetime(String registDatetime) {
        this.registDatetime = registDatetime;
    }

    public String getModifyId() {
        return modifyId;
    }

    public void setModifyId(String modifyId) {
        this.modifyId = modifyId;
    }

    public String getModfiyDatetime() {
        return modfiyDatetime;
    }

    public void setModfiyDatetime(String modfiyDatetime) {
        this.modfiyDatetime = modfiyDatetime;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getAppJoinDatetime() {
        return appJoinDatetime;
    }

    public void setAppJoinDatetime(String appJoinDatetime) {
        this.appJoinDatetime = appJoinDatetime;
    }

    public String getJoinDatetime() {
        return joinDatetime;
    }

    public void setJoinDatetime(String joinDatetime) {
        this.joinDatetime = joinDatetime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getBillIndex() {
        return billIndex;
    }

    public void setBillIndex(int billIndex) {
        this.billIndex = billIndex;
    }

    public int getRequestInfoIndex() {
        return requestInfoIndex;
    }

    public void setRequestInfoIndex(int requestInfoIndex) {
        this.requestInfoIndex = requestInfoIndex;
    }

    public int getCompanyIndex() {
        return companyIndex;
    }

    public void setCompanyIndex(int companyIndex) {
        this.companyIndex = companyIndex;
    }

    public int getUserIndex() {
        return userIndex;
    }

    public void setUserIndex(int userIndex) {
        this.userIndex = userIndex;
    }

    public int getGoodsIndex() {
        return goodsIndex;
    }

    public void setGoodsIndex(int goodsIndex) {
        this.goodsIndex = goodsIndex;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getBillName() {
        return billName;
    }

    public void setBillName(String billName) {
        this.billName = billName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getLeaveRequestDatetime() {
        return leaveRequestDatetime;
    }

    public void setLeaveRequestDatetime(String leaveRequestDatetime) {
        this.leaveRequestDatetime = leaveRequestDatetime;
    }

    public String getLeaveDatetime() {
        return leaveDatetime;
    }

    public void setLeaveDatetime(String leaveDatetime) {
        this.leaveDatetime = leaveDatetime;
    }

    public String getCompanyGoodsCode() {
        return companyGoodsCode;
    }

    public void setCompanyGoodsCode(String companyGoodsCode) {
        this.companyGoodsCode = companyGoodsCode;
    }

    public int getBillMonthlyIndex() {
        return billMonthlyIndex;
    }

    public void setBillMonthlyIndex(int billMonthlyIndex) {
        this.billMonthlyIndex = billMonthlyIndex;
    }

    public int getBillMonthlyTempIndex() {
        return billMonthlyTempIndex;
    }

    public void setBillMonthlyTempIndex(int billMonthlyTempIndex) {
        this.billMonthlyTempIndex = billMonthlyTempIndex;
    }

    public String getCompanyBillNumber() {
        return companyBillNumber;
    }

    public void setCompanyBillNumber(String companyBillNumber) {
        this.companyBillNumber = companyBillNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(String startDatetime) {
        this.startDatetime = startDatetime;
    }

    public String getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(String endDatetime) {
        this.endDatetime = endDatetime;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodNumber() {
        return paymentMethodNumber;
    }

    public void setPaymentMethodNumber(String paymentMethodNumber) {
        this.paymentMethodNumber = paymentMethodNumber;
    }

    public String getChargeDatetime() {
        return chargeDatetime;
    }

    public void setChargeDatetime(String chargeDatetime) {
        this.chargeDatetime = chargeDatetime;
    }

    public String getChargeCost() {
        return chargeCost;
    }

    public void setChargeCost(String chargeCost) {
        this.chargeCost = chargeCost;
    }

    public String getChargeYear() {
        return chargeYear;
    }

    public void setChargeYear(String chargeYear) {
        this.chargeYear = chargeYear;
    }

    public String getChargeMonth() {
        return chargeMonth;
    }

    public void setChargeMonth(String chargeMonth) {
        this.chargeMonth = chargeMonth;
    }

    public String getChargeDay() {
        return chargeDay;
    }

    public void setChargeDay(String chargeDay) {
        this.chargeDay = chargeDay;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getPaymentDatetime() {
        return paymentDatetime;
    }

    public void setPaymentDatetime(String paymentDatetime) {
        this.paymentDatetime = paymentDatetime;
    }

    public String getCostOptionContents() {
        return costOptionContents;
    }

    public void setCostOptionContents(String costOptionContents) {
        this.costOptionContents = costOptionContents;
    }

    public int getBillGroupIndex() {
        return billGroupIndex;
    }

    public void setBillGroupIndex(int billGroupIndex) {
        this.billGroupIndex = billGroupIndex;
    }

    public String getBillGroupCode() {
        return billGroupCode;
    }

    public void setBillGroupCode(String billGroupCode) {
        this.billGroupCode = billGroupCode;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getIssueDatetime() {
        return issueDatetime;
    }

    public void setIssueDatetime(String issueDatetime) {
        this.issueDatetime = issueDatetime;
    }

    public String getBillGroupIssueDate() {
        return billGroupIssueDate;
    }

    public void setBillGroupIssueDate(String billGroupIssueDate) {
        this.billGroupIssueDate = billGroupIssueDate;
    }

    public int getBillGroupCost() {
        return billGroupCost;
    }

    public void setBillGroupCost(int billGroupCost) {
        this.billGroupCost = billGroupCost;
    }

    public String getBillIssueDate() {
        return billIssueDate;
    }

    public void setBillIssueDate(String billIssueDate) {
        this.billIssueDate = billIssueDate;
    }

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public int getPayIndex() {
        return payIndex;
    }

    public void setPayIndex(int payIndex) {
        this.payIndex = payIndex;
    }

    public int getNonPaymentCost() {
        return nonPaymentCost;
    }

    public void setNonPaymentCost(int nonPaymentCost) {
        this.nonPaymentCost = nonPaymentCost;
    }

    public int getPaymentCost() {
        return paymentCost;
    }

    public void setPaymentCost(int paymentCost) {
        this.paymentCost = paymentCost;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getFinancialCompanyName() {
        return financialCompanyName;
    }

    public void setFinancialCompanyName(String financialCompanyName) {
        this.financialCompanyName = financialCompanyName;
    }

    public String getApprovalNumber() {
        return approvalNumber;
    }

    public void setApprovalNumber(String approvalNumber) {
        this.approvalNumber = approvalNumber;
    }

    public String getPaymentRound() {
        return paymentRound;
    }

    public void setPaymentRound(String paymentRound) {
        this.paymentRound = paymentRound;
    }

    public int getPaymentDetailIndex() {
        return paymentDetailIndex;
    }

    public void setPaymentDetailIndex(int paymentDetailIndex) {
        this.paymentDetailIndex = paymentDetailIndex;
    }

    public int getRequestIndex() {
        return requestIndex;
    }

    public void setRequestIndex(int requestIndex) {
        this.requestIndex = requestIndex;
    }

    public String getDisplayAccountNo() {
        return displayAccountNo;
    }

    public void setDisplayAccountNo(String displayAccountNo) {
        this.displayAccountNo = displayAccountNo;
    }
}