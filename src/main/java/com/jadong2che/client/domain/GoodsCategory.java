package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;



public class GoodsCategory extends Common {

	private Integer rownum;

	private Integer goodsCategoryIndex;

	private String goodsCategoryTitle;

	private String goodsCategoryContents;

	private String registDatetime;

	private String registId;

	private String modifyDatetime;

	private String modifyId;

	private String goodsCategoryImage;

	private Integer order;

	public Integer getRownum() {
		return rownum;
	}

	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}

	public Integer getGoodsCategoryIndex() {
		return goodsCategoryIndex;
	}

	public void setGoodsCategoryIndex(Integer goodsCategoryIndex) {
		this.goodsCategoryIndex = goodsCategoryIndex;
	}

	public String getGoodsCategoryTitle() {
		return goodsCategoryTitle;
	}

	public void setGoodsCategoryTitle(String goodsCategoryTitle) {
		this.goodsCategoryTitle = goodsCategoryTitle;
	}

	public String getGoodsCategoryContents() {
		return goodsCategoryContents;
	}

	public void setGoodsCategoryContents(String goodsCategoryContents) {
		this.goodsCategoryContents = goodsCategoryContents;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public String getRegistId() {
		return registId;
	}

	public void setRegistId(String registId) {
		this.registId = registId;
	}

	public String getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(String modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public String getModifyId() {
		return modifyId;
	}

	public void setModifyId(String modifyId) {
		this.modifyId = modifyId;
	}

	public String getGoodsCategoryImage() {
		return goodsCategoryImage;
	}

	public void setGoodsCategoryImage(String goodsCategoryImage) {
		this.goodsCategoryImage = goodsCategoryImage;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}
}
