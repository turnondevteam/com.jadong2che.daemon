package com.jadong2che.client.domain;

import com.jadong2che.common.domain.Common;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NewBill extends Common {

    //Bill
    private Integer billIndex;

    private Integer requestIndex;

    private Integer companyIndex;

    private Integer userIndex;

    private Integer goodsIndex;

    private String billStatus;

    private String billStatusName;

    private String name;

    private Integer billGroupIndex;

    private Integer round;

    private String issueDate;

    private String paymentDatetime;

    private String leaveRequestDatetime;

    private String leaveDatetime;

    private String registId;

    private String registDatetime;

    private String modifyId;

    private String modifyDatetime;



    //Bill Group
    private String billGroupType;

    private String billGroupName;

    private String billGroupCode;

    private Integer bill;



    //Bill Monthly
    private Integer billMonthlyIndex;

    private String companyBillNumber;

    private String status;

    private String startDatetime;

    private String endDatetime;

    private String paymentMethod;

    private String paymentMethodNumber;

    private String chargeYear;

    private String chargeMonth;

    private String chargeDay;

    private String chargeDatetime;

    private String jsonData;

    private String payJsonData;

    private String payDatetime;

    private String payYn;

    private Integer basicCost;

    private Integer useCost;

    private Integer overdueCost;

    private Integer cuttingCost;

    private Integer discountCost;

    private Integer vat;

    private Integer nonPaymentCost;

    private Integer etcCost;

    private String paymentDueDate;

    private String paymentEndDate;

    private String usage;

    private String subscriberRelationship;

    private Integer singleSellPrice;

    private String payerName;


    //ETC
    private String userName;

    private String cellphone;

    private String goodsName;

    private String costType;

    private Integer chargeCost;

}