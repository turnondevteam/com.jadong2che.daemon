package com.jadong2che.config;

import javax.servlet.Filter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {

	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/").addResourceLocations("/resources/");
		registry.addResourceHandler("/home/**").addResourceLocations("file:/home/deploy/resource/");
		registry.addResourceHandler("/image/**").addResourceLocations("file:/home/deploy/resource/image/");
		registry.addResourceHandler("/photo_upload/**").addResourceLocations("file:/home/deploy/resource/photo_upload/");

    }
	
	@Bean
	public TilesConfigurer tilesConfigurer() {
		final TilesConfigurer configurer = new TilesConfigurer();
		configurer.setDefinitions(new String[] { "classpath:tiles.xml" });
		configurer.setCheckRefresh(true);
		return configurer;
	}

	@Bean
	public TilesViewResolver tilesViewResolver() {
	    final TilesViewResolver resolver = new TilesViewResolver();
	    resolver.setViewClass(TilesView.class);
	    resolver.setOrder(1);
	    return resolver;
	}

    @Bean
    public Filter hiddenHttpMethodFilter() {
        Filter filter = new HiddenHttpMethodFilter();
        return filter;
    }
    
    @Bean
    public ViewResolver viewResolver() {
    	InternalResourceViewResolver resolver = new InternalResourceViewResolver("/WEB-INF/views/", ".jsp");
    	resolver.setOrder(2);
    	return resolver;
    }
	
}
