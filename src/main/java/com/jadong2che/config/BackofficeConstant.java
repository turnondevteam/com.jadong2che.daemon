package com.jadong2che.config;

public class BackofficeConstant {
	
	//******************************************************************************************************************
	// PAGING CONSTANT
	public static final int PAGINGUTIL_PAGE_SIZE = 10;
	
	public static final int PAGINGUTIL_PAGE_BLOCK_SIZE = 10;
	
	// DATE(YYYY FORMAT)
	public static final String DATE_FORMAT = "yyyy.MM.dd";
	public static final String DATETIME_FORMAT = "yyyy.MM.dd hh:mm:ss";
	
	public static final String COOP_CPN_IMG = "/data_IMG/cpn/coop/img";
	public static final String COOP_CPN_HTM = "/data_IMG/cpn/coop/dtl";
	public static final String COOP_CPN_CI =  "/data_IMG/cpn/coop/img/brnd";
	
	public static final String YAP_ORDER_IMG = "/data_IMG/cpn/order/img";
	public static final String YAP_ORDER_HTM = "/data_IMG/cpn/order/dtl";
	
	public static final String YAP_PAID_CPN_BANNER_IMG = "/data_IMG/cpn/banner/img";
	
	public static final String CMS_CPN_IMG = "/data_IMG/cms/cpn/img";
	
	public static final String FILE_DEV_PATH =  "http://img.dev-popcorn.co.kr";
	public static final String FILE_REAL_PATH =  "http://img.i-popcorn.co.kr";
	
	public static final String JADONG2CHE_GOODS_IMG = "/data_IMG/goods/img";
	
	// PNT 도메인
	public static final String DOMAIN_PNT_API =  "https://api.pntbiz.com";
	// https://api.pntbiz.com/beacon/list/656D6172-7474-696D-6573-717561726500
	
	// PNT UUID(이마트)
	public static final String CONFIG_PNT_UUID =  "656D6172-7474-696D-6573-717561726500";
	
	// 비콘 리스트
	public static final String URL_PNT_BEACON_LIST =  "/beacon/list/";
	
	// ZONE 리스트
	public static final String URL_PNT_ZONE_LIST =  "/geofencing/list/";
	
}