package com.jadong2che.common.mapper;

import com.jadong2che.common.domain.Dashboard;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface DashboardMapper {

	int getDashboard2CheUserCount(HashMap<String, Object> param) throws Exception;

	int getDashboardCompanyUserCount(HashMap<String, Object> param) throws Exception;

	int getDashboardChangeUserCount(HashMap<String, Object> param) throws Exception;

	int getDashboard2CheUserJoinThisWeekCount(HashMap<String, Object> param) throws Exception;

	int getDashboard2CheUserJoinLastWeekCount(HashMap<String, Object> param) throws Exception;

	int getDashboardCompanyUserJoinThisWeekCount(HashMap<String, Object> param) throws Exception;

	int getDashboardCompanyUserJoinLastWeekCount(HashMap<String, Object> param) throws Exception;

	int getDashboardCompanyUserChangeThisWeekCount(HashMap<String, Object> param) throws Exception;

	int getDashboardCompanyUserChangeLastWeekCount(HashMap<String, Object> param) throws Exception;


	List<Dashboard> getDashboardJadong2cheUser6month(HashMap<String,Object> param) throws Exception;

	List<Dashboard> getDashboardCompanyUser6month(HashMap<String,Object> param) throws Exception;

	Dashboard getDashboardRequest(HashMap<String,Object> param) throws Exception;

	Dashboard getDashboardBill(HashMap<String,Object> param) throws Exception;

	int getDashboardChageUserInfo(HashMap<String, Object> param) throws Exception;

	int getDashboardChangeRequetInfo(HashMap<String, Object> param) throws Exception;

	List<Dashboard> getDashboardGoodsInfo(HashMap<String,Object> param) throws Exception;

	int getChangeLogCount(HashMap<String,Object> param) throws Exception;

	List<Dashboard> getDashboardCsAppList(HashMap<String,Object> param) throws Exception;

	List<Dashboard> getDashboardPushList(HashMap<String,Object> param) throws Exception;



	List<HashMap<String,Object>> getNoticeList(HashMap<String,Object> param) throws Exception;

	List<HashMap<String,Object>> getGoodsList(HashMap<String,Object> param) throws Exception;

}
