package com.jadong2che.common.mapper;

import java.util.HashMap;
import java.util.List;

import com.jadong2che.client.domain.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CommonMapper {

	/**
	 * 고객사별 결제수단 리스트
	 * @param companyIndex 
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<PaymentMethod> getPaymentMethodList(int companyIndex) throws Exception;
	
	/**
	 * 고객사별 상품 리스트
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Goods> getGoodsList(int companyIndex) throws Exception;

	/**
	 * 시도 리스트
	 *
	 * @return
	 * @throws Exception
	 */
	public List<Region> getSidoList() throws Exception;

	/**
	 * 고객사 리스트
	 *
	 * @return
	 * @throws Exception
	 */
	public List<Company> getCompanyList() throws Exception;

	/**
	 * 고객사 리스트
	 *
	 * @return
	 * @throws Exception
	 */
	public List<Company> getGasCompanyList() throws Exception;

	/**
	 * 회원 리스트
	 *
	 * @return
	 * @throws Exception
	 */
	public List<User> getUserPushList() throws Exception;

	/**
	 * 회원
	 *
	 * @return
	 * @throws Exception
	 */
	public User getUserPush(int userIndex) throws Exception;
	/**
	 * 회원 리스트
	 *
	 * @return
	 * @throws Exception
	 */
	public List<User> getCompanyUserPushList(int companyIndex) throws Exception;

	/**
	 * 메일 등록
	 *
	 * @return
	 * @throws Exception
	 */
	public void registEmail(HashMap<String,Object> param) throws Exception;

	/**
	 * 푸시 등록
	 *
	 * @return
	 * @throws Exception
	 */
	public void registPush(HashMap<String,Object> param) throws Exception;

	/**
	 * 푸시 등록
	 *`
	 * @return
	 * @throws Exception
	 */
	public void registPushUser(HashMap<String,Object> param) throws Exception;

	public void modifyPush(HashMap<String,Object> param) throws Exception;
	/**
	 * 푸시 이력 등록
	 *
	 * @return
	 * @throws Exception
	 */
	public void registPushHistory(HashMap<String,Object> param) throws Exception;

	public void registMessage(HashMap<String,Object> param) throws Exception;

	public void registMessageBill(HashMap<String,Object> param) throws Exception;

	void registChangHistory(HashMap<String,Object> param) throws Exception;

	List<HashMap<String,Object>> getCompanyTypeList() throws Exception;

	HashMap<String,Object> getUserOsType(HashMap<String,Object> param) throws Exception;

	HashMap<String, Object> getUserName(HashMap<String, Object> param);

	HashMap<String, Object> getUserCellphone(HashMap<String, Object> param);

	HashMap<String, Object> getBirthday(HashMap<String, Object> param);

	HashMap<String, Object> getCompanyApiInfo(HashMap<String, Object> param);
}
