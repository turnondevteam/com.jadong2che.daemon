package com.jadong2che.common.mapper;

import com.jadong2che.client.domain.*;
import com.jadong2che.common.domain.Member;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface MyPageMapper {


	/**
	 * 비밀번호 변경
	 *
	 * @return
	 * @throws Exception
	 */
	public void firstLoginChangePassword(HashMap<String, Object> param) throws Exception;

	void firstLoginChangeCount(HashMap<String, Object> param) throws Exception;

	Member getMember(HashMap<String,Object> param) throws Exception;

	int modifyMember(HashMap<String, Object> param) throws Exception;

	int modifyPassword(HashMap<String, Object> param) throws Exception;

}


