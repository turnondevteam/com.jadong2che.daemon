package com.jadong2che.common.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.jadong2che.common.domain.Member;

import java.util.List;

@Mapper
public interface MemberMapper {

	public Member findByMemberId(@Param("username") String username);

	public List<Member> getMenuList(int authorityIndex);
}
