package com.jadong2che.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.jadong2che.common.domain.Code;

@Mapper
public interface CodeMapper {

	/**
	 * 코드리스트
	 * 
	 * @param highRankCode
	 * @return
	 * @throws Exception
	 */
	public List<Code> getCodeList(@Param("highRankCode") String highRankCode) throws Exception;
	
}
