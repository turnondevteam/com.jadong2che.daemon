package com.jadong2che.common.service;


import java.util.List;


import com.jadong2che.common.domain.Code;
import com.jadong2che.common.mapper.CodeMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CodeService {

    @Autowired
    private CodeMapper codeMapper;

    public List<Code> getCodeList(@Param("highRankCode") String highRankCode) throws Exception{
        return codeMapper.getCodeList(highRankCode);
    }

}
