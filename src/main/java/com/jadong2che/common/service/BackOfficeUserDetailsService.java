package com.jadong2che.common.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.jadong2che.common.domain.AuthMember;
import com.jadong2che.common.domain.Member;
import com.jadong2che.common.mapper.MemberMapper;

@Service
public class BackOfficeUserDetailsService implements UserDetailsService {

	@Autowired
	private MemberMapper memberMapper;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Member member = memberMapper.findByMemberId(username);
//		member.setMenuList(memberMapper.getMenuList(member.getAuthorityIndex()));

		List<GrantedAuthority> authorities = buildAuthority("ROLE_"+ member.getCode());
		return buildUserForAuthentication(member, authorities);
	}
	
	private List<GrantedAuthority> buildAuthority(String userRole) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(userRole));
		return authorities;
	}
	
	private AuthMember buildUserForAuthentication(Member member, List<GrantedAuthority> authorities) {
		return new AuthMember(member, member.getId(), member.getPassword(), member.getMemberIndex(), member.getCompanyIndex(), member.getAffiliateName(), member.getTel(), member.getCellphone(),  true, true, true, true, authorities);
	}

}
