package com.jadong2che.common.service;

import java.util.HashMap;
import java.util.List;

import com.jadong2che.client.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jadong2che.common.mapper.CommonMapper;

@Service
public class CommonService {

	@Autowired
	private CommonMapper commonMapper;

	public List<PaymentMethod> getPaymentMethodList(int companyIndex) throws Exception {
		return commonMapper.getPaymentMethodList(companyIndex);
	}
	
	public List<Goods> getGoodsList(int companyIndex) throws Exception {
		return commonMapper.getGoodsList(companyIndex);
	}
	
	public List<Region> getSidoList() throws Exception {
		return commonMapper.getSidoList();
	}

	public List<Company> getCompanyList() throws Exception {
		return commonMapper.getCompanyList();
	}

	public List<Company> getGasCompanyList() throws Exception {
		return commonMapper.getGasCompanyList();
	}

	public List<User> getUserPushList() throws Exception {
		return commonMapper.getUserPushList();
	}

	public User getUserPush(int userIndex) throws Exception {
		return commonMapper.getUserPush(userIndex);
	}

	public List<User> getCompanyUserPushList(int companyIndex) throws Exception {
		return commonMapper.getCompanyUserPushList(companyIndex);
	}

	public void registPush(HashMap<String,Object> param) throws Exception{
		commonMapper.registPush(param);
	}

	public void registPushUser(HashMap<String,Object> param) throws Exception{
		commonMapper.registPushUser(param);
	}

	public void registEmail(HashMap<String,Object> param) throws Exception{
		commonMapper.registEmail(param);
	}

	public void modifyPush(HashMap<String,Object> param) throws Exception{
		commonMapper.modifyPush(param);
	}

	public void registPushHistory(HashMap<String,Object> param) throws Exception{
		commonMapper.registPushHistory(param);
	}

	public void registMessage(HashMap<String,Object> param) throws Exception{
		commonMapper.registMessage(param);
	}

	public void registMessageBill(HashMap<String,Object> param) throws Exception{
		commonMapper.registMessageBill(param);
	}

	public void registChangHistory(HashMap<String,Object> param) throws Exception{
		commonMapper.registChangHistory(param);
	}

	public List<HashMap<String,Object>> getCompanyTypeList() throws Exception {
		return commonMapper.getCompanyTypeList();
	}

	public HashMap<String,Object> getUserOsType(int userIndex) throws Exception{
		System.out.println("SERVICE=================================");
		System.out.println("param = " + userIndex);
		HashMap<String,Object> param = new HashMap<String, Object>();

		param.put("userIndex", userIndex);
		return commonMapper.getUserOsType(param);
	}

	public String getUserName(int userIndex){
		HashMap<String, Object> param = new HashMap<>();
		param.put("userIndex", userIndex);
		HashMap<String, Object> result = commonMapper.getUserName(param);

		if(result != null && result.get("userName") != null)
			return result.get("userName").toString();
		else
			return "";

	}

	public String getUserCellphone(int userIndex){
		HashMap<String, Object> param = new HashMap<>();
		param.put("userIndex", userIndex);
		HashMap<String, Object> result = commonMapper.getUserCellphone(param);

		if(result != null && result.get("cellphone") != null)
			return result.get("cellphone").toString();
		else
			return "";

	}

	public String getBirthDay(int userIndex){
		HashMap<String, Object> param = new HashMap<>();
		param.put("userIndex", userIndex);

		HashMap<String, Object> result = commonMapper.getBirthday(param);

		if(result != null && result.get("birthday") != null)
			return result.get("birthday").toString();
		else
			return "";

	}

	public HashMap<String, Object> getCompanyApiInfo(HashMap<String, Object> param) {
		return commonMapper.getCompanyApiInfo(param);
	}

}