package com.jadong2che.common.service;

import com.jadong2che.client.domain.*;
import com.jadong2che.common.domain.Member;
import com.jadong2che.common.mapper.CommonMapper;
import com.jadong2che.common.mapper.MyPageMapper;
import com.jadong2che.util.FileInfo;
import com.jadong2che.util.FileUpLoadUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Service
public class MyPageService {

	@Autowired
	private MyPageMapper myPageMapper;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public String firstLoginChangePassword(HashMap<String,Object> param) throws Exception{

		String result = "";

		try {

			param.put("password",passwordEncoder.encode(param.get("newPasswd").toString()));
			myPageMapper.firstLoginChangePassword(param);
			myPageMapper.firstLoginChangeCount(param);
			result = "success";

		} catch(Exception e) {
			e.printStackTrace();
			result = "fail";
		}

		return result;
	}

	public Member getMember(HashMap<String,Object> param) throws Exception {
		return myPageMapper.getMember(param);
	}

	public int modifyMember(HashMap<String,Object> param, HttpServletRequest request) throws Exception {

		int cnt = 0;

		try {

			MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
			MultipartFile profileImage = mRequest.getFile("profileImage");


			if(profileImage != null && profileImage.getSize() > 0){

				String realPath = "/home/deploy/resource/image/profile/" + param.get("companyIndex");

				FileInfo imgInfo = FileUpLoadUtil.uploadFormFile(request,profileImage, realPath, false, false, "");
				param.put("profileImage", imgInfo.getFilePath().replaceAll("/home/deploy/resource",""));
			}

			cnt = myPageMapper.modifyMember(param);

		}catch (Exception e){
			e.printStackTrace();
		}

		return cnt;
	}

	public int modifyPassword(HashMap<String,Object> param) throws Exception {
		param.put("newPasswd",passwordEncoder.encode(param.get("newPasswd").toString()));
		return myPageMapper.modifyPassword(param);
	}


}
