package com.jadong2che.common.service;

import com.jadong2che.common.domain.Dashboard;
import com.jadong2che.common.mapper.DashboardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

@Service
public class DashboardService {

	@Autowired
	private DashboardMapper dashboardMapper;
	
	public Dashboard getDashboard(HashMap<String,Object> param) throws Exception{

		Dashboard dashboard = new Dashboard();

		int jadong2cheUserCount = dashboardMapper.getDashboard2CheUserCount(param);
		int companyUserCount = dashboardMapper.getDashboardCompanyUserCount(param);
		int changeUserCount = dashboardMapper.getDashboardChangeUserCount(param);

		int jadong2cheThisWeekCount = dashboardMapper.getDashboard2CheUserJoinThisWeekCount(param);
		int jadong2cheLastWeekCount = dashboardMapper.getDashboard2CheUserJoinLastWeekCount(param);

		int companyThisWeekCount = dashboardMapper.getDashboardCompanyUserJoinThisWeekCount(param);
		int companyLastWeekCount = dashboardMapper.getDashboardCompanyUserJoinLastWeekCount(param);

		int changeThisWeekCount = dashboardMapper.getDashboardCompanyUserChangeThisWeekCount(param);
		int changeLastWeekCount = dashboardMapper.getDashboardCompanyUserChangeLastWeekCount(param);

		dashboard.setTotalUserCount(jadong2cheUserCount + companyUserCount - changeUserCount);

		dashboard.setJadong2cheUserCount(jadong2cheUserCount);
		dashboard.setCompanyUserCount(companyUserCount);
		dashboard.setChangeUserCount(changeUserCount);

		dashboard.setJadong2cheThisWeekCount(jadong2cheThisWeekCount);
		dashboard.setJadong2cheLastWeekCount(jadong2cheLastWeekCount);

		dashboard.setCompanyThisWeekCount(companyThisWeekCount);
		dashboard.setCompanyLastWeekCount(companyLastWeekCount);

		dashboard.setChangeThisWeekCount(companyThisWeekCount);
		dashboard.setChangeLastWeekCount(companyLastWeekCount);

		if((jadong2cheUserCount+companyUserCount) - (jadong2cheThisWeekCount+companyThisWeekCount) > 0){
			dashboard.setTotalUserRate(((jadong2cheUserCount+companyUserCount) - ((jadong2cheUserCount+companyUserCount) - (jadong2cheThisWeekCount+companyThisWeekCount)))/((jadong2cheUserCount+companyUserCount) - (jadong2cheThisWeekCount+companyThisWeekCount)));
		}else{
			dashboard.setTotalUserRate(0);
		}

		if((jadong2cheUserCount - jadong2cheThisWeekCount) > 0){
			dashboard.setJadong2cheUserRate((jadong2cheUserCount - (jadong2cheUserCount - jadong2cheThisWeekCount))/(jadong2cheUserCount - jadong2cheThisWeekCount));
		}else{
			dashboard.setJadong2cheUserRate(0);
		}

		if((companyUserCount - companyThisWeekCount) > 0){
			dashboard.setCompanyUserRate((companyUserCount - (companyUserCount - companyThisWeekCount))/(companyUserCount - companyThisWeekCount));
		}else{
			dashboard.setCompanyUserRate(0);
		}

		if((changeUserCount - changeThisWeekCount) > 0){
			dashboard.setChangeUserRate((changeUserCount - (changeUserCount - changeThisWeekCount))/(changeUserCount - changeThisWeekCount));
		}else{
			dashboard.setChangeUserRate(0);
		}


		dashboard.setJadong2cheList(dashboardMapper.getDashboardJadong2cheUser6month(param));
		dashboard.setCompanyList(dashboardMapper.getDashboardCompanyUser6month(param));

		param.put("requestStatus", "1");
		int requestCount1 = 0;
		if(dashboardMapper.getDashboardRequest(param) != null){
			requestCount1 = dashboardMapper.getDashboardRequest(param).getCount();
		}

		param.put("requestStatus", "3");
		int requestCount3 = 0;
		if(dashboardMapper.getDashboardRequest(param) != null){
			requestCount3 = dashboardMapper.getDashboardRequest(param).getCount();
		}

		int requestCount = requestCount1 + requestCount3;

		param.put("requestStatus", "5");
		int requestCount5 = 0;
		if(dashboardMapper.getDashboardRequest(param) != null){
			requestCount5 = dashboardMapper.getDashboardRequest(param).getCount();
		}

		param.put("requestStatus", "8");
		int requestCount8 = 0;
		if(dashboardMapper.getDashboardRequest(param) != null){
			requestCount8 = dashboardMapper.getDashboardRequest(param).getCount();
		}

		param.put("requestStatus", "9");
		int requestCount9 = 0;
		if(dashboardMapper.getDashboardRequest(param) != null){
			requestCount9 = dashboardMapper.getDashboardRequest(param).getCount();
		}


		int closeCount = requestCount8 + requestCount9;

		dashboard.setRequestCount(requestCount);
		dashboard.setCloseCount(closeCount);

		dashboard.setRequestCount1(requestCount1);
		dashboard.setRequestCount3(requestCount3);
		dashboard.setRequestCount5(requestCount5);
		dashboard.setRequestCount8(requestCount8);
		dashboard.setRequestCount9(requestCount9);


		param.put("billStatus","1");
		int billCount1 = 0;
		if(dashboardMapper.getDashboardBill(param) != null){
			billCount1 = dashboardMapper.getDashboardBill(param).getCount();
		}

		dashboard.setBillCount1(billCount1);

		param.put("billStatus","3");

		int billCount3 = 0;
		if(dashboardMapper.getDashboardBill(param) != null){
			billCount3 = dashboardMapper.getDashboardBill(param).getCount();
		}

		dashboard.setBillCount3(billCount3);

		param.put("billStatus","5");
		int billCount5 = 0;
		if(dashboardMapper.getDashboardBill(param) != null){
			billCount5 = dashboardMapper.getDashboardBill(param).getCount();
		}

		dashboard.setBillCount5(billCount5);

		param.put("billStatus","8");
		int billCount8 = 0;
		if(dashboardMapper.getDashboardBill(param) != null){
			billCount8 = dashboardMapper.getDashboardBill(param).getCount();
		}

		dashboard.setBillCount8(billCount8);

		param.put("billStatus","9");
		int billCount9 = 0;
		if(dashboardMapper.getDashboardBill(param) != null){
			billCount9 = dashboardMapper.getDashboardBill(param).getCount();
		}

		dashboard.setBillCount9(billCount9);






		dashboard.setBillCount3(billCount3);

		dashboard.setUserInfoChangeCount(dashboardMapper.getDashboardChageUserInfo(param));
		dashboard.setRequestInfoChangeCount(dashboardMapper.getDashboardChangeRequetInfo(param));

		dashboard.setGoodsList(dashboardMapper.getDashboardGoodsInfo(param));

		param.put("changeCode","CHG_PAY_METHD");
		int changeHistoryPayMethdCnt = dashboardMapper.getChangeLogCount(param);

		dashboard.setChangeHistoryPayMethdCnt(changeHistoryPayMethdCnt);

		param.put("changeCode","CHG_DRIV_ADDR");
		int changeHistorDrivAddrCnt = dashboardMapper.getChangeLogCount(param);

		dashboard.setChangeHistorDrivAddrCnt(changeHistorDrivAddrCnt);

		param.put("changeCode","CHG_MEM_INFO_TEL");
		int changeHistoryMemInfoTelCnt = dashboardMapper.getChangeLogCount(param);

		dashboard.setChangeHistoryMemInfoTelCnt(changeHistoryMemInfoTelCnt);

		param.put("changeCode","CHG_REQ_GOODS");
		int changeHistoryReqGoodsCnt = dashboardMapper.getChangeLogCount(param);

		dashboard.setChangeHistoryReqGoodsCnt (changeHistoryReqGoodsCnt);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
		SimpleDateFormat sdfm = new SimpleDateFormat("yyyy.MM");
        Calendar cal = Calendar.getInstance();
        String today = sdf.format(cal.getTime());
        String thisMonth = sdfm.format(cal.getTime());

        cal.setTime(cal.getTime());

        cal.add(Calendar.MONTH, -1);

        String monthDay = sdf.format(cal.getTime());

        dashboard.setToday(today);
        dashboard.setThisMonth(thisMonth);
        dashboard.setMonthDay(monthDay);

        dashboard.setCsAppList(dashboardMapper.getDashboardCsAppList(param));
        dashboard.setPushList(dashboardMapper.getDashboardPushList(param));

		return dashboard;
	}

	public List<HashMap<String,Object>> getNoticeList(HashMap<String,Object> param) throws Exception{
		return dashboardMapper.getNoticeList(param);
	}

	public List<HashMap<String,Object>> getGoodsList(HashMap<String,Object> param) throws Exception{
		return dashboardMapper.getGoodsList(param);
	}

	public HashMap<String,Object> getRequestCount(HashMap<String,Object> param) throws Exception{

		HashMap<String,Object> result = new HashMap<String, Object>();

		int jadong2cheUserCount = dashboardMapper.getDashboard2CheUserCount(param);
		int companyUserCount = dashboardMapper.getDashboardCompanyUserCount(param);
		int changeUserCount = dashboardMapper.getDashboardChangeUserCount(param);

		result.put("jadong2cheUserCount", jadong2cheUserCount);
		result.put("companyUserCount", companyUserCount);
		result.put("changeUserCount", changeUserCount);

		param.put("requestStatus", "1");
		int requestCount1 = 0;
		if(dashboardMapper.getDashboardRequest(param) != null){
			requestCount1 = dashboardMapper.getDashboardRequest(param).getCount();
		}
		result.put("requestCount1", requestCount1);


		param.put("requestStatus", "3");
		int requestCount3 = 0;
		if(dashboardMapper.getDashboardRequest(param) != null){
			requestCount3 = dashboardMapper.getDashboardRequest(param).getCount();
		}
		result.put("requestCount3", requestCount3);

		int requestCount = requestCount1 + requestCount3;

		param.put("requestStatus", "5");
		int requestCount5 = 0;
		if(dashboardMapper.getDashboardRequest(param) != null){
			requestCount5 = dashboardMapper.getDashboardRequest(param).getCount();
		}
		result.put("requestCount5", requestCount5);

		param.put("requestStatus", "8");
		int requestCount8 = 0;
		if(dashboardMapper.getDashboardRequest(param) != null){
			requestCount8 = dashboardMapper.getDashboardRequest(param).getCount();
		}
		result.put("requestCount8", requestCount8);

		param.put("requestStatus", "9");
		int requestCount9 = 0;
		if(dashboardMapper.getDashboardRequest(param) != null){
			requestCount9 = dashboardMapper.getDashboardRequest(param).getCount();
		}
		result.put("requestCount9", requestCount9);


		param.put("billStatus","1");
		int billCount1 = 0;
		if(dashboardMapper.getDashboardBill(param) != null){
			billCount1 = dashboardMapper.getDashboardBill(param).getCount();
		}
		result.put("billCount1", billCount1);


		param.put("billStatus","3");
		int billCount3 = 0;
		if(dashboardMapper.getDashboardBill(param) != null){
			billCount3 = dashboardMapper.getDashboardBill(param).getCount();
		}
		result.put("billCount3", billCount3);


		param.put("billStatus","5");
		int billCount5 = 0;
		if(dashboardMapper.getDashboardBill(param) != null){
			billCount5 = dashboardMapper.getDashboardBill(param).getCount();
		}
		result.put("billCount5", billCount5);


		param.put("billStatus","8");
		int billCount8 = 0;
		if(dashboardMapper.getDashboardBill(param) != null){
			billCount8 = dashboardMapper.getDashboardBill(param).getCount();
		}
		result.put("billCount8", billCount8);

		param.put("billStatus","9");
		int billCount9 = 0;
		if(dashboardMapper.getDashboardBill(param) != null){
			billCount9 = dashboardMapper.getDashboardBill(param).getCount();
		}
		result.put("billCount9", billCount9);


		result.put("csAppList",dashboardMapper.getDashboardCsAppList(param));

		return result;
	}
}
