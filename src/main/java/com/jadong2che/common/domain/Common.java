package com.jadong2che.common.domain;

import com.jadong2che.config.BackofficeConstant;

public class Common {

	// 페이지번호
	protected int pageIndex = 1;
	
	// MySql Limit Offset값
	protected int limitOffset = 0;
	
	// 전체 리스트 카운트
	protected int totPageCnt = 0;
	
	// 페이지 블럭 사이즈
	protected int pageSize = BackofficeConstant.PAGINGUTIL_PAGE_SIZE;
	
	// 조회 건수
	protected int totalCount = 0;
	
	// rownum
	protected int rn;

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		try{
			this.pageIndex = pageIndex;
		}catch(Exception e){
			pageIndex = 1;
		}
		limitOffset = (pageIndex - 1) * pageSize;
	}

	public int getTotPageCnt() {
		return totPageCnt;
	}

	public void setTotPageCnt(int totPageCnt) {
		this.totPageCnt = totPageCnt;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getRn() {
		return rn;
	}

	public void setRn(int rn) {
		this.rn = rn;
	}
	
	public int getStartCount() {
		return (this.pageIndex-1)*this.pageSize;
	}

	public int getLimitOffset() {
		return limitOffset;
	}

	public void setLimitOffset(int limitOffset) {
		this.limitOffset = limitOffset;
	}	
}
