package com.jadong2che.common.domain;

import java.util.Collection;

import org.json.simple.JSONObject;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class AuthMember extends User {
	
	private Member member;

	JSONObject menuList;

	public AuthMember(Member member, String username, String password, int memberIndex, int companyIndex, String affiliateName, String tel, String cellphone, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		
		this.member = member;
	}

	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member){
		this.member = member;
	}

	public JSONObject getMenuList() {
		return menuList;
	}

	public void setMenuList(JSONObject menuList) {
		this.menuList = menuList;
	}
}
