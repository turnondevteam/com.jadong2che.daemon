package com.jadong2che.common.domain;

import javax.activation.DataHandler;
import java.util.List;

public class Dashboard extends Common{

	private int cnt;

	private int totalUserCount;

	private int jadong2cheUserCount;

	private int companyUserCount;

	private int changeUserCount;

	private int jadong2cheThisWeekCount;

	private int jadong2cheLastWeekCount;

	private int companyThisWeekCount;

	private int companyLastWeekCount;

    private int changeThisWeekCount;

    private int changeLastWeekCount;

	private double totalUserRate;

	private double jadong2cheUserRate;

	private double companyUserRate;

    private double changeUserRate;

	private String month;

	private int count = 0;

	private String requestStatus;

	private String billStatus;

	private int requestCount;

	private int closeCount;

	private int billCount1;

    private int billCount3;

    private int billCount5;

    private int billCount8;

    private int billCount9;

	private int billCloseCount;

	private int userInfoChangeCount;

	private int requestInfoChangeCount;

	private int goodsIndex;

	private String goodsDivision;

	private String bigCategory;

	private String goodsName;

	private int cost;

	private int discountCost;

	private int sellCost;

	private String goodsImgThumbnail;



	private int requestCount1;

	private int requestCount3;

	private int requestCount5;

	private int requestCount8;

	private int requestCount9;


	private int changeHistoryIndex;

	private String code;

	private String tableName;

	private int index;

	private String desc;

	private String registId;

	private String registDatetime;

	private int getCnt() {
		return cnt;
	}

	private void setCnt(int cnt) {
		this.cnt = cnt;
	}

	private int changeHistoryPayMethdCnt;

	private int changeHistorDrivAddrCnt;

	private int changeHistoryMemInfoEmailCnt;

	private int changeHistoryMemInfoTelCnt;

	private int changeHistoryReqGoodsCnt;

	private String today;

	private String thisMonth;

	private String monthDay;

	private String userName;

	private String title;

	private String contents;

	private int csAppA;

	private int csAppB;

	private int csAppC;

	private int csAppD;

	private int pushCount;

	private String pushTypeName;

	private String requestStatusName;

    private List<Dashboard> jadong2cheList;

	private List<Dashboard> companyList;

    private List<Dashboard> goodsList;

    private List<Dashboard> csAppList;

    private List<Dashboard> pushList;


    public int getTotalUserCount() {
        return totalUserCount;
    }

    public void setTotalUserCount(int totalUserCount) {
        this.totalUserCount = totalUserCount;
    }

    public int getJadong2cheUserCount() {
        return jadong2cheUserCount;
    }

    public void setJadong2cheUserCount(int jadong2cheUserCount) {
        this.jadong2cheUserCount = jadong2cheUserCount;
    }

    public int getCompanyUserCount() {
        return companyUserCount;
    }

    public void setCompanyUserCount(int companyUserCount) {
        this.companyUserCount = companyUserCount;
    }

    public int getChangeUserCount() {
        return changeUserCount;
    }

    public void setChangeUserCount(int changeUserCount) {
        this.changeUserCount = changeUserCount;
    }

    public int getJadong2cheThisWeekCount() {
        return jadong2cheThisWeekCount;
    }

    public void setJadong2cheThisWeekCount(int jadong2cheThisWeekCount) {
        this.jadong2cheThisWeekCount = jadong2cheThisWeekCount;
    }

    public int getJadong2cheLastWeekCount() {
        return jadong2cheLastWeekCount;
    }

    public void setJadong2cheLastWeekCount(int jadong2cheLastWeekCount) {
        this.jadong2cheLastWeekCount = jadong2cheLastWeekCount;
    }

    public int getCompanyThisWeekCount() {
        return companyThisWeekCount;
    }

    public void setCompanyThisWeekCount(int companyThisWeekCount) {
        this.companyThisWeekCount = companyThisWeekCount;
    }

    public int getCompanyLastWeekCount() {
        return companyLastWeekCount;
    }

    public void setCompanyLastWeekCount(int companyLastWeekCount) {
        this.companyLastWeekCount = companyLastWeekCount;
    }

    public int getChangeThisWeekCount() {
        return changeThisWeekCount;
    }

    public void setChangeThisWeekCount(int changeThisWeekCount) {
        this.changeThisWeekCount = changeThisWeekCount;
    }

    public int getChangeLastWeekCount() {
        return changeLastWeekCount;
    }

    public void setChangeLastWeekCount(int changeLastWeekCount) {
        this.changeLastWeekCount = changeLastWeekCount;
    }

    public double getTotalUserRate() {
        return totalUserRate;
    }

    public void setTotalUserRate(double totalUserRate) {
        this.totalUserRate = totalUserRate;
    }

    public double getJadong2cheUserRate() {
        return jadong2cheUserRate;
    }

    public void setJadong2cheUserRate(double jadong2cheUserRate) {
        this.jadong2cheUserRate = jadong2cheUserRate;
    }

    public double getCompanyUserRate() {
        return companyUserRate;
    }

    public void setCompanyUserRate(double companyUserRate) {
        this.companyUserRate = companyUserRate;
    }

    public double getChangeUserRate() {
        return changeUserRate;
    }

    public void setChangeUserRate(double changeUserRate) {
        this.changeUserRate = changeUserRate;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public int getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
    }

    public int getCloseCount() {
        return closeCount;
    }

    public void setCloseCount(int closeCount) {
        this.closeCount = closeCount;
    }

    public int getBillCount1() {
        return billCount1;
    }

    public void setBillCount1(int billCount1) {
        this.billCount1 = billCount1;
    }

    public int getBillCount3() {
        return billCount3;
    }

    public void setBillCount3(int billCount3) {
        this.billCount3 = billCount3;
    }

    public int getBillCount5() {
        return billCount5;
    }

    public void setBillCount5(int billCount5) {
        this.billCount5 = billCount5;
    }

    public int getBillCount8() {
        return billCount8;
    }

    public void setBillCount8(int billCount8) {
        this.billCount8 = billCount8;
    }

    public int getBillCount9() {
        return billCount9;
    }

    public void setBillCount9(int billCount9) {
        this.billCount9 = billCount9;
    }

    public int getBillCloseCount() {
        return billCloseCount;
    }

    public void setBillCloseCount(int billCloseCount) {
        this.billCloseCount = billCloseCount;
    }

    public int getUserInfoChangeCount() {
        return userInfoChangeCount;
    }

    public void setUserInfoChangeCount(int userInfoChangeCount) {
        this.userInfoChangeCount = userInfoChangeCount;
    }

    public int getRequestInfoChangeCount() {
        return requestInfoChangeCount;
    }

    public void setRequestInfoChangeCount(int requestInfoChangeCount) {
        this.requestInfoChangeCount = requestInfoChangeCount;
    }

    public int getGoodsIndex() {
        return goodsIndex;
    }

    public void setGoodsIndex(int goodsIndex) {
        this.goodsIndex = goodsIndex;
    }

    public String getGoodsDivision() {
        return goodsDivision;
    }

    public void setGoodsDivision(String goodsDivision) {
        this.goodsDivision = goodsDivision;
    }

    public String getBigCategory() {
        return bigCategory;
    }

    public void setBigCategory(String bigCategory) {
        this.bigCategory = bigCategory;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getDiscountCost() {
        return discountCost;
    }

    public void setDiscountCost(int discountCost) {
        this.discountCost = discountCost;
    }

    public int getSellCost() {
        return sellCost;
    }

    public void setSellCost(int sellCost) {
        this.sellCost = sellCost;
    }

    public String getGoodsImgThumbnail() {
        return goodsImgThumbnail;
    }

    public void setGoodsImgThumbnail(String goodsImgThumbnail) {
        this.goodsImgThumbnail = goodsImgThumbnail;
    }

    public int getRequestCount1() {
        return requestCount1;
    }

    public void setRequestCount1(int requestCount1) {
        this.requestCount1 = requestCount1;
    }

    public int getRequestCount3() {
        return requestCount3;
    }

    public void setRequestCount3(int requestCount3) {
        this.requestCount3 = requestCount3;
    }

    public int getRequestCount5() {
        return requestCount5;
    }

    public void setRequestCount5(int requestCount5) {
        this.requestCount5 = requestCount5;
    }

    public int getRequestCount8() {
        return requestCount8;
    }

    public void setRequestCount8(int requestCount8) {
        this.requestCount8 = requestCount8;
    }

    public int getRequestCount9() {
        return requestCount9;
    }

    public void setRequestCount9(int requestCount9) {
        this.requestCount9 = requestCount9;
    }

    public int getChangeHistoryIndex() {
        return changeHistoryIndex;
    }

    public void setChangeHistoryIndex(int changeHistoryIndex) {
        this.changeHistoryIndex = changeHistoryIndex;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRegistId() {
        return registId;
    }

    public void setRegistId(String registId) {
        this.registId = registId;
    }

    public String getRegistDatetime() {
        return registDatetime;
    }

    public void setRegistDatetime(String registDatetime) {
        this.registDatetime = registDatetime;
    }

    public int getChangeHistoryPayMethdCnt() {
        return changeHistoryPayMethdCnt;
    }

    public void setChangeHistoryPayMethdCnt(int changeHistoryPayMethdCnt) {
        this.changeHistoryPayMethdCnt = changeHistoryPayMethdCnt;
    }

    public int getChangeHistorDrivAddrCnt() {
        return changeHistorDrivAddrCnt;
    }

    public void setChangeHistorDrivAddrCnt(int changeHistorDrivAddrCnt) {
        this.changeHistorDrivAddrCnt = changeHistorDrivAddrCnt;
    }

    public int getChangeHistoryMemInfoEmailCnt() {
        return changeHistoryMemInfoEmailCnt;
    }

    public void setChangeHistoryMemInfoEmailCnt(int changeHistoryMemInfoEmailCnt) {
        this.changeHistoryMemInfoEmailCnt = changeHistoryMemInfoEmailCnt;
    }

    public int getChangeHistoryMemInfoTelCnt() {
        return changeHistoryMemInfoTelCnt;
    }

    public void setChangeHistoryMemInfoTelCnt(int changeHistoryMemInfoTelCnt) {
        this.changeHistoryMemInfoTelCnt = changeHistoryMemInfoTelCnt;
    }

    public int getChangeHistoryReqGoodsCnt() {
        return changeHistoryReqGoodsCnt;
    }

    public void setChangeHistoryReqGoodsCnt(int changeHistoryReqGoodsCnt) {
        this.changeHistoryReqGoodsCnt = changeHistoryReqGoodsCnt;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public String getThisMonth() {
        return thisMonth;
    }

    public void setThisMonth(String thisMonth) {
        this.thisMonth = thisMonth;
    }

    public String getMonthDay() {
        return monthDay;
    }

    public void setMonthDay(String monthDay) {
        this.monthDay = monthDay;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public int getCsAppA() {
        return csAppA;
    }

    public void setCsAppA(int csAppA) {
        this.csAppA = csAppA;
    }

    public int getCsAppB() {
        return csAppB;
    }

    public void setCsAppB(int csAppB) {
        this.csAppB = csAppB;
    }

    public int getCsAppC() {
        return csAppC;
    }

    public void setCsAppC(int csAppC) {
        this.csAppC = csAppC;
    }

    public int getCsAppD() {
        return csAppD;
    }

    public void setCsAppD(int csAppD) {
        this.csAppD = csAppD;
    }

    public int getPushCount() {
        return pushCount;
    }

    public void setPushCount(int pushCount) {
        this.pushCount = pushCount;
    }

    public String getPushTypeName() {
        return pushTypeName;
    }

    public void setPushTypeName(String pushTypeName) {
        this.pushTypeName = pushTypeName;
    }

    public String getRequestStatusName() {
        return requestStatusName;
    }

    public void setRequestStatusName(String requestStatusName) {
        this.requestStatusName = requestStatusName;
    }

    public List<Dashboard> getJadong2cheList() {
        return jadong2cheList;
    }

    public void setJadong2cheList(List<Dashboard> jadong2cheList) {
        this.jadong2cheList = jadong2cheList;
    }

    public List<Dashboard> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<Dashboard> companyList) {
        this.companyList = companyList;
    }

    public List<Dashboard> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<Dashboard> goodsList) {
        this.goodsList = goodsList;
    }

    public List<Dashboard> getCsAppList() {
        return csAppList;
    }

    public void setCsAppList(List<Dashboard> csAppList) {
        this.csAppList = csAppList;
    }

    public List<Dashboard> getPushList() {
        return pushList;
    }

    public void setPushList(List<Dashboard> pushList) {
        this.pushList = pushList;
    }
}
