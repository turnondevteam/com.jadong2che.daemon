package com.jadong2che.common.domain;

import java.util.List;

public class Member extends Common{

	// 회원 일련번호
	private int memberIndex;
	
	// 고객사 일련번호
	private int companyIndex;
	
	// 권한 일련번호
	private int authorityIndex;
	
	// id
	private String id;
	
	// 이름
	private String name;
	
	// 비밀번호
	private String password;
	
	// 등급
	private int grade;
	
	// 가맹점 이름
	private String affiliateName;
	
	// 상위 고객사 일련번호
	private int highRankCompanyIndex;
	
	// 전화번호
	private String tel;
	
	// 휴대폰
	private String cellphone;
	
	// 주소
	private String address;
	
	// 계정만료여부
	private int isAccountNonExpired;
	
	// 계정잠김여부
	private int isAccountNonLocked;
	
	// 계정패스워드만료여부
	private int isCredentialsNonExpired;
	
	// 계정사용여부
	private int isEnabled;
	
	// 마지막 로그인 일시
	private String lastLoginDateTime;
	
	//
	private String code;
	
	// 등록 id
	private String registId;
	
	// 등록 일시
	private String registDatetime;
	
	// 수정 id
	private String modifIid;
	
	// 수정 일시
	private String modifyDatetime;

	private int menuIndex;

	private int parentMenuIndex;

	private String menuName;

	private String menuCode;

	private String url;

	private String useYn;

	private String displayYn;

	private int sort;

	private String type;

	private List<Member> menuList;

	private String companyName;

	private int firstLogin;

	private String email;

	private String profileImage;

	private String authorityCode;

	private int authorityLevel;

	public int getMemberIndex() {
		return memberIndex;
	}

	public void setMemberIndex(int memberIndex) {
		this.memberIndex = memberIndex;
	}

	public int getCompanyIndex() {
		return companyIndex;
	}

	public void setCompanyIndex(int companyIndex) {
		this.companyIndex = companyIndex;
	}

	public int getAuthorityIndex() {
		return authorityIndex;
	}

	public void setAuthorityIndex(int authorityIndex) {
		this.authorityIndex = authorityIndex;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getAffiliateName() {
		return affiliateName;
	}

	public void setAffiliateName(String affiliateName) {
		this.affiliateName = affiliateName;
	}

	public int getHighRankCompanyIndex() {
		return highRankCompanyIndex;
	}

	public void setHighRankCompanyIndex(int highRankCompanyIndex) {
		this.highRankCompanyIndex = highRankCompanyIndex;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getIsAccountNonExpired() {
		return isAccountNonExpired;
	}

	public void setIsAccountNonExpired(int isAccountNonExpired) {
		this.isAccountNonExpired = isAccountNonExpired;
	}

	public int getIsAccountNonLocked() {
		return isAccountNonLocked;
	}

	public void setIsAccountNonLocked(int isAccountNonLocked) {
		this.isAccountNonLocked = isAccountNonLocked;
	}

	public int getIsCredentialsNonExpired() {
		return isCredentialsNonExpired;
	}

	public void setIsCredentialsNonExpired(int isCredentialsNonExpired) {
		this.isCredentialsNonExpired = isCredentialsNonExpired;
	}

	public int getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(int isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getLastLoginDateTime() {
		return lastLoginDateTime;
	}

	public void setLastLoginDateTime(String lastLoginDateTime) {
		this.lastLoginDateTime = lastLoginDateTime;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getRegistId() {
		return registId;
	}

	public void setRegistId(String registId) {
		this.registId = registId;
	}

	public String getRegistDatetime() {
		return registDatetime;
	}

	public void setRegistDatetime(String registDatetime) {
		this.registDatetime = registDatetime;
	}

	public String getModifIid() {
		return modifIid;
	}

	public void setModifIid(String modifIid) {
		this.modifIid = modifIid;
	}

	public String getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(String modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public int getMenuIndex() {
		return menuIndex;
	}

	public void setMenuIndex(int menuIndex) {
		this.menuIndex = menuIndex;
	}

	public int getParentMenuIndex() {
		return parentMenuIndex;
	}

	public void setParentMenuIndex(int parentMenuIndex) {
		this.parentMenuIndex = parentMenuIndex;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getDisplayYn() {
		return displayYn;
	}

	public void setDisplayYn(String displayYn) {
		this.displayYn = displayYn;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public List<Member> getMenuList() {
		return menuList;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setMenuList(List<Member> menuList) {
		this.menuList = menuList;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(int firstLogin) {
		this.firstLogin = firstLogin;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getAuthorityCode() {
		return authorityCode;
	}

	public void setAuthorityCode(String authorityCode) {
		this.authorityCode = authorityCode;
	}

	public int getAuthorityLevel() {
		return authorityLevel;
	}

	public void setAuthorityLevel(int authorityLevel) {
		this.authorityLevel = authorityLevel;
	}

	@Override
	public String toString() {
		return "Member [memberIndex=" + memberIndex + ", companyIndex=" + companyIndex + ", authorityIndex="
				+ authorityIndex + ", id=" + id + ", name=" + name + ", password=" + password + ", grade=" + grade
				+ ", affiliateName=" + affiliateName + ", highRankCompanyIndex=" + highRankCompanyIndex + ", tel=" + tel
				+ ", cellphone=" + cellphone + ", address=" + address + ", isAccountNonExpired=" + isAccountNonExpired
				+ ", isAccountNonLocked=" + isAccountNonLocked + ", isCredentialsNonExpired=" + isCredentialsNonExpired
				+ ", isEnabled=" + isEnabled + ", lastLoginDateTime=" + lastLoginDateTime + ", registId=" + registId
				+ ", registDatetime=" + registDatetime + ", modifIid=" + modifIid + ", modifyDatetime=" + modifyDatetime
				+ "]";
	}
	
	public Member() {}

	public Member(int companyIndex, int authorityIndex, String id, String name, String password) {
		super();
		this.companyIndex = companyIndex;
		this.authorityIndex = authorityIndex;
		this.id = id;
		this.name = name;
		this.password = password;
	}



}
