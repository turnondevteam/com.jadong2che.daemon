package com.jadong2che.common.domain;

public class Code {

	private String code;
	
	private String value;
	
	private String highRankCode;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getHighRankCode() {
		return highRankCode;
	}

	public void setHighRankCode(String highRankCode) {
		this.highRankCode = highRankCode;
	}

	@Override
	public String toString() {
		return "Code [code=" + code + ", value=" + value + ", highRankCode=" + highRankCode + "]";
	}

}
