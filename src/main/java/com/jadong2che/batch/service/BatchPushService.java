package com.jadong2che.batch.service;

import com.jadong2che.batch.mapper.BatchPushMapper;
import com.jadong2che.client.domain.Push;
import com.jadong2che.common.service.CommonService;
import com.jadong2che.util.PushUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Service
public class BatchPushService {

    @Autowired
    private BatchPushMapper batchPushMapper;

    @Autowired
	private CommonService commonService;

    public  void sendPush() throws Exception{

        List<Push> pushInfoList = batchPushMapper.getPushInfoList();

        if(pushInfoList != null && pushInfoList.size() >0){

            for(int i=0; i<pushInfoList.size(); i++){

                Push push = pushInfoList.get(i);

                if(push.getUserIndex() > 0 && push.getAppToken() != null){

                    HashMap<String, Object> param = new HashMap<String, Object>();

                    String appToken = push.getAppToken();
                    String sendTitle = push.getSendTitle();
                    String sendContents = push.getSendContents();
                    String messageCode = push.getPushType();
                    Integer pushIndex = push.getPushIndex();
                    Integer userIndex = push.getUserIndex();
                    Integer contentsIndex = push.getContentsIndex();
                    Integer billIndex = push.getBillIndex();
                    String url = push.getUrl();
                    String pushTitle = "";

                    if(push.getPushType().equals("BILL")){
                        pushTitle = "청구서";
//                        url = "bill?title="+ pushTitle +"&billIndex="+ billIndex +"&billMonthlyIndex=" + contentsIndex;
                        url = "bill?billIndex="+ billIndex +"&billMonthlyIndex=" + contentsIndex;

                    }

                    if(push.getPushType().contains("JADONG2CHE")){
                        pushTitle = "자동이체";
                        url = "application/detail?requestIndex=" + contentsIndex;
                    }

                    System.out.println("==============================================================================================================");
                    System.out.println("URL = " + url);
                    System.out.println("==============================================================================================================");
                    String osType = "A";
                    HashMap osMap = commonService.getUserOsType(userIndex);

                    if(osMap != null && osMap.get("osType") != null && !osMap.get("osType").equals("")){
                        osType = osMap.get("osType").toString();
                    }

                    HashMap<String, Object> result = PushUtil.pushFCMNotification(appToken, sendTitle, sendContents, messageCode, pushIndex.toString(), userIndex.toString(), url, osType, pushTitle);

                    String successYn = "";

                    if (Integer.parseInt(result.get("success").toString()) > 0) {
                        successYn = "Y";
                    } else {
                        successYn = "N";
                    }

                    param.put("pushIndex", pushIndex);
                    param.put("userIndex", userIndex);
                    param.put("successYn", successYn);

                    param.put("messageCode", messageCode);
                    param.put("contentsIndex", contentsIndex);
                    param.put("billIndex", billIndex);

                    batchPushMapper.modifyPush(param);
                    batchPushMapper.registPushHistory(param);
//                    commonService.registMessage(param);

                    if(messageCode.equals("BILL")){

//                        commonService.registMessageBill(param);

                        batchPushMapper.modifyPushBill(param);

                        batchPushMapper.modifyPushBillMonthly(param);

                    }else if(messageCode.contains("BILL.PAYMENT")){
                        batchPushMapper.modifyPaymentDetail(param);
                    }


                }
            }
        }

        /*
        if(pushInfoList != null && pushInfoList.size() >0){

            List<Push> pushList = new ArrayList<Push>();

            for(int i=0; i<pushInfoList.size(); i++){

                Push push = pushInfoList.get(i);

                if(push.getUserIndex() == 0){
                    pushList = batchPushMapper.getPushAllUserList();
                }else{
                    pushList = batchPushMapper.getPushList();
                }

                if(pushList != null && pushList.size() >0) {
                    for (int j = 0; j < pushList.size(); j++) {
                        HashMap<String, Object> param = new HashMap<String, Object>();

                        String appToken = pushList.getAppToken();
                        String sendTitle = pushList.getSendTitle();
                        String sendContents = pushList.getSendContents();
                        String messageCode = pushList.getPushType();
                        Integer pushIndex = pushList.getPushIndex();
                        Integer userIndex = pushList.getUserIndex();
                        Integer contentsIndex = pushList.getContentsIndex();
                        Integer billIndex = pushList.getBillIndex();

                        HashMap<String, Object> result = PushUtil.pushFCMNotification(appToken, sendTitle, sendContents, messageCode, pushIndex.toString(), userIndex.toString());

                        String successYn = "";

                        if (Integer.parseInt(result.get("success").toString()) > 0) {
                            successYn = "Y";
                        } else {
                            successYn = "N";
                        }

                        param.put("pushIndex", pushIndex);
                        param.put("userIndex", userIndex);
                        param.put("successYn", successYn);

                        param.put("messageCode", messageCode);
                        param.put("contentsIndex", contentsIndex);
                        param.put("billIndex", billIndex);

                        batchPushMapper.modifyPush(param);
                        batchPushMapper.registPushHistory(param);
                        commonService.registMessage(param);

                        if(messageCode.equals("BILL")){

                            commonService.registMessageBill(param);

                            batchPushMapper.modifyPushBill(param);

                            batchPushMapper.modifyPushBillMonthly(param);

                        }else if(messageCode.contains("BILL.PAYMENT")){
                            batchPushMapper.modifyPaymentDetail(param);
                        }

                    }
                }
            }
        }
        */
    }
}
