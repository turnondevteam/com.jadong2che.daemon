package com.jadong2che.batch.service;

import com.jadong2che.batch.mapper.BatchMailMapper;
import com.jadong2che.client.domain.Mail;
import com.jadong2che.common.mapper.DashboardMapper;
import com.jadong2che.common.service.CommonService;
import com.jadong2che.util.MailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


@Service
public class BatchMailService{

    @Autowired
    private BatchMailMapper batchMailMapper;

    @Autowired
    private DashboardMapper dashboardMapper;

    @Autowired
	private CommonService commonService;

    public void sendMail() throws Exception {

        List<Mail> mailUserList = batchMailMapper.getMailUserList();


        for(int i=0; i<mailUserList.size(); i++){

            String htmlText = "";
            String title = "";
            String content = "";

            Mail sendMailInfo = mailUserList.get(i);

            HashMap<String,Object> param = new HashMap<String,Object>();

            param.put("companyIndex", sendMailInfo.getCompanyIndex());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
            Calendar cal = Calendar.getInstance();
            String monthDay = sdf.format(cal.getTime());
            String today = sdf.format(cal.getTime());

            param.put("requestStatus", "1");
            int requestCount1 = 0;
            if(dashboardMapper.getDashboardRequest(param) != null){
                requestCount1 = dashboardMapper.getDashboardRequest(param).getCount();
            }

            param.put("requestStatus", "3");
            int requestCount3 = 0;
            if(dashboardMapper.getDashboardRequest(param) != null){
                requestCount3 = dashboardMapper.getDashboardRequest(param).getCount();
            }

            param.put("requestStatus", "5");
            int requestCount5 = 0;
            if(dashboardMapper.getDashboardRequest(param) != null){
                requestCount5 = dashboardMapper.getDashboardRequest(param).getCount();
            }

            param.put("requestStatus", "8");
            int requestCount8 = 0;
            if(dashboardMapper.getDashboardRequest(param) != null){
                requestCount8 = dashboardMapper.getDashboardRequest(param).getCount();
            }

            param.put("requestStatus", "9");
            int requestCount9 = 0;
            if(dashboardMapper.getDashboardRequest(param) != null){
                requestCount9 = dashboardMapper.getDashboardRequest(param).getCount();
            }

            int requestCount = requestCount1 + requestCount3;

            int closeCount = requestCount8 + requestCount9;


            param.put("billStatus","1");
            int billCount1 = 0;
            if(dashboardMapper.getDashboardBill(param) != null){
                billCount1 = dashboardMapper.getDashboardBill(param).getCount();
            }

            param.put("billStatus","3");

            int billCount3 = 0;
            if(dashboardMapper.getDashboardBill(param) != null){
                billCount3 = dashboardMapper.getDashboardBill(param).getCount();
            }

            param.put("billStatus","5");
            int billCount5 = 0;
            if(dashboardMapper.getDashboardBill(param) != null){
                billCount5 = dashboardMapper.getDashboardBill(param).getCount();
            }

            param.put("billStatus","8");
            int billCount8 = 0;
            if(dashboardMapper.getDashboardBill(param) != null){
                billCount8 = dashboardMapper.getDashboardBill(param).getCount();
            }

            param.put("billStatus","9");
            int billCount9 = 0;
            if(dashboardMapper.getDashboardBill(param) != null){
                billCount9 = dashboardMapper.getDashboardBill(param).getCount();
            }

            param.put("changeCode","CHG_PAY_METHD");
            int changeHistoryPayMethdCnt = dashboardMapper.getChangeLogCount(param);

            param.put("changeCode","CHG_DRIV_ADDR");
            int changeHistorDrivAddrCnt = dashboardMapper.getChangeLogCount(param);

            param.put("changeCode","CHG_MEM_INFO_TEL");
            int changeHistoryMemInfoTelCnt = dashboardMapper.getChangeLogCount(param);

            param.put("changeCode","CHG_REQ_GOODS");
            int changeHistoryReqGoodsCnt = dashboardMapper.getChangeLogCount(param);


            htmlText += "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
            htmlText += "<html xmlns='http://www.w3.org/1999/xhtml'>";
            htmlText += "<head>";
            htmlText += "    <meta name='viewport' content='width=device-width' />";
            htmlText += "    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
            htmlText += "    ";
            htmlText += "    <style type='text/css'>";
            htmlText += "	/* -------------------------------------";
            htmlText += "    GLOBAL";
            htmlText += "    A very basic CSS reset";
            htmlText += "	------------------------------------- */";
            htmlText += "	* {";
            htmlText += "	    margin: 0;";
            htmlText += "	    padding: 0;";
            htmlText += "	    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;";
            htmlText += "	    box-sizing: border-box;";
            htmlText += "	    font-size: 14px;";
            htmlText += "	}";
            htmlText += "	img {";
            htmlText += "	    max-width: 100%;";
            htmlText += "	}";
            htmlText += "	body {";
            htmlText += "	    -webkit-font-smoothing: antialiased;";
            htmlText += "	    -webkit-text-size-adjust: none;";
            htmlText += "	    width: 100% !important;";
            htmlText += "	    height: 100%;";
            htmlText += "	    line-height: 1.6;";
            htmlText += "	}";
            htmlText += "	table td {";
            htmlText += "	    vertical-align: top;";
            htmlText += "	}";
            htmlText += "	body {";
            htmlText += "	    background-color: #f6f6f6;";
            htmlText += "	}";
            htmlText += "	.body-wrap {";
            htmlText += "	    background-color: #f6f6f6;";
            htmlText += "	    width: 100%;";
            htmlText += "	}";
            htmlText += "	.container {";
            htmlText += "	    display: block !important;";
            htmlText += "	    max-width: 600px !important;";
            htmlText += "	    margin: 0 auto !important;";
            htmlText += "	    clear: both !important;";
            htmlText += "	}";
            htmlText += "	.content {";
            htmlText += "	    max-width: 600px;";
            htmlText += "	    margin: 0 auto;";
            htmlText += "	    display: block;";
            htmlText += "	    padding: 20px;";
            htmlText += "	}";
            htmlText += "	.main {";
            htmlText += "	    background: #fff;";
            htmlText += "	    border: 1px solid #e9e9e9;";
            htmlText += "	    border-radius: 3px;";
            htmlText += "	}";
            htmlText += "	.content-wrap {";
            htmlText += "	    padding: 20px;";
            htmlText += "	}";
            htmlText += "	.content-block {";
            htmlText += "	    padding: 0 0 20px;";
            htmlText += "	}";
            htmlText += "	.header {";
            htmlText += "	    width: 100%;";
            htmlText += "	    margin-bottom: 20px;";
            htmlText += "	}";
            htmlText += "	.footer {";
            htmlText += "	    width: 100%;";
            htmlText += "	    clear: both;";
            htmlText += "	    color: #999;";
            htmlText += "	    padding: 20px;";
            htmlText += "	}";
            htmlText += "	.footer a {";
            htmlText += "	    color: #999;";
            htmlText += "	}";
            htmlText += "	.footer p, .footer a, .footer unsubscribe, .footer td {";
            htmlText += "	    font-size: 12px;";
            htmlText += "	}";
            htmlText += "	h1, h2, h3 {";
            htmlText += "	    font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;";
            htmlText += "	    color: #000;";
            htmlText += "	    margin: 40px 0 0;";
            htmlText += "	    line-height: 1.2;";
            htmlText += "	    font-weight: 400;";
            htmlText += "	}";
            htmlText += "	h1 {";
            htmlText += "	    font-size: 32px;";
            htmlText += "	    font-weight: 500;";
            htmlText += "	}";
            htmlText += "	h2 {";
            htmlText += "	    font-size: 24px;";
            htmlText += "	}";
            htmlText += "	h3 {";
            htmlText += "	    font-size: 18px;";
            htmlText += "	}";
            htmlText += "	h4 {";
            htmlText += "	    font-size: 14px;";
            htmlText += "	    font-weight: 600;";
            htmlText += "	}";
            htmlText += "	p, ul, ol {";
            htmlText += "	    margin-bottom: 10px;";
            htmlText += "	    font-weight: normal;";
            htmlText += "	}";
            htmlText += "	p li, ul li, ol li {";
            htmlText += "	    margin-left: 5px;";
            htmlText += "	    list-style-position: inside;";
            htmlText += "	}";
            htmlText += "	a {";
            htmlText += "	    color: #1ab394;";
            htmlText += "	    text-decoration: underline;";
            htmlText += "	}";
            htmlText += "	.btn-primary {";
            htmlText += "	    text-decoration: none;";
            htmlText += "	    color: #FFF;";
            htmlText += "	    background-color: #1ab394;";
            htmlText += "	    border: solid #1ab394;";
            htmlText += "	    border-width: 5px 10px;";
            htmlText += "	    line-height: 2;";
            htmlText += "	    font-weight: bold;";
            htmlText += "	    text-align: center;";
            htmlText += "	    cursor: pointer;";
            htmlText += "	    display: inline-block;";
            htmlText += "	    border-radius: 5px;";
            htmlText += "	    text-transform: capitalize;";
            htmlText += "	}";
            htmlText += "	.last {";
            htmlText += "	    margin-bottom: 0;";
            htmlText += "	}";
            htmlText += "	.first {";
            htmlText += "	    margin-top: 0;";
            htmlText += "	}";
            htmlText += "	.aligncenter {";
            htmlText += "	    text-align: center;";
            htmlText += "	}";
            htmlText += "	.alignright {";
            htmlText += "	    text-align: right;";
            htmlText += "	}";
            htmlText += "	.alignleft {";
            htmlText += "	    text-align: left;";
            htmlText += "	}";
            htmlText += "	.clear {";
            htmlText += "	    clear: both;";
            htmlText += "	}";
            htmlText += "	.alert {";
            htmlText += "	    font-size: 16px;";
            htmlText += "	    color: #fff;";
            htmlText += "	    font-weight: 500;";
            htmlText += "	    padding: 20px;";
            htmlText += "	    text-align: center;";
            htmlText += "	    border-radius: 3px 3px 0 0;";
            htmlText += "	}";
            htmlText += "	.alert a {";
            htmlText += "	    color: #fff;";
            htmlText += "	    text-decoration: none;";
            htmlText += "	    font-weight: 500;";
            htmlText += "	    font-size: 16px;";
            htmlText += "	}";
            htmlText += "	.alert.alert-warning {";
            htmlText += "	    background: #f8ac59;";
            htmlText += "	}";
            htmlText += "	.alert.alert-bad {";
            htmlText += "	    background: #ed5565;";
            htmlText += "	}";
            htmlText += "	.alert.alert-good {";
            htmlText += "	    background: #1ab394;";
            htmlText += "	}";
            htmlText += "	.invoice {";
            htmlText += "	    margin: 40px auto;";
            htmlText += "	    text-align: left;";
            htmlText += "	    width: 80%;";
            htmlText += "	}";
            htmlText += "	.invoice td {";
            htmlText += "	    padding: 5px 0;";
            htmlText += "	}";
            htmlText += "	.invoice .invoice-items {";
            htmlText += "	    width: 100%;";
            htmlText += "	}";
            htmlText += "	.invoice .invoice-items td {";
            htmlText += "	    border-top: #eee 1px solid;";
            htmlText += "	}";
            htmlText += "	.invoice .invoice-items .total td {";
            htmlText += "	    border-top: 2px solid #333;";
            htmlText += "	    border-bottom: 2px solid #333;";
            htmlText += "	    font-weight: 700;";
            htmlText += "	}";
            htmlText += "	@media only screen and (max-width: 640px) {";
            htmlText += "	    h1, h2, h3, h4 {";
            htmlText += "	        font-weight: 600 !important;";
            htmlText += "	        margin: 20px 0 5px !important;";
            htmlText += "	    }";
            htmlText += "	    h1 {";
            htmlText += "	        font-size: 22px !important;";
            htmlText += "	    }";
            htmlText += "	    h2 {";
            htmlText += "	        font-size: 18px !important;";
            htmlText += "	    }";
            htmlText += "	    h3 {";
            htmlText += "	        font-size: 16px !important;";
            htmlText += "	    }";
            htmlText += "	    .container {";
            htmlText += "	        width: 100% !important;";
            htmlText += "	    }";
            htmlText += "	    .content, .content-wrap {";
            htmlText += "	        padding: 10px !important;";
            htmlText += "	    }";
            htmlText += "	    .invoice {";
            htmlText += "	        width: 100% !important;";
            htmlText += "	    }";
            htmlText += "	}";
            htmlText += "	</style>";

            htmlText += "    <title>자동이체 닷컴 데일리 리포트</title>";

            htmlText += "   </head>";
            htmlText += "   <body>";
            htmlText += "       <div class='tableBox mB10'>";
            htmlText += "				<table class='listTb'>";
            htmlText += "					<colgroup>";
            htmlText += "						<col width='%'>";
            htmlText += "						<col width='12%'>";
            htmlText += "						<col width='12%'>";
            htmlText += "						<col width='12%'>";
            htmlText += "					</colgroup>";
            htmlText += "					<thead>";
            htmlText += "					<tr>";
            htmlText += "						<th>"+ monthDay +" ~ "+ today +" (최근 한달)</th>";
            htmlText += "						<th>접수</th>";
            htmlText += "						<th>처리중</th>";
            htmlText += "						<th>처리완료</th>";
            htmlText += "					</tr>";
            htmlText += "					</thead>";
            htmlText += "					<tbody id='tbody'>";

            htmlText += "						<tr>";
            htmlText += "							<td>자동이체 신청</td>";
            htmlText += "							<td class='tdbold'>" + requestCount1 + "</td>";
            htmlText += "							<td class='tdbold'>" + requestCount5 + "</td>";
            htmlText += "							<td class='tdbold'>" + requestCount3 + "</td>";
            htmlText += "						</tr>";
            htmlText += "						<tr>";
            htmlText += "							<td>자동이체 해지</td>";
            htmlText += "							<td class='tdbold'>" + requestCount8 + "</td>";
            htmlText += "							<td class='tdbold'>" + requestCount5 + "</td>";
            htmlText += "							<td class='tdbold'>" + requestCount9 + "</td>";
            htmlText += "						</tr>";
            htmlText += "						<tr>";
            htmlText += "							<td>청구서 신청</td>";
            htmlText += "							<td class='tdbold'>" + billCount1 + "</td>";
            htmlText += "							<td class='tdbold'>" + billCount5 + "</td>";
            htmlText += "							<td class='tdbold'>" + billCount3 + "</td>";
            htmlText += "						</tr>";
            htmlText += "						<tr>";
            htmlText += "							<td>청구서 해지</td>";
            htmlText += "							<td class='tdbold'>" + billCount8 + "</td>";
            htmlText += "							<td class='tdbold'>" + billCount5 + "</td>";
            htmlText += "							<td class='tdbold'>" + billCount9 + "</td>";
            htmlText += "						</tr>";
            htmlText += "						<tr>";
            htmlText += "							<td>결제수단 변경</td>";
            htmlText += "							<td class='tdbold'>0</td>";
            htmlText += "							<td class='tdbold'>0</td>";
            htmlText += "							<td class='tdbold'>" + changeHistoryPayMethdCnt + "</td>";
            htmlText += "						</tr>";
            htmlText += "						<tr>";
            htmlText += "							<td>주문상품 정보 변경</td>";
            htmlText += "							<td class='tdbold'>0</td>";
            htmlText += "							<td class='tdbold'>0</td>";
            htmlText += "							<td class='tdbold'>" + changeHistoryReqGoodsCnt + "</td>";
            htmlText += "						</tr>";
            htmlText += "						<tr>";
            htmlText += "					        <td>배송지 주소 변경</td>";
            htmlText += "							<td class='tdbold'>0</td>";
            htmlText += "							<td class='tdbold'>0</td>";
            htmlText += "							<td class='tdbold'>" + changeHistorDrivAddrCnt + "</td>";
            htmlText += "						</tr>";
            htmlText += "						<tr>";
            htmlText += "							<td>회원 정보 변경(전화번호)</td>";
            htmlText += "							<td class='tdbold'>0</td>";
            htmlText += "							<td class='tdbold'>0</td>";
            htmlText += "							<td>" + changeHistoryMemInfoTelCnt + "</td>";
            htmlText += "						</tr>";

            htmlText += "					</tbody>";
            htmlText += "				</table>";
            htmlText += "		</div>";
            htmlText += "   </body>";
            htmlText += "</html>";

            MailUtil.sendMail("자동이체 닷컴 데일리 리포트", "jadong2che@gmail.com",sendMailInfo.getEmail(),htmlText );

        }
    }
}
