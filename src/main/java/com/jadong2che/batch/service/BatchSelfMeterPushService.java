package com.jadong2che.batch.service;

import com.jadong2che.batch.mapper.BatchSelfMeterPushMapper;
import com.jadong2che.client.domain.SelfMeterPush;
import com.jadong2che.common.service.CommonService;
import com.jadong2che.util.PushUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Service
public class BatchSelfMeterPushService {

    @Autowired
    private BatchSelfMeterPushMapper batchSelfMeterPushMapper;

    @Autowired
	private CommonService commonService;

    public  void sendPush() throws Exception{

        List<SelfMeterPush> pushInfoList = batchSelfMeterPushMapper.getSelfMeterPushInfoList();

        if(pushInfoList != null && pushInfoList.size() >0){

            List<SelfMeterPush> pushList = new ArrayList<SelfMeterPush>();

            for(int i=0; i<pushInfoList.size(); i++){

                SelfMeterPush push = pushInfoList.get(i);

                if(push.getUserIndex() > 0 && push.getAppToken() != null){
                    HashMap<String, Object> param = new HashMap<String, Object>();

                    String appToken = push.getAppToken();
                    String sendTitle = push.getSendTitle();
                    String sendContents = push.getSendContents();
                    String messageCode = push.getPushType();
                    Integer selfMeterPushIndex = push.getSelfmeterPushIndex();
                    Integer userIndex = push.getUserIndex();
                    String startDatetime = push.getSelfmeterStartDatetime();
                    String endDatetime = push.getSelfmeterEndDatetime();
                    String userName = push.getUserName();

                    String url = "reading/info?selfmeterDetailIndex=" + push.getSelfmeterDetailIndex();

                    sendContents = sendContents.replaceAll("고객명", userName);
                    sendContents = sendContents.replaceAll("00일~00일", startDatetime +"~" + endDatetime + " ");

                    System.out.println("============================================================================================");
                    System.out.println(sendContents);
                    System.out.println("============================================================================================");

                    String osType = "A";
                    HashMap osMap = commonService.getUserOsType(userIndex);

                    if(osMap != null && osMap.get("osType") != null && !osMap.get("osType").equals("")){
                        osType = osMap.get("osType").toString();
                    }

                    HashMap<String, Object> result = PushUtil.pushFCMNotification(appToken, sendTitle, sendContents, messageCode, selfMeterPushIndex.toString(), userIndex.toString(),url,osType, "자가검침");
//                    HashMap<String, Object> result = PushUtil.pushFCMNotification(appToken, sendTitle, sendContents, messageCode, selfMeterPushIndex.toString(), userIndex.toString(),url,osType);

                    String successYn = "";

                    if (Integer.parseInt(result.get("success").toString()) > 0) {
                        successYn = "Y";
                    } else {
                        successYn = "N";
                    }

                    param.put("sendTitle",sendTitle);
                    param.put("sendContents",sendContents);
                    param.put("selfMeterPushIndex", selfMeterPushIndex);
                    param.put("contentIndex", push.getSelfmeterDetailIndex());

                    param.put("userIndex", userIndex);
                    param.put("successYn", successYn);

                    param.put("messageCode", messageCode);

                    batchSelfMeterPushMapper.modifySelfMeterPush(param);
                    batchSelfMeterPushMapper.registSelfMeterPushHistory(param);

//                    param.put("url", "jadong2che://push/web?url=https://jadong2che-fd5a7.firebaseapp.com/" + url);
//                    commonService.registMessage(param);
                }
            }
        }
    }
}
