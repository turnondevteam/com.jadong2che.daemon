package com.jadong2che.batch;

import com.jadong2che.batch.job.batchMailSend;
import com.jadong2che.batch.job.batchPushSend;
import com.jadong2che.batch.job.batchSelfMeterPushSend;
import com.jadong2che.batch.service.BatchMailService;
import com.jadong2che.batch.service.BatchPushService;
import com.jadong2che.batch.service.BatchSelfMeterPushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 배치는 동시에 10개이상 돌지 않는다.
 * @author Hongsuk-Choi
 *
 */
@Component
public class BatchScheduler {

	@Autowired
	BatchPushService batchPushService;

	@Autowired
	BatchSelfMeterPushService batchSelfMeterPushService;

	@Autowired
	BatchMailService batchMailService;

	/**
	 초 분 시 일 월 년
	* : 모든 값
	? : 특정 값 없음
	- : 범위 지정에 사용
	, : 여러 값 지정 구분에 사용
	/ : 초기값과 증가치 설정에 사용
	L : 지정할 수 있는 범위의 마지막 값
	W : 월~금요일 또는 가장 가까운 월/금요일
	# : 몇 번째 무슨 요일 2#1 => 첫 번째 월요일
	H
	 */

	/**
	* 매일 5분 간격으로
 	*/
	@Scheduled(cron = "0 0/5 * * * *")
	public void pushSend() {

		System.out.println("=========================================================================================================================");

		Processor processor = new batchPushSend(batchPushService);
//		processor.execute("푸시 조회");
		System.out.println("=========================================================================================================================");
	}

	/**
	 * 매일 20분 간격으로
	 */
//	@Scheduled(cron = "0 0/20 * * * *")
	public void selfMeterpushSend() {

		System.out.println("=========================================================================================================================");

		Processor processor = new batchSelfMeterPushSend(batchSelfMeterPushService);
//		processor.execute("자가검침 푸시 조회");
		System.out.println("=========================================================================================================================");
	}

	/**
	 * 매일 8시
	 */
	@Scheduled(cron="0 0 08 * * ?")
//	@Scheduled(cron = "0 0/1 * * * *")
	public void mailSend() {

		System.out.println("EMAIL=========================================================================================================================");

		Processor processor = new batchMailSend(batchMailService);
//		processor.execute("데일리 리포트 발송");
		System.out.println("=========================================================================================================================");
	}
}
