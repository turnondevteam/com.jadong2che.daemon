package com.jadong2che.batch.job;


import com.jadong2che.batch.Processor;
import com.jadong2che.batch.service.BatchPushService;

public class batchPushSend extends Processor {

	private BatchPushService service;

	public batchPushSend(BatchPushService batchPushService){
		service = batchPushService;
	}

	@Override
	protected void execute() throws Exception{
		service.sendPush();
	}

}
