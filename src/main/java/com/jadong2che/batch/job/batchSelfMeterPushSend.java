package com.jadong2che.batch.job;


import com.jadong2che.batch.Processor;
import com.jadong2che.batch.service.BatchPushService;
import com.jadong2che.batch.service.BatchSelfMeterPushService;

public class batchSelfMeterPushSend extends Processor {

	private BatchSelfMeterPushService service;

	public batchSelfMeterPushSend(BatchSelfMeterPushService batchPushService){

		service = batchPushService;
	}

	@Override
	protected void execute() throws Exception{
		service.sendPush();
	}

}
