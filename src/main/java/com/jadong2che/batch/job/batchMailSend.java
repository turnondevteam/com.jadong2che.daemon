package com.jadong2che.batch.job;


import com.jadong2che.batch.Processor;
import com.jadong2che.batch.service.BatchMailService;
import com.jadong2che.batch.service.BatchPushService;

public class batchMailSend extends Processor {

	private BatchMailService service;

	public batchMailSend(BatchMailService batchMailService){
		service = batchMailService;
	}

	@Override
	protected void execute() throws Exception{
		service.sendMail();
	}

}
