package com.jadong2che.batch;


import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;

/**
 * 
 * @author Hongsuk-Choi
 *
 */
public abstract class Processor {
    
    Log log = LogFactory.getLog(this.getClass());
    
    protected abstract void execute() throws Exception;
    
    private void executeProcess(String batchId) {
        
        try {
        	log.debug("[" + batchId + "] batch start!" +  new Date());
            execute();
        } catch (Throwable e) {
            if (log.isErrorEnabled()) {
                log.error(e.getMessage(), e);
            }
            
        } finally {
        	log.debug("[" + batchId + "] batch end!" +  new Date());
        }
        
    }

    public void execute(String batchId) {

        executeProcess(StringUtils.substring(batchId, batchId.lastIndexOf('.') + 1));

    }

}
