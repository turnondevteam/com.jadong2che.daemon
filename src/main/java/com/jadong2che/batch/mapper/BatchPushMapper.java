package com.jadong2che.batch.mapper;

import com.jadong2che.client.domain.Push;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface BatchPushMapper {

	List<Push> getPushInfoList() throws Exception;

	List<Push> getPushList() throws Exception;

	List<Push> getPushAllUserList() throws Exception;

	void modifyPush(HashMap<String,Object> param) throws Exception;

	void registPushHistory(HashMap<String,Object> param) throws Exception;

	void modifyPushBill(HashMap<String,Object> param) throws Exception;

	void modifyPushBillMonthly(HashMap<String,Object> param) throws Exception;

	void modifyPaymentDetail(HashMap<String,Object> param) throws Exception;



}
