package com.jadong2che.batch.mapper;

import com.jadong2che.client.domain.Push;
import com.jadong2che.client.domain.SelfMeter;
import com.jadong2che.client.domain.SelfMeterPush;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public  interface BatchSelfMeterPushMapper {

	List<SelfMeterPush> getSelfMeterPushInfoList() throws Exception;

	List<SelfMeterPush> getSelfMeterPushList() throws Exception;

	void modifySelfMeterPush(HashMap<String, Object> param) throws Exception;

	void registSelfMeterPushHistory(HashMap<String, Object> param) throws Exception;


}
