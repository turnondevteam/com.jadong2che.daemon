package com.jadong2che.batch.mapper;

import com.jadong2che.client.domain.Mail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public  interface BatchMailMapper {

	List<Mail> getMailUserList() throws Exception;

}
