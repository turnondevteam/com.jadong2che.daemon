package com.jadong2che.util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;


public class PageCriteria implements Serializable {
	private static final int DEFAULT_PAGE_NUM = 1;

	public static final Charset UTF8 = Charset.forName("UTF-8");

    private static final long serialVersionUID = -8930895458937778804L;

    /** 페이지 */
    private int page = 1;
    /** 한 페이지당 아이템 수 */
    private int limit = 10;
    /** 전체페이지 */
    private int totalPage;
    /** 전체 아이템수 */
    private int totalNum;
    /** 검색조건 원본 : serialized되어 넘어온 모든 파라미터 원본임.*/
    private Map<String, Object> params = new HashMap<String, Object>();
    // 페이징 네비게이션 바에서 보여질 페이지 갯수
    private int navigateLimit = 10;
    

    /** offset */
    private int offset = 0;
    /** offset활성화 */
    private boolean activeOffset = false;

    /**
     * 기본 생성자.
     */
    public PageCriteria() {
    }

    protected void deserialize() {
    	try {
    		ConvertUtilsBean convertUtilsBean = new ConvertUtilsBean();
            convertUtilsBean.deregister(Date.class);
            convertUtilsBean.register(new BeanUtilsDateConvertor(), Date.class);
            convertUtilsBean.register(new BeanUtilsListConvertor(), List.class);

            BeanUtilsBean beanUtils = new BeanUtilsBean(convertUtilsBean);

            beanUtils.populate(this, getParams());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
    }

    /**
     * 페이지 번호와 페이지당 표시할 아이템수를 초기화하는 생성자.
     *
     * @param page
     *            페이지 번호
     * @param limit
     *            페이지당 표시할 아이템 갯수
     */
    public PageCriteria(String page, int limit) {
        if (page != null && page.length() > 0) {
            this.page = Integer.parseInt(page);
        } else {
            this.page = DEFAULT_PAGE_NUM;
        }
        this.limit = limit;
    }

    /**
     * offset 번호 생성자.
     *
     * @param offset
     *              offset 값.
     * @param limit
     *              offset 기준 표시할 아이템 갯수
     */
    public PageCriteria(String offset, int limit, boolean offsetType) {

    	this.page = DEFAULT_PAGE_NUM;

    	setActiveOffset(offsetType);
    	setOffset(Integer.parseInt(offset));
        setLimit(limit);
    }

	protected void init(int defaultLimit, String serializedCriteria) {
		parseSerializedCriteria(defaultLimit, serializedCriteria);

    	try {
			deserialize();
		} catch (Exception e) {
			// serialize 에러 ...
			// Creteria 클래스 정보 & serializedCriteria를 예외에 정리해서 알려줘야 함.
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	protected void init(String serializedCriteria) {
		parseSerializedCriteria(serializedCriteria);

    	try {
			deserialize();
		} catch (Exception e) {
			// serialize 에러 ...
			// Creteria 클래스 정보 & serializedCriteria를 예외에 정리해서 알려줘야 함.
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
    /**
     * @param page
     *            설정할 페이지
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * @param limit
     *            설정할 한 페이지당 아이템 수
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * 전체 아이템수 설정
     *
     * @param totalNum
     *            전체 아이템수
     */
    public void setTotalNum(int totalNum) {
        if (totalNum == 0) {
            return;
        }
        this.totalNum = totalNum;
        this.totalPage = (totalNum / this.limit) + (totalNum % this.limit > 0 ? 1 : 0);
    }

    /**
     * @param conditions
     *            설정할 조건들
     */
    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    /**
     * 조건 추가
     *
     * @param key
     *            조건 키
     * @param value
     *            조건 값
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void addCondition(String key, String value) {
   		if(this.params.containsKey(key)){
   			Object targetValue = this.params.get(key);

   			List<Object> array = null;
   			if(targetValue instanceof List){
   				array = ((List)targetValue);
   			}else{
   				array =new ArrayList<Object>();
   				array.add(targetValue);
   			}
   			array.add(value);

   			this.params.put(key, array);
   		}else{
   			this.params.put(key, value);
   		}
    }

    public void parseSerializedCriteria(int defaultLimit, String serializedCriteria){
    	if(serializedCriteria!=null && serializedCriteria.length()>0){
    		String[] splitedCondition = serializedCriteria.split("&");
        	for (String keyValue : splitedCondition) {
        		String[] param = keyValue.split("=");
        		// 빈 값의 조건은 무시
        		if(param.length <= 1)
        			continue;
    			try {
    				addCondition(param[0], URLDecoder.decode(param[1], UTF8.name()));
    			} catch (UnsupportedEncodingException e) {
    				//무시
    			}
    		}
    	}

    	setPageAndLimit(defaultLimit);
    }
    
	public void parseSerializedCriteria(String serializedCriteria){
    	if(serializedCriteria!=null && serializedCriteria.length()>0){
    		String[] splitedCondition = serializedCriteria.split("&");
        	for (String keyValue : splitedCondition) {
        		String[] param = keyValue.split("=");
        		// 빈 값의 조건은 무시
        		if(param.length <= 1)
        			continue;
    			try {
    				addCondition(param[0], URLDecoder.decode(param[1], UTF8.name()));
    			} catch (UnsupportedEncodingException e) {
    				//무시
    			}
    		}
    	}
    }

    private void setPageAndLimit(int defaultLimit){
    	String page = (String)getParamValue("page");
    	if(page!=null)
    		setPage(Integer.parseInt(page));
    	else{
    		setPage(DEFAULT_PAGE_NUM);
    	}
    	String pageLimit = (String)getParamValue("limit");
    	if(pageLimit!=null)
    		setLimit(Integer.parseInt(pageLimit));
    	else
    		setLimit(defaultLimit);
    }

    public Object getParamValue(String key){
    	return this.params.get(key);
    }
    public String getStringParamValue(String key){
    	if(this.params.get(key) == null ) return null;
    	return this.params.get(key).toString();
    }

    /**
     * @return 시작 인덱스
     */
    public int getStart() {
        return ((this.page - 1) * this.limit) + 1;
    }

    /**
     * @return mysql에서의 시작 인덱스
     */
    public int getStartIndex() {

    	// offset 추가 변경.
    	int startIndex = 0;
    	if(isActiveOffset()) {
    		startIndex = getOffset();
    	}else{
    		startIndex = ((this.page - 1) * this.limit);
    	}
    	return startIndex;
        //return ((this.page - 1) * this.limit);
    }

    /**
     * @return 끝 인덱스
     */
    public int getEnd() {
        return this.page * this.limit;
    }

    /**
     * @return page 페이지
     */
    public int getPage() {
        return page;
    }

    /**
     * @return limit 한 페이지당 아이템 수
     */
    public int getLimit() {
        return limit;
    }

    /**
     * @return totalPage 전체 페이지 수
     */
    public int getTotalPage() {
        return totalPage;
    }

    /**
     * @return totalNum 전체 아이템 수
     */
    public int getTotalNum() {
        return totalNum;
    }

    /**
     * @return conditions 검색조건
     */
    public Map<String, Object> getParams() {
        return params;
    }

    public int getNavigateLimit() {
        return navigateLimit;
    }

    public void setNavigateLimit(int navigateLimit) {
        this.navigateLimit = navigateLimit;
    }

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public boolean isActiveOffset() {
		return activeOffset;
	}

	public void setActiveOffset(boolean activeOffset) {
		this.activeOffset = activeOffset;
	}
	
}
