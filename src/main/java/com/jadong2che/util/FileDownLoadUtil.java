package com.jadong2che.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
/**
 * <pre>
 * FileDownLoadUtil.java
 * </pre>
 *  
 */
public class FileDownLoadUtil extends AbstractView {

	public void Download(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception{
        setContentType("application/download; utf-8");
        renderMergedOutputModel(model,request,response);
	}

	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		File file = (File)model.get("downloadFile");
     
        response.setContentType(getContentType());
        response.setContentLength((int)file.length());
         
        String userAgent = request.getHeader("User-Agent");
        boolean ie = userAgent.indexOf("MSIE") > -1;
        String fileName = null;

        if(ie){
            fileName = URLEncoder.encode(file.getName(), "utf-8");
        } else {
            fileName = new String(file.getName().getBytes("utf-8"));
        }
 
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
        response.setHeader("Content-Transfer-Encoding", "binary");
        OutputStream out = response.getOutputStream();
        FileInputStream fis = null;
         
        try {
            fis = new FileInputStream(file);
            FileCopyUtils.copy(fis, out);
        } catch(Exception e){
            e.printStackTrace();
        }finally{
            if(fis != null){
                try{
                    fis.close();
                }catch(Exception e){}
            }
        }
        out.flush();
	}
	
	
	
	public static void makeZip(HttpServletRequest request, String zipFileName, ArrayList<String> files) throws Exception {

		byte[] buf = new byte[4096];
		 
		try {

		    ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
		 
		    for (int i=0; i<files.size(); i++) {
//		        FileInputStream in = new FileInputStream(request.getSession().getServletContext().getRealPath("/" + files.get(i)) );
		        FileInputStream in = new FileInputStream(files.get(i));
		        Path p = Paths.get(files.get(i));
		        String fileName = p.getFileName().toString();
		                
		        ZipEntry ze = new ZipEntry(fileName);
		        out.putNextEntry(ze);
		          
		        int len;
		        while ((len = in.read(buf)) > 0) {
		            out.write(buf, 0, len);
		        }
		          
		        out.closeEntry();
		        in.close();
		 
		    }
		          
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
	 
	public static void downloadZip(HttpServletRequest request, HttpServletResponse response, String zipFileName, String path, ArrayList<String> files) throws Exception{

		makeDir(path);

		makeZip(request, zipFileName, files);
		
		File zipFile = new File(zipFileName);

		String header = CommonUtil.getBrowser(request);

		response.setContentType("application/x-zip-compressed;");

		if (header.contains("MSIE")) {
			String docName = URLEncoder.encode(zipFile.getName(),"UTF-8").replaceAll("\\+", "%20");
			response.setHeader("Content-Disposition", "attachment;filename=" + docName + ";");
		} else if (header.contains("Firefox")) {
			String docName = new String(zipFile.getName().getBytes("UTF-8"), "ISO-8859-1");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
		} else if (header.contains("Opera")) {
			String docName = new String(zipFile.getName().getBytes("UTF-8"), "ISO-8859-1");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
		} else if (header.contains("Chrome")) {
			String docName = new String(zipFile.getName().getBytes("UTF-8"), "ISO-8859-1");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
		}
/*
		response.setContentType("application/x-zip-compressed;");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + zipFile.getName() + "\";");
*/

		downloadFile(zipFile,response);

	}

	public static void downloadFile(HttpServletRequest request, HttpServletResponse response, String fileName) throws Exception{

//		fileName = request.getSession().getServletContext().getRealPath("/" + fileName);

		File file = new File(fileName);
		/*
		response.setContentType("application/x-zip-compressed;");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\";");
		*/

		String header = CommonUtil.getBrowser(request);

		response.setContentType("application/x-zip-compressed;");

		if (header.contains("MSIE")) {
			String docName = URLEncoder.encode(file.getName(),"UTF-8").replaceAll("\\+", "%20");
			response.setHeader("Content-Disposition", "attachment;filename=" + docName + ";");
		} else if (header.contains("Firefox")) {
			String docName = new String(file.getName().getBytes("UTF-8"), "ISO-8859-1");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
		} else if (header.contains("Opera")) {
			String docName = new String(file.getName().getBytes("UTF-8"), "ISO-8859-1");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
		} else if (header.contains("Chrome")) {
			String docName = new String(file.getName().getBytes("UTF-8"), "ISO-8859-1");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + docName + "\"");
		}

		downloadFile(file,response);
	}

	public static void downloadFile(File file, HttpServletResponse response){

		OutputStream out = null;
		InputStream in = null;

		try{

			out = response.getOutputStream();
			in = new FileInputStream(file);
			byte[] buf = new byte[1024];

			int len;
			while( (len = in.read(buf)) > 0 ){
				out.write(buf, 0, len);
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (out != null) {
				try { out.close(); } catch (Exception e) {} out = null;
			}
			if (in != null) {
				try { in.close(); } catch (Exception e) {} in = null;
			}
		}
	}

	public static void makeDir(String path){

		File saveDir = new File(path);

		if(!saveDir.exists()){
			saveDir.mkdir();
		}

	}

	
}
