package com.jadong2che.util.ksnet;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;


/*
	Class Name : KSPayEncSocketBean

	최초 작성일자 : 2003/11/24
	최초 작성자   : 이훈구
	최종 수정일자 :
	최종 수정자   :
*/

public class KSPayEncSocketBean {

	private	Socket				socket;				//IPG_Server(C-Daemon)과 연결 소켓
	private DataInputStream		in;
	private DataOutputStream	out;
	public  String				IPAddr;
	public  int					Port;

	// JDK 1.4 이하에서는 BouncyCastleProvider를 사용(아래주석)

	private static final String CIPHER_PROVIDER = "BC";

	static
	{
		if (Security.getProvider(CIPHER_PROVIDER) == null) {
			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		}
	}

	/*
	 **	통신 전담 Private 메쏘드들
	 */

	public KSPayEncSocketBean(String IPAddr, int Port)
	{
		this.IPAddr = IPAddr;
		this.Port   = Port;
	}

	public KSPayEncSocketBean()
	{
		this.IPAddr = null;
		this.Port   = 0;
	}

	// IPG_Server와 연결을 맺는다.
	public void ConnectSocket() throws IOException
	{
		try
		{
			socket = new Socket(this.IPAddr, this.Port);
			in     = new DataInputStream(socket.getInputStream());
			out    = new DataOutputStream(socket.getOutputStream());
			System.out.println("KSPayEncSocketBean.ConnectSocket addr = [" + this.IPAddr + "] port = [" + this.Port + "]....");
		}
		catch( IOException e )
		{
			throw new IOException("[KSPayEncSocketBean] cannot connect server : (" + this.IPAddr + " , " + this.Port + ")");
		}
	}

	// IPG_Server와 연결을 맺는다.
	public void ConnectSocket(int c_msec, int r_msec) throws IOException
	{
		try
		{
			socket = new Socket();
			socket.setSoTimeout(r_msec);//read timeout
			socket.connect(new InetSocketAddress(this.IPAddr, this.Port),c_msec);//connect timeout

			in     = new DataInputStream(socket.getInputStream());
			out    = new DataOutputStream(socket.getOutputStream());
			System.out.println("KSPayEncSocketBean.ConnectSocket("+c_msec+","+r_msec+") addr = [" + this.IPAddr + "] port = [" + this.Port + "]....");
		}
		catch( IOException e )
		{
			throw new IOException("[KSPayEncSocketBean] cannot connect server : (" + this.IPAddr + " , " + this.Port + ")");
		}
	}

	private static byte[] ks_rsa_encrypt(byte[] sbuf) throws NoSuchProviderException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, IllegalBlockSizeException
	{
		BigInteger modulus          = new BigInteger("d4846c2b8228dddfab9e614da2a324c1cc7b29d848cc005624d3a09667a2aab9073290bace6aa536ddceb3c47ddda78d9954da06c83aa65b939c5ec773a3787e71bec5a1c077bb446c06b393d2537967645d386b4b0b4ec21372fdc728c56693028c1c3915c1c4279793eb3dccefd6bf49b86cc7d88a47b0d44aba9e73750fcd",16);
		BigInteger publicExponent   = new BigInteger("0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010001",16);

		RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(modulus, publicExponent);

		// JDK 1.4 이하에서는 BouncyCastleProvider를 사용(아래주석)
		//KeyFactory keyfactory = KeyFactory.getInstance("RSA", CIPHER_PROVIDER);
		KeyFactory keyfactory = KeyFactory.getInstance("RSA");
		PublicKey publickey = keyfactory.generatePublic(pubKeySpec);

		// JDK 1.4 이하에서는 BouncyCastleProvider를 사용(아래주석)
		//Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding",CIPHER_PROVIDER);
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

		cipher.init(Cipher.ENCRYPT_MODE, publickey);//, fixedsecurerandom);

		byte[] rbuf = cipher.doFinal(sbuf);

		return rbuf;
	}

	private static byte[] ks_seed_encrypt(byte[] kbuf, byte[] mbuf) throws NoSuchProviderException, BadPaddingException, NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException
	{
		byte[] iv = new byte[16];

		byte raw[]      = kbuf;

		SecretKeySpec skeySpec = new SecretKeySpec(raw, "SEED");
		Cipher cipher = Cipher.getInstance("SEED/CBC/PKCS5Padding", CIPHER_PROVIDER);

		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(iv));

		byte tdata[]    = cipher.doFinal(mbuf);

		return tdata;
	}

	private static byte[] ks_seed_decrypt(byte[] kbuf, byte[] mbuf) throws NoSuchProviderException, NoSuchPaddingException, InvalidAlgorithmParameterException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException
	{
		byte[] iv = new byte[16];

		byte raw[]      = kbuf;

		SecretKeySpec skeySpec = new SecretKeySpec(raw, "SEED");
		Cipher cipher = Cipher.getInstance("SEED/CBC/PKCS5Padding", CIPHER_PROVIDER);

		cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(iv));

		byte tdata[]    = cipher.doFinal(mbuf);

		return tdata;
	}

	public byte[] SendNRecv(byte[] msg) throws IOException
	{
		try
		{
			if ((byte)'2' != msg[4] && (byte)'0' != msg[4]) throw new IllegalArgumentException("[KSPayEncSocketBean] 암호화구분오류("+(char)msg[4]+")!!");

			int midx = 0;
			byte[] p_kbuf   = new byte[0];
			byte[] e_sbuf   = new byte[0];
			if ((byte)'2' == msg[4])
			{
				p_kbuf   		= new StringBuffer().append(System.currentTimeMillis()).append(Long.MAX_VALUE).substring(0,16).getBytes();
				byte[] e_kbuf   = ks_rsa_encrypt(p_kbuf);

				byte[] p_dbuf   = new byte[msg.length-4];
				System.arraycopy(msg ,4 ,p_dbuf ,0 ,p_dbuf.length);

				byte[] e_dbuf   = ks_seed_encrypt(p_kbuf, p_dbuf);
				e_sbuf          = new byte[4+1+e_kbuf.length+4+e_dbuf.length];

				System.arraycopy("01292".getBytes()                                                                         ,0  ,e_sbuf ,midx,  5           ); midx+= 5             ;
				System.arraycopy(e_kbuf                                                                                     ,0  ,e_sbuf ,midx, e_kbuf.length); midx+=e_kbuf.length  ;

				System.arraycopy(("0000".substring(String.valueOf(e_dbuf.length).length(),4) + e_dbuf.length).getBytes()    ,0  ,e_sbuf ,midx,  4           ); midx+= 4             ;
				System.arraycopy(e_dbuf                                                                                     ,0  ,e_sbuf ,midx, e_dbuf.length); midx+=e_dbuf.length  ;
			}else
			{
				e_sbuf = (byte[])msg.clone();
			}

			write(e_sbuf);

			byte[] len_rbuf = new byte[4];

			read(len_rbuf,0,4);
			int len = Integer.parseInt(new String(len_rbuf,0,4));

			byte[] e_rbuf = new byte[len];
			read(e_rbuf,0,len);

			byte[] rmsg   = new byte[0];
			if ((byte)'2' == msg[4])
			{
				byte[] p_rbuf   = ks_seed_decrypt(p_kbuf,e_rbuf);
				rmsg			= new byte[p_rbuf.length+4];

				midx = 0;
				System.arraycopy(("0000".substring(String.valueOf(p_rbuf.length).length(),4) + p_rbuf.length).getBytes()    ,0  ,rmsg   ,midx,  4           ); midx+= 4             ;
				System.arraycopy(p_rbuf                                                                                     ,0  ,rmsg   ,midx, p_rbuf.length); midx+=p_rbuf.length  ;
			}else
			{
				rmsg			= new byte[e_rbuf.length+4];

				midx = 0;
				System.arraycopy(("0000".substring(String.valueOf(e_rbuf.length).length(),4) + e_rbuf.length).getBytes()    ,0  ,rmsg   ,midx,  4           ); midx+= 4             ;
				System.arraycopy(e_rbuf                                                                                     ,0  ,rmsg   ,midx, e_rbuf.length); midx+=e_rbuf.length  ;
			}

			return rmsg;
		}catch(IOException ie)
		{
			ie.printStackTrace();
			throw new IOException("[KSPayEncSocketBean] SendNRecv 통신오류발생!!");
		}catch(Exception e)
		{
			e.printStackTrace();
			throw new IOException("[KSPayEncSocketBean] SendNRecv 기타오류발생!!");
		}
	}

	// IPG_Server에 데이타를 전달한다..
	private void write(byte[] msg) throws IOException
	{
		try
		{
			out.write(msg);
			out.flush();
		}
		catch( IOException e )
		{
			throw new IOException("[KSPayEncSocketBean] cannot write to socket");
		}
	}

	// IPG_Server로 부터 데이타를 얻는다.
	private void read(byte[] msg, int idx, int size) throws IOException
	{
		try
		{
			in.read(msg,idx,size);
		}
		catch( IOException e )
		{
			throw new IOException("[KSPayEncSocketBean] cannot read from socket");
		}
	}

	// 연결 소켓을 닫는다.
	public void CloseSocket() throws IOException
	{
		try
		{
			if (socket != null) socket.close();
			socket = null;
		}
		catch( IOException e )
		{
			throw new IOException("[KSPayEncSocketBean] cannot close socket");
		}
	}
}
