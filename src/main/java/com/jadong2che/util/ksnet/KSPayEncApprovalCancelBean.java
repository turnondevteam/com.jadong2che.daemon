package com.jadong2che.util.ksnet;

import java.io.IOException;

/*
	Class Name : KSPayEncApprovalCancelBean
				 승인/취소 요청 처리

	최초 작성일자 : 2003/11/22
	최초 작성자   : 이훈구
	최종 수정일자 : 2006/08/01
	최종 수정자   : 문선영
	Version : V1.1
*/

public class KSPayEncApprovalCancelBean{
	private String              IPAddr;
	private int                 Port;

	private KSPayEncSocketBean     KSPaySocket;

	public String               HeadMsg;            //Head Message
	public String               EchoMsg;            //Echo Message
	public String               DataMsg;
	public String               SendMsg;
	public String               RcvEchoMsg;
	public String               ReceiveMsg;

	public int                  SendCount = 0, ReceiveCount = 0;

	private int MAXSIZE = 3;

	/* Header */
	public  String  EncType         ,      // 0: 암화안함, 1:openssl, 2: seed
			Version         ,      // 전문버전
			Type            ,      // 구분
			Resend          ,      // 전송구분 : 0 : 처음,  2: 재전송
			RequestDate     ,      // 요청일자 : yyyymmddhhmmss
			StoreId         ,      // 상점아이디
			OrderNumber     ,      // 주문번호
			UserName        ,      // 주문자명
			IdNum           ,      // 주민번호 or 사업자번호
			Email           ,      // email
			GoodType        ,      // 제품구분 1 : 실물, 2 : 디지털
			GoodName        ,      // 제품명
			KeyInType       ,      // KeyInType 여부 : S : Swap, K: KeyInType
			LineType        ,      // lineType 0 : offline, 1:internet, 2:Mobile
			PhoneNo         ,      // 휴대폰번호
			ApprovalCount   ,      // 복합승인갯수
			HeadFiller      ;      // 예비

	/* 신용카드승인결과 */
	public String[] ApprovalType     = new String[MAXSIZE],     // 승인구분
			TransactionNo    = new String[MAXSIZE],     // 거래번호
			Status           = new String[MAXSIZE],     // 상태 O : 승인 , X : 거절
			TradeDate        = new String[MAXSIZE],     // 거래일자
			TradeTime        = new String[MAXSIZE],     // 거래시간
			IssCode          = new String[MAXSIZE],     // 발급사코드
			AquCode          = new String[MAXSIZE],     // 매입사코드
			AuthNo           = new String[MAXSIZE],     // 승인번호 or 거절시 오류코드
			Message1         = new String[MAXSIZE],     // 메시지1
			Message2         = new String[MAXSIZE],     // 메시지2
			CardNo           = new String[MAXSIZE],     // 카드번호
			ExpDate          = new String[MAXSIZE],     // 유효기간
			Installment      = new String[MAXSIZE],     // 할부
			Amount           = new String[MAXSIZE],     // 금액
			MerchantNo       = new String[MAXSIZE],     // 가맹점번호
			AuthSendType     = new String[MAXSIZE],     // 전송구분= new String[MAXSIZE]
			ApprovalSendType = new String[MAXSIZE],     // 전송구분(0 : 거절, 1 : 승인, 2: 원카드)
			Point1           = new String[MAXSIZE],
			Point2           = new String[MAXSIZE],
			Point3           = new String[MAXSIZE],
			Point4           = new String[MAXSIZE],
			VanTransactionNo = new String[MAXSIZE],     // Van 거래번호
			Filler           = new String[MAXSIZE],     // 예비
			AuthType         = new String[MAXSIZE],     // ISP : ISP거래, MP1, MP2 : MPI거래, SPACE : 일반거래
			MPIPositionType  = new String[MAXSIZE],     // K : KSNET, R : Remote, C : 제3기관, SPACE : 일반거래
			MPIReUseType     = new String[MAXSIZE],     // Y : 재사용, N : 재사용아님
			EncData          = new String[MAXSIZE];     // MPI, ISP 데이터

	/* 계좌이체유형 결과 */
	public String[]
			ACShopName       = new String[MAXSIZE],
			ACCloseDate      = new String[MAXSIZE],
			ACCloseTime      = new String[MAXSIZE],
			ACPareCnt        = new String[MAXSIZE];     //단말정보갯수(페이지갯수가 많아 이름은 바꾸지않음)

	public String[][]
			ACAcctSeleAll   = new String[MAXSIZE][50],  //계좌이체 구분리스트
			ACFeeSeleAll    = new String[MAXSIZE][50],  //선/후불제구분리스트
			ACDptNoAll      = new String[MAXSIZE][50],  //단말정보리스트
			ACPareFillerAll = new String[MAXSIZE][50];  //예비

	/* 계좌이체승인결과 */
	public String[]
			ACTransactionNo      = new String[MAXSIZE],
			ACStatus             = new String[MAXSIZE],
			ACTradeDate          = new String[MAXSIZE],
			ACTradeTime          = new String[MAXSIZE],
			ACAcctSele           = new String[MAXSIZE],
			ACFeeSele            = new String[MAXSIZE],
			ACInjaName           = new String[MAXSIZE],
			ACPareBankCode       = new String[MAXSIZE],
			ACPareAcctNo         = new String[MAXSIZE],
			ACCustBankCode       = new String[MAXSIZE],
			ACCustAcctNo         = new String[MAXSIZE],
			ACAmount             = new String[MAXSIZE],
			ACBankTransactionNo  = new String[MAXSIZE],
			ACIpgumNm            = new String[MAXSIZE],
			ACBankFee            = new String[MAXSIZE],
			ACBankAmount         = new String[MAXSIZE],
			ACBankRespCode       = new String[MAXSIZE],
			ACMessage1           = new String[MAXSIZE],
			ACMessage2           = new String[MAXSIZE],
			ACEntrNumb           = new String[MAXSIZE],
			ACShopPhone          = new String[MAXSIZE],
			ACCavvSele           = new String[MAXSIZE],
			ACFiller             = new String[MAXSIZE],
			ACEncData            = new String[MAXSIZE];


	/* 가상계좌승인결과 */
	public String[]
			VATransactionNo  = new String[MAXSIZE],
			VAStatus         = new String[MAXSIZE],
			VATradeDate      = new String[MAXSIZE],
			VATradeTime      = new String[MAXSIZE],
			VABankCode       = new String[MAXSIZE],
			VAVirAcctNo      = new String[MAXSIZE],
			VAName           = new String[MAXSIZE],
			VACloseDate      = new String[MAXSIZE],
			VACloseTime      = new String[MAXSIZE],
			VARespCode       = new String[MAXSIZE],
			VAMessage1       = new String[MAXSIZE],
			VAMessage2       = new String[MAXSIZE],
			VAFiller         = new String[MAXSIZE];

	/* 월드패스승인결과 */
	public String[]
			WPTransactionNo  = new String[MAXSIZE],
			WPStatus         = new String[MAXSIZE],
			WPTradeDate      = new String[MAXSIZE],
			WPTradeTime      = new String[MAXSIZE],
			WPIssCode        = new String[MAXSIZE],         // 발급사코드
			WPAuthNo         = new String[MAXSIZE],         // 승인번호
			WPBalanceAmount  = new String[MAXSIZE],         // 잔액
			WPLimitAmount    = new String[MAXSIZE],         // 한도액
			WPMessage1       = new String[MAXSIZE],         // 메시지1
			WPMessage2       = new String[MAXSIZE],         // 메시지2
			WPCardNo         = new String[MAXSIZE],         // 카드번호
			WPAmount         = new String[MAXSIZE],         // 금액
			WPMerchantNo     = new String[MAXSIZE],         // 가맹점번호
			WPFiller         = new String[MAXSIZE];

	/* 포인트카드승인결과 */
	public String[]
			PTransactionNo    = new String[MAXSIZE],        // 거래번호
			PStatus           = new String[MAXSIZE],        // 상태 O : 승인 , X : 거절
			PTradeDate        = new String[MAXSIZE],        // 거래일자
			PTradeTime        = new String[MAXSIZE],        // 거래시간
			PIssCode          = new String[MAXSIZE],        // 발급사코드
			PAuthNo           = new String[MAXSIZE],        // 승인번호 or 거절시 오류코드
			PMessage1         = new String[MAXSIZE],        // 메시지1
			PMessage2         = new String[MAXSIZE],        // 메시지2
			PPoint1           = new String[MAXSIZE],        // 거래포인트
			PPoint2           = new String[MAXSIZE],        // 가용포인트
			PPoint3           = new String[MAXSIZE],        // 누적포인트
			PPoint4           = new String[MAXSIZE],        // 가맹점포인트
			PMerchantNo       = new String[MAXSIZE],        // 가맹점번호
			PNotice1          = new String[MAXSIZE],        //
			PNotice2          = new String[MAXSIZE],        //
			PNotice3          = new String[MAXSIZE],        //
			PNotice4          = new String[MAXSIZE],        //
			PFiller           = new String[MAXSIZE];        // 예비

	/* 현금영수증승인결과 */
	public String[]
			HTransactionNo      = new String[MAXSIZE],      // 거래번호
			HStatus             = new String[MAXSIZE],      // 오류구분 O:정상 X:거절
			HCashTransactionNo  = new String[MAXSIZE],      // 현금영수증 거래번호
			HIncomeType         = new String[MAXSIZE],      // 0: 소득      1: 비소득
			HTradeDate          = new String[MAXSIZE],      // 거래 개시 일자
			HTradeTime          = new String[MAXSIZE],      // 거래 개시 시간
			HMessage1           = new String[MAXSIZE],      // 응답 message1
			HMessage2           = new String[MAXSIZE],      // 응답 message2
			HCashMessage1       = new String[MAXSIZE],      // 국세청 메시지 1
			HCashMessage2       = new String[MAXSIZE],      // 국세청 메시지 2
			HFiller             = new String[MAXSIZE];      // 예비

	// 휴대폰결제결과
	public String[]
			MTransactionNo    = new String[MAXSIZE],        // 거래번호
			MStatus           = new String[MAXSIZE],        // 오류구분 O:정상 X:거절
			MTradeDate        = new String[MAXSIZE],        // 거래 일자
			MTradeTime        = new String[MAXSIZE],        // 거래 시간
			MBalAmount        = new String[MAXSIZE],        // 잔액
			MRespCode         = new String[MAXSIZE],        // 응답코드
			MRespMsg          = new String[MAXSIZE],        // 거래 개시 시간
			MBypassMsg        = new String[MAXSIZE],        // Echo항목
			MCompCode         = new String[MAXSIZE],        // 업체코드
			MTid              = new String[MAXSIZE],        // 서비스제공업체 승인번호
			MFiller           = new String[MAXSIZE];        // 예비

	/*핸드폰 인증1차 승인결과*/
	public String[]
			MB1TransactionNo  = new String[MAXSIZE],         // 거래번호
			MB1Status         = new String[MAXSIZE],         // 상태 : O, X
			MB1TradeDate      = new String[MAXSIZE],         // 거래일자
			MB1TradeTime      = new String[MAXSIZE],         // 거래시간
			MB1Serverinfo     = new String[MAXSIZE],         // 서버INFO : 업체에서는 보관 필요 없음 예비로
			MB1Smsval         = new String[MAXSIZE],         // 다날의 경우 space
			MB1Stanrespcode   = new String[MAXSIZE],         // 응답코드
			MB1Message        = new String[MAXSIZE],         // 에러메시지
			MB1Filler         = new String[MAXSIZE];         //

	/*핸드폰 인증2차 승인결과*/
	public String[]
			MB2TransactionNo  = new String[MAXSIZE],      // 거래번호
			MB2Status         = new String[MAXSIZE],      // 상태
			MB2TradeDate      = new String[MAXSIZE],      // 거래일자
			MB2TradeTime      = new String[MAXSIZE],      // 거래시간
			MB2Stanrespcode   = new String[MAXSIZE],      // 응답코드
			MB2Message        = new String[MAXSIZE],      // 응답메시지
			MB2Filler         = new String[MAXSIZE];      // 예비

	/* 상점상세정보 조회결과 */
	public String[]
			SITransactionNo    = new String[MAXSIZE],     // 거래번호
			SIStatus           = new String[MAXSIZE],     // 성공:O, 실패: X
			SIRespCode         = new String[MAXSIZE],     // '0000' : 정상처리
			SIAgenMembDealSele = new String[MAXSIZE],     // 자체대행구분
			SIStartSele        = new String[MAXSIZE],     // 개시여부
			SIEntrNumb         = new String[MAXSIZE],     // 사업자번호
			SIShopName         = new String[MAXSIZE],     // 상점명
			SIMembNumbGene     = new String[MAXSIZE],     // 일반 가맹점번호
			SIMembNumbNoin     = new String[MAXSIZE],     // 무이자 가맹점번호
			SIAlloMontType     = new String[MAXSIZE],     // 할부유형
			SIAlloMontInfo     = new String[MAXSIZE],     // 할부정보
			SIPayCnt           = new String[MAXSIZE],     // 결제수단갯수
			SIFiller           = new String[MAXSIZE],     // 예비
			SIEncData          = new String[MAXSIZE];     // 결제수단정보

	// 계좌인증요청(A100)
	public String[]
			NmTransactionNo    = new String[MAXSIZE],
			NmStatus           = new String[MAXSIZE],
			NmTradeDate        = new String[MAXSIZE],
			NmTradeTime        = new String[MAXSIZE],
			NmMessage1         = new String[MAXSIZE],
			NmMessage2         = new String[MAXSIZE],
			NmStanrespcode     = new String[MAXSIZE],
			NmName             = new String[MAXSIZE],
			NmFiller           = new String[MAXSIZE];


	public KSPayEncApprovalCancelBean(String IPAddr, int Port) {
		this.IPAddr = IPAddr;
		this.Port   = Port;

		this.SendCount    = 0;
		this.ReceiveCount = 0;
		this.SendMsg      = "";
	}

	public boolean HeadMessage
			(
					String  EncType         ,// EncType         : 0: 암화안함, 1:openssl, 2: seed
					String  Version         ,// Version         : 전문버전
					String  Type            ,// Type            : 구분
					String  Resend          ,// Resend          : 전송구분 : 0 : 처음,  1: 재전송
					String  RequestDate     ,// RequestDate     : 요청일자 : yyyymmddhhmmss
					String  StoreId         ,// StoreId         : 상점아이디
					String  OrderNumber     ,// OrderNumber     : 주문번호
					String  UserName        ,// UserName        : 주문자명
					String  IdNum           ,// IdNum           : 주민번호 or 사업자번호
					String  Email           ,// Email           : email
					String  GoodType        ,// GoodType        : 제품구분 0 : 실물, 1 : 디지털
					String  GoodName        ,// GoodName        : 제품명
					String  KeyInType       ,// KeyInType       : KeyInType 여부 : S : Swap, K: KeyInType
					String  LineType        ,// LineType        : lineType 0 : offline, 1:internet, 2:Mobile
					String  PhoneNo         ,// PhoneNo         : 모바일번호
					String  ApprovalCount   ,// ApprovalCount   : 복합승인갯수
					String  HeadFiller      )// HeadFiller      : 예비
	{
		StringBuffer TmpHeadMsg = new StringBuffer();

		TmpHeadMsg.append(fmt(EncType       ,    1, 'X'));
		TmpHeadMsg.append(fmt(Version       ,    4, 'X'));
		TmpHeadMsg.append(fmt(Type          ,    2, 'X'));
		TmpHeadMsg.append(fmt(Resend        ,    1, 'X'));
		TmpHeadMsg.append(fmt(RequestDate   ,   14, 'X'));
		TmpHeadMsg.append(fmt(StoreId       ,   10, 'X'));
		TmpHeadMsg.append(fmt(OrderNumber   ,   50, 'X'));
		TmpHeadMsg.append(fmt(UserName      ,   50, 'X'));
		TmpHeadMsg.append(fmt(IdNum         ,   13, 'X'));
		TmpHeadMsg.append(fmt(Email         ,   50, 'X'));
		TmpHeadMsg.append(fmt(GoodType      ,    1, 'X'));
		TmpHeadMsg.append(fmt(GoodName      ,   50, 'X'));
		TmpHeadMsg.append(fmt(KeyInType     ,    1, 'X'));
		TmpHeadMsg.append(fmt(LineType      ,    1, 'X'));
		TmpHeadMsg.append(fmt(PhoneNo       ,   12, 'X'));
		TmpHeadMsg.append(fmt(ApprovalCount ,    1, 'X'));
		TmpHeadMsg.append(fmt(HeadFiller    ,   35, 'X'));

		this.HeadMsg  = TmpHeadMsg.toString();
		System.out.println("HeadMsg=["+TmpHeadMsg.toString()+"]");
		return true;
	}

	public boolean EchoMessage(String  EchoMsg)
	{
		this.EchoMsg  = fmt(EchoMsg       ,  500, 'X');
		System.out.println("EchoMsg=["+EchoMsg+"]");
		return true;
	}

	// 신용카드승인요청 Body 1
	public boolean CreditDataMessage(
			String ApprovalType     ,// ApprovalType    : 승인구분
			String InterestType     ,// InterestType    : 일반/무이자구분 1:일반 2:무이자
			String TrackII          ,// TrackII         : 카드번호=유효기간  or 거래번호
			String Installment      ,// Installment     : 할부  00일시불
			String Amount           ,// Amount          : 금액
			String Passwd           ,// Passwd          : 비밀번호 앞2자리
			String IdNum            ,// IdNum           : 주민번호  뒤7자리, 사업자번호10
			String CurrencyType     ,// CurrencyType    : 통화구분 0:원화 1: 미화
			String BatchUseType     ,// BatchUseType    : 거래번호배치사용구분  0:미사용 1:사용
			String CardSendType     ,// CardSendType    : 카드정보전송 0:미정송 1:카드번호,유효기간,할부,금액,가맹점번호 2:카드번호앞14자리 + "XXXX",유효기간,할부,금액,가맹점번호
			String VisaAuthYn       ,// VisaAuthYn      : 비자인증유무 0:사용안함,7:SSL,9:비자인증
			String Domain           ,// Domain          : 도메인 자체가맹점(PG업체용)
			String IpAddr           ,// IpAddr          : IP ADDRESS 자체가맹점(PG업체용)
			String BusinessNumber   ,// BusinessNumber  : 사업자 번호 자체가맹점(PG업체용)
			String Filler           ,// Filler          : 예비
			String AuthType         ,// AuthType        : ISP : ISP거래, MP1, MP2 : MPI거래, SPACE : 일반거래
			String MPIPositionType  ,// MPIPositionType : K : KSNET, R : Remote, C : 제3기관, SPACE : 일반거래
			String MPIReUseType     ,// MPIReUseType    : Y :  재사용, N : 재사용아님
			String EncData          )// EndData         : MPI, ISP 데이터
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType      ,   4, 'X'));
		TmpSendMsg.append(fmt(InterestType      ,   1, 'X'));
		TmpSendMsg.append(fmt(TrackII           ,  40, 'X'));
		TmpSendMsg.append(fmt(Installment       ,   2, '9'));
		TmpSendMsg.append(fmt(Amount            ,   9, '9'));
		TmpSendMsg.append(fmt(Passwd            ,   2, 'X'));
		TmpSendMsg.append(fmt(IdNum             ,  10, 'X'));
		TmpSendMsg.append(fmt(CurrencyType      ,   1, 'X'));
		TmpSendMsg.append(fmt(BatchUseType      ,   1, 'X'));
		TmpSendMsg.append(fmt(CardSendType      ,   1, 'X'));
		TmpSendMsg.append(fmt(VisaAuthYn        ,   1, 'X'));
		TmpSendMsg.append(fmt(Domain            ,  40, 'X'));
		TmpSendMsg.append(fmt(IpAddr            ,  20, 'X'));
		TmpSendMsg.append(fmt(BusinessNumber    ,  10, 'X'));
		TmpSendMsg.append(fmt(Filler            , 135, 'X'));
		TmpSendMsg.append(fmt(AuthType          ,   1, 'X'));
		TmpSendMsg.append(fmt(MPIPositionType   ,   1, 'X'));
		TmpSendMsg.append(fmt(MPIReUseType      ,   1, 'X'));
		TmpSendMsg.append(EncData                           );

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("CreditDataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	// 가상계좌(전문버전 0603 이후)
	public boolean VirtualAccountDataMessage(
			String ApprovalType         ,// 승인구분
			String BankCode             ,// 은행코드
			String Amount               ,// 금액
			String CloseDate            ,// 마감일자
			String CloseTime            ,// 마감시간
			String EscrowSele           ,// 에스크로적용구분: 0:적용안함 1:적용 2:강제적용
			String VirFixSele           ,// 가상계좌번호지정구분
			String VirAcctNo            ,// 가상계좌번호
			String OrgTransactionNo     ,// 원거래거래번호
			String Filler               )// 기타
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType      ,    4, 'X'));
		TmpSendMsg.append(fmt(BankCode          ,    6, 'X'));
		TmpSendMsg.append(fmt(Amount            ,    9, '9'));
		TmpSendMsg.append(fmt(CloseDate         ,    8, 'X'));
		TmpSendMsg.append(fmt(CloseTime         ,    6, 'X'));
		TmpSendMsg.append(fmt(EscrowSele        ,    1, 'X'));
		TmpSendMsg.append(fmt(VirFixSele        ,    1, 'X'));
		TmpSendMsg.append(fmt(VirAcctNo         ,   15, 'X'));
		TmpSendMsg.append(fmt(OrgTransactionNo  ,   12, 'X'));
		TmpSendMsg.append(fmt(Filler            ,   52, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("VirtualAccountDataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	//가상계좌상점변경
	public boolean VAccountShopDataMessage(
			String ApprovalType,        // 승인구분
			String VirAcctNo   ,        // 가상계좌번호
			String Filler      )        // 예비
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType   ,     4, 'X'));
		TmpSendMsg.append(fmt(VirAcctNo      ,    15, 'X'));
		TmpSendMsg.append(fmt(Filler         ,    31, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("VAccountShopDataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	// 월드패스카드
	public boolean WorldPassDataMessage(
			String ApprovalType        ,// 승인구분
			String TrackII             ,// 카드번호=유효기간  or 거래번호
			String Passwd              ,// 비밀번호 앞2자리
			String Amount              ,// 금액
			String WorldPassType       ,// 선후불카드구분
			String AdultType           ,// 성인확인구분
			String CardSendType        ,// 카드정보전송 0:미전송 1:카드번호 유효기간 할부 금액 가맹점번호 2:카드번호앞14자리 + "XXXX" 유효기간 할부 금액 가맹점번호
			String Filler              )// 기타
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType     , 4, 'X'));
		TmpSendMsg.append(fmt((TrackII+"=4912"),40, 'X'));
		TmpSendMsg.append(fmt(Passwd           , 4, 'X'));
		TmpSendMsg.append(fmt(Amount           , 9, '9'));
		TmpSendMsg.append(fmt(WorldPassType    , 1, 'X'));
		TmpSendMsg.append(fmt(AdultType        , 1, 'X'));
		TmpSendMsg.append(fmt(CardSendType     , 1, 'X'));
		TmpSendMsg.append(fmt(Filler           ,40, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("WorldPassDataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	// 포인트카드승인
	public boolean PointDataMessage(
			String ApprovalType         ,// 승인구분
			String TrackII              ,// 카드번호=유효기간  or 거래번호
			String Amount               ,// 금액
			String Passwd               ,// 비밀번호 앞4자리
			String SaleType             ,// 판매구분
			String Filler               )// 기타
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType     , 4, 'X'));
		TmpSendMsg.append(fmt((TrackII+"=4912"),40, 'X'));
		TmpSendMsg.append(fmt(Amount           , 9, '9'));
		TmpSendMsg.append(fmt(Passwd           , 4, 'X'));
		TmpSendMsg.append(fmt(SaleType         , 2, 'X'));
		TmpSendMsg.append(fmt(Filler           ,41, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("PointDataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	// 현금영수증발급
	public boolean CashBillDataMessage(
			String ApprovalType  ,// H000:일반발급, H200:계좌이체, H600:가상계좌
			String TransactionNo ,// 입금완료된 계좌이체, 가상계좌 거래번호
			String IssuSele      ,// 0:일반발급(PG원거래번호 중복체크), 1:단독발급(주문번호 중복체크 : PG원거래 없음), 2:강제발급(중복체크 안함)
			String UserInfoSele  ,// 0:주민등록번호, 1:사업자번호, 2:카드번호, 3:휴대폰번호, 4:기타
			String UserInfo      ,// 주민등록번호, 사업자번호, 카드번호, 휴대폰번호, 기타
			String TranSele      ,// 0: 개인, 1: 사업자
			String CallCode      ,// 통화코드  (0: 원화, 1: 미화)
			String SupplyAmt     ,// 공급가액
			String TaxAmt        ,// 세금
			String SvcAmt        ,// 봉사료
			String TotAmt        ,// 현금영수증 발급금액
			String Filler        )// 예비
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType    ,   4, 'X'));
		TmpSendMsg.append(fmt(TransactionNo   ,  12, 'X'));
		TmpSendMsg.append(fmt(IssuSele        ,   1, 'X'));
		TmpSendMsg.append(fmt(UserInfoSele    ,   1, 'X'));
		TmpSendMsg.append(fmt(UserInfo        ,  37, 'X'));
		TmpSendMsg.append(fmt(TranSele        ,   1, 'X'));
		TmpSendMsg.append(fmt(CallCode        ,   1, 'X'));
		TmpSendMsg.append(fmt(SupplyAmt       ,   9, '9'));
		TmpSendMsg.append(fmt(TaxAmt          ,   9, '9'));
		TmpSendMsg.append(fmt(SvcAmt          ,   9, '9'));
		TmpSendMsg.append(fmt(TotAmt          ,   9, '9'));
		TmpSendMsg.append(fmt(Filler          , 147, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("CashBillDataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	//핸드폰 인증1차
	public boolean Mobile1DataMessage(
			String ApprovalType ,// 구분코드
			String MobileNo     ,// 휴대폰번호
			String SocialNo     ,// 명의자 주민번호
			String UsersocialNo ,// LGT 실사용자 인증시에만 사용
			String Commsele     ,// SKT, KTF, LGT
			String Filler       )// 예비
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType  ,     4, 'X'));
		TmpSendMsg.append(fmt(MobileNo      ,    12, 'X'));
		TmpSendMsg.append(fmt(SocialNo      ,    13, 'X'));
		TmpSendMsg.append(fmt(UsersocialNo  ,    13, 'X'));
		TmpSendMsg.append(fmt(Commsele      ,     3, 'X'));
		TmpSendMsg.append(fmt(Filler        ,   155, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("Mobile1DataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	//핸드폰 인증2차
	public boolean Mobile2DataMessage(
			String ApprovalType     ,// 구분코드
			String TransactionNo    ,// 거래번호
			String Smsval           ,// sms 인증코드
			String Filler           )// 예비
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType  ,     4,  'X'));
		TmpSendMsg.append(fmt(TransactionNo ,    12,  'X'));
		TmpSendMsg.append(fmt(Smsval        ,    20,  'X'));
		TmpSendMsg.append(fmt(Filler        ,   164,  'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("Mobile2DataMessage=["+TmpSendMsg.toString()+"]");

		return true;
	}

	//핸드폰 결제1차
	public boolean MobileAppr1DataMessage(
			String ApprovalType ,// 구분코드
			String MobileNo     ,// 휴대폰번호
			String SocialNo     ,// 명의자 주민번호
			String UsersocialNo ,// LGT 실사용자 인증시에만 사용
			String Commsele     ,// SKT, KTF, LGT
			String ApprAmount   ,// 승인금액
			String BypassMsg    ,// Echo Msg
			String CompSele     ,// 기관코드
			String Filler       )// 예비
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType    ,    4, 'X'));
		TmpSendMsg.append(fmt(MobileNo        ,   12, 'X'));
		TmpSendMsg.append(fmt(SocialNo        ,   13, 'X'));
		TmpSendMsg.append(fmt(UsersocialNo    ,   13, 'X'));
		TmpSendMsg.append(fmt(Commsele        ,    3, 'X'));
		TmpSendMsg.append(fmt(ApprAmount      ,    9, '9'));
		TmpSendMsg.append(fmt(BypassMsg       ,  100, 'X'));
		TmpSendMsg.append(fmt(CompSele        ,    1, 'X'));
		TmpSendMsg.append(fmt(Filler          ,  145, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("MobileAppr1DataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	//핸드폰 결제2차
	public boolean MobileAppr2DataMessage(
			String ApprovalType     ,// 구분코드
			String TransactionNo    ,// 거래번호
			String Smsval           ,// sms 인증코드
			String BypassMsg        ,// BypassMsg
			String Filler           )
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType     ,      4, 'X'));
		TmpSendMsg.append(fmt(TransactionNo    ,     12, 'X'));
		TmpSendMsg.append(fmt(Smsval           ,     20, 'X'));
		TmpSendMsg.append(fmt(BypassMsg        ,     20, 'X'));
		TmpSendMsg.append(fmt(Filler           ,    164, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("Mobile2DataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	// 취소
	public boolean CancelDataMessage(
			String ApprovalType     ,// ApprovalType    : 승인구분
			String CancelType       ,// CancelType      : 취소처리구분 1:거래번호 2:주문번호
			String TransactionNo    ,// TransactionNo   : 거래번호
			String TradeDate        ,// TradeDate       : 거래일자
			String OrderNumber      ,// OrderNumber     : 주문번호
			String Filler           )// Filler)         : 기타
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType      ,  4, 'X'));
		TmpSendMsg.append(fmt(CancelType        ,  1, 'X'));
		TmpSendMsg.append(fmt(TransactionNo     , 12, 'X'));
		TmpSendMsg.append(fmt(TradeDate         ,  8, 'X'));
		TmpSendMsg.append(fmt(OrderNumber       , 50, 'X'));
		TmpSendMsg.append(fmt(Filler            , 75, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("CancelDataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	//카드 BIN check
	public boolean CardBinDataMessage(
			String ApprovalType ,// 승인구분
			String TrackII      ,// 카드번호
			String Filler       )// 기타
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType ,   4, 'X'));
		TmpSendMsg.append(fmt(TrackII      ,  40, 'X'));
		TmpSendMsg.append(fmt(Filler       ,  56, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("CardBinDataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	//상점상세정보 조회  - XMPI 관련 추가
	public boolean ShopInfoDetailDataMessage(
			String ApprovalType ,   //승인구분
			String ShopId       ,   //상점아이디
			String BusiSele     ,   //업무구분
			String CardCode     ,   //카드코드
			String Filler       )   //기타
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType  ,  4, 'X'));
		TmpSendMsg.append(fmt(ShopId        , 10, 'X'));
		TmpSendMsg.append(fmt(BusiSele      ,  1, 'X'));
		TmpSendMsg.append(fmt(CardCode      ,  6, 'X'));
		TmpSendMsg.append(fmt(Filler        , 79, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("ShopInfoDetailDataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	// 계좌이체시작요청전문을 만든다.(send)
	public boolean AcctRequest_send(
			String ApprovalType    ,// 승인구분
			String AcctSele        ,// 계좌이체유형구문
			String FeeSele         ,// 선/후불제구분
			String PareBankCode    ,// 모계좌은행코드
			String PareAcctNo      ,// 모계좌번호
			String CustBankCode    ,// 고객계좌은행코드
			String Amount          ,// 금액
			String InjaName        ,// 인자명(상점명)
			String Filler          )// 기타
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType,        4, 'X'));
		TmpSendMsg.append(fmt(AcctSele    ,        1, 'X'));
		TmpSendMsg.append(fmt(FeeSele     ,        1, 'X'));
		TmpSendMsg.append(fmt(PareBankCode,        6, 'X'));
		TmpSendMsg.append(fmt(PareAcctNo  ,       15, 'X'));
		TmpSendMsg.append(fmt(CustBankCode,        6, 'X'));
		TmpSendMsg.append(fmt(Amount      ,       13, '9'));
		TmpSendMsg.append(fmt(InjaName    ,       16, 'X'));
		TmpSendMsg.append(fmt(Filler      ,       38, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("AcctRequest_send=["+TmpSendMsg.toString()+"]");
		return true;
	}

	//계좌이체 인증승인 요청전문을 만든다.
	public boolean AcctRequest_iappr(
			String ApprovalType         ,// 승인구분 코드
			String AcctSele             ,// 계좌이체 구분 - 1:Dacom, 2:Pop Banking, 3:Scrapping 계좌이체, 4:승인형계좌이체, 5:금결원계좌이체
			String FeeSele              ,// 계좌이체 구분 - 선/후불제구분 - 1:선불, 2:후불
			String TransactionNo        ,// 거래번호
			String BankCode             ,// 입금모계좌코드
			String Amount               ,// 금액    (결제대상금액)
			String CustBankInja         ,// 출금모계좌코드
			String BankTransactionNo    ,// 은행거래번호
			String Filler               ,// 예비
			String CertData             )// 인증정보
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType          ,       4, 'X'));
		TmpSendMsg.append(fmt(AcctSele              ,       1, 'X'));
		TmpSendMsg.append(fmt(FeeSele               ,       1, 'X'));
		TmpSendMsg.append(fmt(TransactionNo         ,      12, 'X'));
		TmpSendMsg.append(fmt(BankCode              ,       6, 'X'));
		TmpSendMsg.append(fmt(Amount                ,      13, '9'));
		TmpSendMsg.append(fmt(CustBankInja          ,      30, 'X'));
		TmpSendMsg.append(fmt(BankTransactionNo     ,      30, 'X'));
		TmpSendMsg.append(fmt(Filler                ,      53, 'X'));

		TmpSendMsg.append(CertData                                  );

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("AcctRequest_iappr=["+TmpSendMsg.toString()+"]");
		return true;
	}

	//계좌인증 요청전문을 만든다.
	public boolean AcctConfirmDataMessage(
			String ApprovalType         ,// 승인구분 코드
			String BankCode             ,// 은행코드
			String AcctNo               ,// 계좌번호
			String Filler               )// 예비
	{
		StringBuffer TmpSendMsg = new StringBuffer();

		TmpSendMsg.append(fmt(ApprovalType         ,       4, 'X'));
		TmpSendMsg.append(fmt(BankCode             ,       6, 'X'));
		TmpSendMsg.append(fmt(AcctNo               ,      20, 'X'));
		TmpSendMsg.append(fmt(Filler               ,      70, 'X'));

		this.SendMsg += TmpSendMsg.toString();
		this.SendCount++;
		System.out.println("AcctConfirmDataMessage=["+TmpSendMsg.toString()+"]");
		return true;
	}

	//승인이후에 결과값을 가지고 온다.
	public boolean ParseMessage(byte[] rmsg) throws IOException
	{
		if (rmsg.length < 304) return false;

		int midx = 0, mlen = 0;

		mlen =  4; String Len         = b2s(rmsg,midx,mlen);midx += mlen;// 데이터 길이
		mlen =  1; this.EncType       = b2s(rmsg,midx,mlen);midx += mlen;// 0: 암화안함, 1:openssl, 2: seed
		mlen =  4; this.Version       = b2s(rmsg,midx,mlen);midx += mlen;// 전문버전
		mlen =  2; this.Type          = b2s(rmsg,midx,mlen);midx += mlen;// 구분
		mlen =  1; this.Resend        = b2s(rmsg,midx,mlen);midx += mlen;// 전송구분 : 0 : 처음,  2: 재전송
		mlen = 14; this.RequestDate   = b2s(rmsg,midx,mlen);midx += mlen;// 요청일자 : yyyymmddhhmmss
		mlen = 10; this.StoreId       = b2s(rmsg,midx,mlen);midx += mlen;// 상점아이디
		mlen = 50; this.OrderNumber   = b2s(rmsg,midx,mlen);midx += mlen;// 주문번호
		mlen = 50; this.UserName      = b2s(rmsg,midx,mlen);midx += mlen;// 주문자명
		mlen = 13; this.IdNum         = b2s(rmsg,midx,mlen);midx += mlen;// 주민번호 or 사업자번호
		mlen = 50; this.Email         = b2s(rmsg,midx,mlen);midx += mlen;// email
		mlen =  1; this.GoodType      = b2s(rmsg,midx,mlen);midx += mlen;// 제품구분 0 : 실물, 1 : 디지털
		mlen = 50; this.GoodName      = b2s(rmsg,midx,mlen);midx += mlen;// 제품명
		mlen =  1; this.KeyInType     = b2s(rmsg,midx,mlen);midx += mlen;// KeyInType 여부 : 1 : Swap, 2: KeyIn
		mlen =  1; this.LineType      = b2s(rmsg,midx,mlen);midx += mlen;// lineType 0 : offline, 1:internet, 2:Mobile
		mlen = 12; this.PhoneNo       = b2s(rmsg,midx,mlen);midx += mlen;// 휴대폰번호
		mlen =  1; this.ApprovalCount = b2s(rmsg,midx,mlen);midx += mlen;// 승인갯수
		mlen = 35; this.HeadFiller    = b2s(rmsg,midx,mlen);midx += mlen;// 예비

		int iCnt = Integer.parseInt(this.ApprovalCount);

		System.out.println("Recv_HeadMsg=["+b2s(rmsg,0  ,300        )+"]");

		if ((byte)'R' == rmsg[midx] && (byte)'E' == rmsg[midx+1])
		{
			mlen = 500; this.RcvEchoMsg    = b2s(rmsg,midx,mlen);midx += mlen;//서브정산내역

			System.out.println("Recv_EchoMsg=["+this.RcvEchoMsg+"]");
		}

		this.ReceiveMsg = b2s(rmsg,midx,rmsg.length-midx);

		System.out.println("Recv_DataMsg=["+this.ReceiveMsg+"]");

		for(int i=0; i < iCnt; i++)
		{
			mlen =  4; this.ApprovalType[i]     = b2s(rmsg,midx,mlen);midx += mlen;// 승인구분

			// 카드 Bin
			if (this.ApprovalType[i].startsWith("15"))
			{
				mlen = 12;this.TransactionNo      [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
				mlen =  1;this.Status             [i] = b2s(rmsg,midx,mlen);midx += mlen;// 상태 O : 승인, X : 거절
				mlen =  8;this.TradeDate          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래일자
				mlen =  6;this.TradeTime          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래시간
				mlen =  6;this.IssCode            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 발급사코드
				mlen = 16;this.Message1           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 메시지1
				mlen = 16;this.Message2           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 메시지2
			}else // 신용카드
				if (this.ApprovalType[i].startsWith("1") || this.ApprovalType[i].startsWith("I")) // 신용카드
				{
					mlen = 12;this.TransactionNo      [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
					mlen =  1;this.Status             [i] = b2s(rmsg,midx,mlen);midx += mlen;// 상태 O : 승인, X : 거절
					mlen =  8;this.TradeDate          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래일자
					mlen =  6;this.TradeTime          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래시간
					mlen =  6;this.IssCode            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 발급사코드
					mlen =  6;this.AquCode            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 매입사코드
					mlen = 12;this.AuthNo             [i] = b2s(rmsg,midx,mlen);midx += mlen;// 승인번호 or 거절시 오류코드
					mlen = 16;this.Message1           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 메시지1
					mlen = 16;this.Message2           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 메시지2
					mlen = 16;this.CardNo             [i] = b2s(rmsg,midx,mlen);midx += mlen;// 카드번호
					mlen =  4;this.ExpDate            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 유효기간
					mlen =  2;this.Installment        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 할부
					mlen =  9;this.Amount             [i] = b2s(rmsg,midx,mlen);midx += mlen;// 금액
					mlen = 15;this.MerchantNo         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 가맹점번호
					mlen =  1;this.AuthSendType       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 전송구분= new String(this.read(2));
					mlen =  1;this.ApprovalSendType   [i] = b2s(rmsg,midx,mlen);midx += mlen;// 전송구분(0 : 거절, 1 : 승인, 2: 원카드)
					mlen = 12;this.Point1             [i] = b2s(rmsg,midx,mlen);midx += mlen;// Point1
					mlen = 12;this.Point2             [i] = b2s(rmsg,midx,mlen);midx += mlen;// Point2
					mlen = 12;this.Point3             [i] = b2s(rmsg,midx,mlen);midx += mlen;// Point3
					mlen = 12;this.Point4             [i] = b2s(rmsg,midx,mlen);midx += mlen;// Point4
					mlen = 12;this.VanTransactionNo   [i] = b2s(rmsg,midx,mlen);midx += mlen;// Point4
					mlen = 82;this.Filler             [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비
					mlen =  1;this.AuthType           [i] = b2s(rmsg,midx,mlen);midx += mlen;// I : ISP거래, M : MPI거래, SPACE : 일반거래
					mlen =  1;this.MPIPositionType    [i] = b2s(rmsg,midx,mlen);midx += mlen;// K : KSNET, R : Remote, C : 제3기관, SPACE : 일반거래
					mlen =  1;this.MPIReUseType       [i] = b2s(rmsg,midx,mlen);midx += mlen;// Y : 재사용, N : 재사용아님

					String EncLen = "";
					if( AuthType[i] == "" || AuthType[i].trim().equals("") )
					{
						this.EncData[i]      = "";
					}else
					{
						mlen =  5                       ;EncLen               = b2s(rmsg,midx,mlen);midx += mlen;
						mlen =  Integer.parseInt(EncLen);this.EncData[i]      = b2s(rmsg,midx,mlen);midx += mlen; // MPI, ISP 데이터
					}
				}else // 포인트카드
					if (this.ApprovalType[i].startsWith("4"))
					{
						mlen = 12;this.PTransactionNo     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
						mlen =  1;this.PStatus            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 상태 O : 승인 , X : 거절
						mlen =  8;this.PTradeDate         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래일자
						mlen =  6;this.PTradeTime         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래시간
						mlen =  6;this.PIssCode           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 발급사코드
						mlen = 12;this.PAuthNo            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 승인번호 or 거절시 오류코드
						mlen = 16;this.PMessage1          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 메시지1
						mlen = 16;this.PMessage2          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 메시지2
						mlen =  9;this.PPoint1            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래포인트
						mlen =  9;this.PPoint2            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 가용포인트
						mlen =  9;this.PPoint3            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 누적포인트
						mlen =  9;this.PPoint4            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 가맹점포인트
						mlen = 15;this.PMerchantNo        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 가맹점번호
						mlen = 40;this.PNotice1           [i] = b2s(rmsg,midx,mlen);midx += mlen;//
						mlen = 40;this.PNotice2           [i] = b2s(rmsg,midx,mlen);midx += mlen;//
						mlen = 40;this.PNotice3           [i] = b2s(rmsg,midx,mlen);midx += mlen;//
						mlen = 40;this.PNotice4           [i] = b2s(rmsg,midx,mlen);midx += mlen;//
						mlen =  8;this.PFiller            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비
					}else // 가상계좌 상점 정보변경
						if (this.ApprovalType[i].startsWith("63"))
						{
							mlen = 12;this.VATransactionNo  [i] = b2s(rmsg,midx,mlen);midx += mlen;
							mlen =  1;this.VAStatus         [i] = b2s(rmsg,midx,mlen);midx += mlen;
							mlen =  8;this.VATradeDate      [i] = b2s(rmsg,midx,mlen);midx += mlen;
							mlen =  6;this.VATradeTime      [i] = b2s(rmsg,midx,mlen);midx += mlen;
							mlen = 16;this.VAMessage1       [i] = b2s(rmsg,midx,mlen);midx += mlen;
							mlen = 16;this.VAMessage2       [i] = b2s(rmsg,midx,mlen);midx += mlen;
							mlen = 15;this.VAVirAcctNo      [i] = b2s(rmsg,midx,mlen);midx += mlen;
							mlen = 30;this.VAName           [i] = b2s(rmsg,midx,mlen);midx += mlen;
							mlen = 42;this.VAFiller         [i] = b2s(rmsg,midx,mlen);midx += mlen;
						}else // 가상계좌
							if (this.ApprovalType[i].startsWith("6"))
							{
								mlen = 12;this.VATransactionNo    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
								mlen =  1;this.VAStatus           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 오류구분 - O:승인 X:거절
								mlen =  8;this.VATradeDate        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 일자(YYYYMMDD)
								mlen =  6;this.VATradeTime        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 시간(HHMMSS)
								mlen =  6;this.VABankCode         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 은행코드
								mlen = 15;this.VAVirAcctNo        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 가상계좌번호
								mlen = 30;this.VAName             [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예금주
								mlen =  8;this.VACloseDate        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 마감일자
								mlen =  6;this.VACloseTime        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 마감시간
								mlen =  4;this.VARespCode         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답코드
								mlen = 16;this.VAMessage1         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message1
								mlen = 16;this.VAMessage2         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message2
								mlen = 36;this.VAFiller           [i] = b2s(rmsg,midx,mlen);midx += mlen;//
							}else // 월드패스
								if (this.ApprovalType[i].startsWith("7"))
								{
									mlen = 12;this.WPTransactionNo    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
									mlen =  1;this.WPStatus           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 오류구분- O:승인 X:거절
									mlen =  8;this.WPTradeDate        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 일자(YYYYMMDD)
									mlen =  6;this.WPTradeTime        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 시간(HHMMSS)
									mlen =  6;this.WPIssCode          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 발급기관코드
									mlen = 12;this.WPAuthNo           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 승인번호 - 오류시 오류코드
									mlen =  9;this.WPBalanceAmount    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 잔액
									mlen =  9;this.WPLimitAmount      [i] = b2s(rmsg,midx,mlen);midx += mlen;// 한도액
									mlen = 16;this.WPMessage1         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message1
									mlen = 16;this.WPMessage2         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message2
									mlen = 16;this.WPCardNo           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 카드번호
									mlen =  9;this.WPAmount           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 금액(승인요청금액)
									mlen = 15;this.WPMerchantNo       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 매입사부여 가맹점번호
									mlen = 11;this.WPFiller           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비
								}else // 계좌이체시작요청
									if (this.ApprovalType[i].startsWith("210")||this.ApprovalType[i].startsWith("240"))
									{
										mlen = 12;this.ACTransactionNo    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
										mlen =  1;this.ACStatus           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 오류구분:- O:승인 X:거절
										mlen =  8;this.ACTradeDate        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 일자(YYYYMMDD)
										mlen =  6;this.ACTradeTime        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 시간(HHMMSS)
										mlen =  1;this.ACAcctSele         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 계좌이체 구분 -   1:Dacom, 2:Pop Banking, 3:실시간계좌이체, 4:X
										mlen =  1;this.ACFeeSele          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 선/후불제구분 -   1:선불, 2:후불
										mlen =  6;this.ACPareBankCode     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 입금모계좌은행코드
										mlen = 15;this.ACPareAcctNo       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 입금모계좌 번호
										mlen =  6;this.ACCustBankCode     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 출급은행코드
										mlen = 13;this.ACAmount           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 금액
										mlen = 16;this.ACInjaName         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 인자명(상점명)
										mlen = 16;this.ACMessage1         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message1
										mlen = 16;this.ACMessage2         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message2
										mlen = 10;this.ACEntrNumb         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 사업자번호
										mlen = 20;this.ACShopPhone        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 전화번호
										mlen = 49;this.ACFiller           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비
									}else // 좌이체결과반영요청 || 계좌이체승인요청 || 계좌이체취소요청
										if (this.ApprovalType[i].startsWith("2"))
										{
											mlen = 12;this.ACTransactionNo    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
											mlen =  1;this.ACStatus           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 오류구분 :승인 X:거절
											mlen =  8;this.ACTradeDate        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 일자(YYYYMMDD)
											mlen =  6;this.ACTradeTime        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 시간(HHMMSS)
											mlen =  1;this.ACAcctSele         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 계좌이체 구분 -  1:Dacom, 2:Pop Banking, 3:실시간계좌이체 4: 승인형계좌이체
											mlen =  1;this.ACFeeSele          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 선/후불제구분 -  1:선불, 2:후불
											mlen = 16;this.ACInjaName         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 인자명(통장인쇄메세지-상점명)
											mlen =  6;this.ACPareBankCode     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 입금모계좌코드
											mlen = 15;this.ACPareAcctNo       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 입금모계좌번호
											mlen =  6;this.ACCustBankCode     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 출금모계좌코드
											mlen = 15;this.ACCustAcctNo       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 출금모계좌번호
											mlen = 13;this.ACAmount           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 금액 (결제대상금액)
											mlen = 30;this.ACBankTransactionNo[i] = b2s(rmsg,midx,mlen);midx += mlen;// 은행거래번호
											mlen = 20;this.ACIpgumNm          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 입금자명
											mlen = 13;this.ACBankFee          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 계좌이체 수수료
											mlen = 13;this.ACBankAmount       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 총결제금액(결제대상금액+ 수수료
											mlen =  4;this.ACBankRespCode     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 오류코드
											mlen = 16;this.ACMessage1         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 오류 message 1
											mlen = 16;this.ACMessage2         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 오류 message 2
											mlen =  1;this.ACCavvSele         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 암호화데이터응답여부
											mlen =183;this.ACFiller           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비

											String EncLen = "";
											this.ACEncData[i] = "";
											if( ACCavvSele[i].equals("1") )
											{
												mlen = 5                       ;EncLen               = b2s(rmsg,midx,mlen);midx += mlen;
												mlen = Integer.parseInt(EncLen);this.ACEncData[i]    = b2s(rmsg,midx,mlen);midx += mlen; // 금결원암호화응답
											}
										}else // 현금영수증
											if (this.ApprovalType[i].startsWith("H"))
											{
												mlen = 12 ;this.HTransactionNo     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
												mlen = 1  ;this.HStatus            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 오류구분 O:정상 X:거절
												mlen = 12 ;this.HCashTransactionNo [i] = b2s(rmsg,midx,mlen);midx += mlen;// 현금영수증 거래번호
												mlen = 1  ;this.HIncomeType        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 0: 소득      1: 비소득
												mlen = 8  ;this.HTradeDate         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 일자
												mlen = 6  ;this.HTradeTime         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 시간
												mlen = 16 ;this.HMessage1          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message1
												mlen = 16 ;this.HMessage2          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message2
												mlen = 20 ;this.HCashMessage1      [i] = b2s(rmsg,midx,mlen);midx += mlen;// 국세청 메시지 1
												mlen = 20 ;this.HCashMessage2      [i] = b2s(rmsg,midx,mlen);midx += mlen;// 국세청 메시지 2
												mlen = 150;this.HFiller            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비
											}else // 핸드폰 인증 1차 : M020
												if (this.ApprovalType[i].startsWith("M02"))
												{
													mlen = 12 ;this.MB1TransactionNo   [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
													mlen = 1  ;this.MB1Status          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 상태 : O, X
													mlen = 8  ;this.MB1TradeDate       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래일자
													mlen = 6  ;this.MB1TradeTime       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래시간
													mlen = 128;this.MB1Serverinfo      [i] = b2s(rmsg,midx,mlen);midx += mlen;// 서버INFO : 업체에서는 보관 필요 없음 예비로
													mlen = 20 ;this.MB1Smsval          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 다날의 경우 space
													mlen = 4  ;this.MB1Stanrespcode    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답코드
													mlen = 200;this.MB1Message         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 에러메시지
													mlen = 117;this.MB1Filler          [i] = b2s(rmsg,midx,mlen);midx += mlen;
												}else // 핸드폰 인증 2차 : M030
													if (this.ApprovalType[i].startsWith("M03"))
													{
														mlen = 12 ;this.MB2TransactionNo   [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
														mlen = 1  ;this.MB2Status          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 상태
														mlen = 8  ;this.MB2TradeDate       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래일자
														mlen = 6  ;this.MB2TradeTime       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래시간
														mlen = 4  ;this.MB2Stanrespcode    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답코드
														mlen = 200;this.MB2Message         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답메시지
														mlen = 115;this.MB2Filler          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비
													}else // 핸드폰 결제 1차 : M120, 핸드폰결제취소
														if (this.ApprovalType[i].startsWith("M12")||this.ApprovalType[i].startsWith("M11"))
														{
															mlen = 12 ;this.MTransactionNo     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
															mlen = 1  ;this.MStatus            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 상태 : O, X
															mlen = 8  ;this.MTradeDate         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래일자
															mlen = 6  ;this.MTradeTime         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래시간
															mlen = 9  ;this.MBalAmount         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 잔액
															mlen = 4  ;this.MRespCode          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답코드
															mlen = 200;this.MRespMsg           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답메시지
															mlen = 100;this.MBypassMsg         [i] = b2s(rmsg,midx,mlen);midx += mlen;// Echo 메시지
															mlen = 6  ;this.MCompCode          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 기관코드
															mlen = 150;this.MFiller            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비
														}else // 핸드폰 결제 2차 : M130
															if (this.ApprovalType[i].startsWith("M13"))
															{
																mlen = 12 ;this.MTransactionNo     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
																mlen = 1  ;this.MStatus            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 상태 : O, X
																mlen = 8  ;this.MTradeDate         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래일자
																mlen = 6  ;this.MTradeTime         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래시간
																mlen = 9  ;this.MBalAmount         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 잔액
																mlen = 20 ;this.MTid               [i] = b2s(rmsg,midx,mlen);midx += mlen;// Tid
																mlen = 4  ;this.MRespCode          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답코드
																mlen = 200;this.MRespMsg           [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답메시지
																mlen = 100;this.MBypassMsg         [i] = b2s(rmsg,midx,mlen);midx += mlen;// Echo 메시지
																mlen = 6  ;this.MCompCode          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 기관코드
																mlen = 150;this.MFiller            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비
															}else // 상점정보조회
																if (this.ApprovalType[i].startsWith("A7"))
																{
																	mlen =  12;this.SITransactionNo   [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
																	mlen =   1;this.SIStatus          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 성공:O, 실패: X
																	mlen =   4;this.SIRespCode        [i] = b2s(rmsg,midx,mlen);midx += mlen;// '0000' : 정상처리
																	mlen =   1;this.SIAgenMembDealSele[i] = b2s(rmsg,midx,mlen);midx += mlen;// 자체대행구분
																	mlen =   1;this.SIStartSele       [i] = b2s(rmsg,midx,mlen);midx += mlen;// 개시여부
																	mlen =  10;this.SIEntrNumb        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 사업자번호
																	mlen =  30;this.SIShopName        [i] = b2s(rmsg,midx,mlen);midx += mlen;// 상점명
																	mlen =  15;this.SIMembNumbGene    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 일반 가맹점번호
																	mlen =  15;this.SIMembNumbNoin    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 무이자 가맹점번호
																	mlen =   2;this.SIAlloMontType    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 할부유형
																	mlen = 200;this.SIAlloMontInfo    [i] = b2s(rmsg,midx,mlen);midx += mlen;// 할부정보
																	mlen =   2;this.SIPayCnt          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 결제수단갯수
																	mlen = 203;this.SIFiller          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비

																	String EncLen = "";
																	this.SIEncData[i] = "";
																	if (this.SIPayCnt[i].trim().length() != 0)
																	{
																		mlen = 5                       ;EncLen               = b2s(rmsg,midx,mlen);midx += mlen;
																		mlen = Integer.parseInt(EncLen);this.SIEncData[i]    = b2s(rmsg,midx,mlen);midx += mlen; // 결제수단별정보
																	}
																}else // 주민번호 성명조회
																	if (this.ApprovalType[i].startsWith("A0"))
																	{
																		mlen = 12;this.NmTransactionNo     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
																		mlen =  1;this.NmStatus            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 오류구분, - O:승인 X:거절
																		mlen =  8;this.NmTradeDate         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 일자(YYYYMMDD)
																		mlen =  6;this.NmTradeTime         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 시간(HHMMSS)
																		mlen = 16;this.NmMessage1          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message1
																		mlen = 16;this.NmMessage2          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message2
																		mlen = 37;this.NmFiller            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비
																	}else // 계좌인증요청
																		if (this.ApprovalType[i].startsWith("A1"))
																		{
																			mlen = 12;this.NmTransactionNo     [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래번호
																			mlen =  1;this.NmStatus            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 오류구분, - O:승인 X:거절
																			mlen =  8;this.NmTradeDate         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 일자(YYYYMMDD)
																			mlen =  6;this.NmTradeTime         [i] = b2s(rmsg,midx,mlen);midx += mlen;// 거래 개시 시간(HHMMSS)
																			mlen = 16;this.NmMessage1          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message1
																			mlen = 16;this.NmMessage2          [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답 message2
																			mlen =  4;this.NmStanrespcode      [i] = b2s(rmsg,midx,mlen);midx += mlen;// 응답코드 - 0000:정상
																			mlen = 22;this.NmName              [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예금주
																			mlen = 11;this.NmFiller            [i] = b2s(rmsg,midx,mlen);midx += mlen;// 예비
																		}else
																		{
																			System.out.println("ERROR: 응답수신 결과점검 오류(정의되지 않은 결제수단:"+this.ApprovalType[i]+")");
																			return false;
																		}
			this.ReceiveCount++;
		}
		return true;
	}

	public boolean SendSocket(String Flag)
	{
		int state_flag ;        /*최종상태플래그*/

		String  pData = (this.HeadMsg+this.SendMsg);
		String  pDataLen = fmt(s2b(pData).length, 4, '9');

		System.out.println(">>>>>>>  SendSocket Start~!! <<<<<<<<");
		System.out.println("SendMessage=["+(pDataLen+pData)+"]");

		state_flag = 9;
		/*
			state_flag = 0 성공
			state_flag = 1 FEP와통신실패
			state_flag = 2 BackUrl쓰기에 실패해 재취소를 하였음.
			state_flag = 3 BackUrl쓰기에 실패해 재취소를 하였으나 취소실패하였음.
			state_flag =
		*/
		try
		{

			if( !this.ProcessRequest(this.IPAddr, this.Port, Flag, pDataLen+this.HeadMsg+this.SendMsg) )    //FEP와 통신실패의경우
			{
				state_flag = 1 ;
			}

		}
		catch(IOException e)
		{
			System.out.println(e.toString());
			System.out.println("승인요청 실패");
			return false;
		}

		/*예외상황(통신실패,BU에러로재취소)*/
		if(state_flag == 1)  /*FEP와통신실패*/
		{
			for(int i = 0; i < this.ReceiveCount; i++)
			{
				Status[i]          = "X";
				Message1[i]        = "KSPAY와통신실패";            // 메시지1
				Message2[i]        = "잠시후재시도";       // 메시지2
				Point1[i]          = "000000000000";
				Point2[i]          = "000000000000";
				Point3[i]          = "000000000000";
				Point4[i]          = "000000000000";

				VAStatus[i]        = "X";
				VAMessage1[i]      = "KSPAY와통신실패";            // 메시지1
				VAMessage2[i]      = "잠시후재시도";       // 메시지2

				WPStatus[i]        = "X";
				WPMessage1[i]      = "KSPAY와통신실패";            // 메시지1
				WPMessage2[i]      = "잠시후재시도";       // 메시지2
				WPAuthNo[i]        = "9999";
				WPBalanceAmount[i] = "000000000";
				WPLimitAmount[i]   = "000000000";

				PStatus[i]         = "X";
				PMessage1[i]       = "KSPAY와통신실패";            // 메시지1
				PMessage2[i]       = "잠시후재시도";       // 메시지2
				PPoint1[i]         = "000000000";
				PPoint2[i]         = "000000000";
				PPoint3[i]         = "000000000";
				PPoint4[i]         = "000000000";

				HStatus[i]         = "X";
				HMessage1[i]       = "KSPAY와통신실패";            // 메시지1
				HMessage2[i]       = "잠시후재시도";       // 메시지2

				MB1Status[i]       = "X";
				MB1Message[i]      = "KSPAY와통신실패";            // 메시지

				MB2Status[i]       = "X";
				MB2Message[i]      = "KSPAY와통신실패";            // 메시지
			}
		}

		return true;
	}


	public boolean ProcessRequest(String addr, int port, String ServiceType, String SendMsg) throws IOException
	{
		boolean ret = false;

		this.KSPaySocket = new KSPayEncSocketBean(addr, port);

		this.KSPaySocket.ConnectSocket();   //IPG_Server와 연결을 맺는다
		byte[] rmsg = this.KSPaySocket.SendNRecv(s2b(SendMsg)); //IPG_Server에 승인/취소요청 데이타를 보낸다.

		ret = ParseMessage(rmsg);

		this.KSPaySocket.CloseSocket();

		return ret;
	}

	public static byte[] s2b(String str)
	{
		byte[] buf = null;
		try
		{
			buf = str.getBytes("ksc5601");
		}catch(Exception e)
		{
			e.printStackTrace();
		}

		return buf;
	}

	public static String b2s(byte[] buf)
	{
		return (null == buf) ? null : b2s(buf,0,buf.length);
	}

	public static String b2s(byte[] buf, int bidx, int blen)
	{
		String str = null;
		try
		{
			str = new String(buf,bidx,blen,"ksc5601");
		}catch(Exception e)
		{
			e.printStackTrace();
		}

		return str;
	}

	public String fmt(String str, int len, char ctype)
	{
		return format(str,len,ctype);
	}

	public String fmt(int no, int len, char ctype)
	{
		return format(String.valueOf(no),len,ctype);
	}

	public String format(String str, int len, char ctype)
	{
		byte[] buff;
		int filllen = 0;

		String          trim_str = null;
		StringBuffer    sb = new StringBuffer();

		buff = (str == null) ? new byte[0] : s2b(str);

		filllen = len - buff.length;
		if (filllen < 0)
		{
			for(int i=0, j=0; j<len-4; i++)//적당히 여유를 두고 잘라버리자
			{
				j += (str.charAt(i) > 127) ? 2 : 1;
				sb.append(str.charAt(i));
			}

			trim_str = sb.toString();
			buff = s2b(trim_str);
			filllen = len - buff.length;

			if (filllen <= 0) return new String(buff, 0, len);//여기는 절대로 안타겠지...
			sb.setLength(0);
		}else
		{
			trim_str = str;
		}

		if(ctype == '9')    // 숫자열인 경우
		{
			for(int i = 0; i<filllen;i++) sb.append('0');
			sb.append(trim_str);
		}else               // 문자열인 경우
		{
			for(int i = 0; i<filllen;i++) sb.append(' ');
			sb.insert(0, trim_str);
		}
		return sb.toString();
	}
}