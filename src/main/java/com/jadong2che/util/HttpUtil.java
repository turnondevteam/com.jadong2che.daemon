package com.jadong2che.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Component
public class HttpUtil {

    public HttpEntity<?> makeHttpEntity(String secertKey, String accessToken, String appType, HashMap<String, Object> params) {

        HttpHeaders requestHeaders = new HttpHeaders();

        if(secertKey != null && !secertKey.equals("")){
            if(accessToken != null && !accessToken.equals("")){
                requestHeaders.set("Authorization", secertKey + " " + accessToken);
            }
        }else {
            if(accessToken != null && !accessToken.equals("")){
                requestHeaders.set("Authorization", accessToken);
            }
        }

        if(appType != null && !appType.equals("")){
            requestHeaders.set("Content-Type", "application/" + appType);
        }


        if(params != null && !params.equals("")) {
            return new HttpEntity<Object>(params, requestHeaders);
        }else{
            return new HttpEntity<Object>(requestHeaders);
        }
    }

    public ResponseEntity getForEntity(String url, HttpEntity request){

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> responseEntity = restTemplate.getForEntity(url, Object.class,request);

        return responseEntity;
    }

    public Object getForObject(String url, HttpEntity request){

        RestTemplate restTemplate = new RestTemplate();
        Object result = restTemplate.getForObject(url, Object.class, request);

        return result;
    }

    public String getForString(String url, HttpEntity request){

        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class, request);

        return result;
    }

    public ResponseEntity postForEntity(String url, HttpEntity request){

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> responseEntity = restTemplate.postForEntity(url, request, Object.class);

        return responseEntity;
    }

    public Object postForObject(String url, HttpEntity request){

        RestTemplate restTemplate = new RestTemplate();
        Object result = restTemplate.postForObject(url, request, Object.class);

        return result;
    }

    public String postForString(String url, HttpEntity request){

        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.postForObject(url, request, String.class);

        return result;
    }

    public ResponseEntity getExchnage(String url, HttpEntity request){

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

        return responseEntity;
    }

    public ResponseEntity postExchange(String url, HttpEntity request){

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

        return responseEntity;
    }




}
