package com.jadong2che.util;

import org.apache.commons.beanutils.Converter;


public class BeanUtilsDateConvertor implements Converter {
	
	private String dateFormat = "yyyy-MM-dd";
	
	public BeanUtilsDateConvertor() {}
	
	public BeanUtilsDateConvertor(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	
	@SuppressWarnings("rawtypes")
	public Object convert(Class arg0, Object arg1) {
		return DateUtil.stringToDate(arg1.toString(), this.dateFormat);
	}

}
