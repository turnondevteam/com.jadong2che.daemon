package com.jadong2che.util;

import java.util.HashMap;

public class PagingUtil {

	private int totalRows = 0;
	private int currentPage = 1;
	private int pageSize = 0;
	private int blockSize = 0;
	private int totalPages;
	private int totalBlocks;
	private int currentBlock;
	private int startPageNum;
	private int endPageNum;
	private int prevPageNum;
	private int nextPageNum;
	private String scriptNme = "goPage";
	private String scriptNmeAjax = "goPageAjax";

	// result temp object
	private StringBuffer pageString = new StringBuffer();

	public PagingUtil() {
		this.pageSize = LocalConstant.PAGINGUTIL_PAGE_SIZE;
		this.blockSize = LocalConstant.PAGINGUTIL_PAGE_BLOCK_SIZE;
		initialize();
	}
	public PagingUtil(int pageSize) {
		this.pageSize = pageSize;
		this.blockSize = LocalConstant.PAGINGUTIL_PAGE_BLOCK_SIZE;
		initialize();
	}
	public PagingUtil(int pageSize,int pageBlockSize) {
		this.pageSize = pageSize;
		this.blockSize = pageBlockSize;
		initialize();
	}

	private void initialize() {
		this.totalPages = (int)Math.ceil((double)this.totalRows / this.pageSize);
		this.totalBlocks = (int)Math.ceil((double)this.totalPages / this.blockSize) + 1;
		this.currentBlock = (int)Math.ceil((double)((this.currentPage - 1) / this.blockSize)) + 1;
		this.startPageNum = (this.currentBlock - 1) * this.blockSize + 1;
		this.endPageNum = (startPageNum + blockSize - 1) > totalPages ? totalPages : (startPageNum + blockSize - 1);
		this.prevPageNum = ((this.currentBlock - 1) * this.pageSize);
		this.nextPageNum = this.prevPageNum + this.pageSize + 1;
	}

	private void prePrint() {
		//pageString.append("<div class=\"pagination\">");

		// 첫페이지가 아닐 경우
		if(this.currentPage > 1)
			pageString.append("<span><a href=\"#\" onclick=\"javascript:"+scriptNme+"(1,'frm'); return false;\"><img src=\"/r/backoffice/image/btn_page_first.gif\" alt=\"처음으로\"></a></span>&nbsp;");
			// 첫페이지일 경우
		else
			pageString.append("<span><img src=\"/r/backoffice/image/btn_page_first.gif\" alt=\"처음으로\"></span>&nbsp;");

		// 이전페이지가 있을 경우
		if(this.currentBlock > 1)
			pageString.append("<span><a href=\"#\" onclick=\"javascript:"+scriptNme+"(" + prevPageNum + ",'frm'); return false;\"><img src=\"/r/backoffice/image/btn_page_pre.gif\" alt=\"이전으로\"></a></span>&nbsp;");
			// 이전페이지가 없을 경우
		else
			pageString.append("<span><img src=\"/r/backoffice/image/btn_page_pre.gif\" alt=\"이전으로\"></span>&nbsp;");
	}

	private void postPrint()
	{
		// 다음페이지가 있을 경우
		if(this.nextPageNum < this.totalPages )
			pageString.append("<span><a href=\"#\" onclick=\"javascript:"+scriptNme+"("+ nextPageNum +",'frm'); return false;\"><img src=\"/r/backoffice/image/btn_page_next.gif\" alt=\"다음으로\"></a></span>&nbsp;");
			// 다음페이지가 없을 경우
		else
			pageString.append("<span><img src=\"/r/backoffice/image/btn_page_next.gif\" alt=\"다음으로\"></span>&nbsp;");

		// 마지막페이지가 아닐 경우
		if(this.currentBlock < this.totalBlocks)
			pageString.append("<span><a href=\"#\" onclick=\"javascript:"+scriptNme+"("+ totalPages +",'frm'); return false;\"><img src=\"/r/backoffice/image/btn_page_last.gif\" alt=\"마지막으로\"></a></span>&nbsp;");
			// 마지막페이지일 경우
		else
			pageString.append("<span><img src=\"/r/backoffice/image/btn_page_last.gif\" alt=\"마지막으로\"></span>&nbsp;");

		//pageString.append("</div>");
	}

	private void printList()
	{
		//pageString.append("<span class=\"pageNum\">");

		for(int i = startPageNum ; i <= endPageNum ; i++)
		{
			if(i == this.currentPage)
				pageString.append("<span class='on'>"+ i +"</span>&nbsp;");
			else
				pageString.append("<span><a href=\"#\" onclick=\""+scriptNme+"(" + i + ",'frm'); return false;\">"+ i +"</a></span>&nbsp;");
		}

		//	pageString.append("</span>");
	}


	@SuppressWarnings("unused")
	private String print()
	{
		if(this.totalPages >= 1)
		{
			this.prePrint();
			this.printList();
			this.postPrint();
		}
		return(pageString.toString());
	}

	public String pageingPrint(int totalRows, HashMap<String,Object> param)
	{
		this.totalRows = totalRows;
		this.currentPage = pageIdxInitialize(param);

		if(param.get("pageSize") != null){
			this.pageSize = Integer.parseInt(param.get("pageSize").toString());
		}

		initialize();

		if(this.totalPages >= 1)
		{
			this.prePrint();
			this.printList();
			this.postPrint();
		}
		return(pageString.toString());
	}
	private void prePrintAjax() {
		//pageString.append("<div class=\"pagination\">");

		// 첫페이지가 아닐 경우
		if(this.currentPage > 1)
			pageString.append("<span><a href=\"javascript:"+scriptNmeAjax+"(1);\"><img src=\"/r/backoffice/image/btn_page_first.gif\" alt=\"처음으로\"></a></span>&nbsp;");
			// 첫페이지일 경우
		else
			pageString.append("<span><img src=\"/r/backoffice/image/btn_page_first.gif\" alt=\"처음으로\"></span>&nbsp;");

		// 이전페이지가 있을 경우
		if(this.currentBlock > 1)
			pageString.append("<span><a href=\"javascript:"+scriptNmeAjax+"(" + prevPageNum + ");\"><img src=\"/r/backoffice/image/btn_page_pre.gif\" alt=\"이전으로\"></a></span>&nbsp;");
			// 이전페이지가 없을 경우
		else
			pageString.append("<span><img src=\"/r/backoffice/image/btn_page_pre.gif\" alt=\"이전으로\"></span>&nbsp;");
	}

	private void postPrintAjax()
	{
		// 다음페이지가 있을 경우
		if(this.nextPageNum < this.totalPages )
			pageString.append("<span><a href=\"javascript:"+scriptNmeAjax+"("+ nextPageNum +");\"><img src=\"/r/backoffice/image/btn_page_next.gif\" alt=\"다음으로\"></a></span>&nbsp;");
			// 다음페이지가 없을 경우
		else
			pageString.append("<span><img src=\"/r/backoffice/image/btn_page_next.gif\" alt=\"다음으로\"></span>&nbsp;");

		// 마지막페이지가 아닐 경우
		if(this.currentBlock < this.totalBlocks)
			pageString.append("<span><a href=\"javascript:"+scriptNmeAjax+"("+ totalPages +");\"><img src=\"/r/backoffice/image/btn_page_last.gif\" alt=\"마지막으로\"></a></span>&nbsp;");
			// 마지막페이지일 경우
		else
			pageString.append("<span><img src=\"/r/backoffice/image/btn_page_last.gif\" alt=\"마지막으로\"></span>&nbsp;");

		//pageString.append("</div>");
	}

	private void printListAjax()
	{
		//pageString.append("<span class=\"pageNum\">");

		for(int i = startPageNum ; i <= endPageNum ; i++)
		{
			if(i == this.currentPage)
				pageString.append("<span class='on'>"+ i +"</span>&nbsp;");
			else
				pageString.append("<span><a href=\"#\" onclick=\""+scriptNmeAjax+"(" + i + "); return false;\">"+ i +"</a></span>&nbsp;");
		}

		//	pageString.append("</span>");
	}

	public String pageingPrintAjax(int totalRows, HashMap<String,Object> param)
	{
		this.totalRows = totalRows;
		this.currentPage = pageIdxInitialize(param);

		if(param.get("pageSize") != null){
			this.pageSize = Integer.parseInt(param.get("pageSize").toString());
		}

		initialize();

		if(this.totalPages >= 1)
		{
			this.prePrintAjax();
			this.printListAjax();
			this.postPrintAjax();
		}
		return(pageString.toString());
	}

	public int pageIdxInitialize(HashMap<String,Object> param){

		int pageIdx = 1;
		
		if(param.get("pageIndex") != null){
			pageIdx = Integer.parseInt(param.get("pageIndex").toString());
		}

		return pageIdx;
	}
	
   public int setOffset(HashMap<String,Object> param) {

	      int offset = 0;
	      int pageSize = 0;
	      
	      if(param.get("pageSize") != null){
	         pageSize = Integer.parseInt(param.get("pageSize").toString());
	      }else{
	         pageSize = this.pageSize;
	      }
	      
	      if(param.get("pageIndex") != null){
	    	
	         offset = (Integer.parseInt(param.get("pageIndex").toString())- 1) * pageSize;
	      }
	            
	      return offset;
   }
   
   public int setPageSize(HashMap<String,Object> param){
		
		int pageSize = 10;
		
		if(param.get("pageSize") != null){
			pageSize = Integer.parseInt(param.get("pageSize").toString());
		}
		
		return pageSize;
	}
}
