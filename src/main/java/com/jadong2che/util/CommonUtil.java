package com.jadong2che.util;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

public class CommonUtil {
	
	/**
	 * Request 객체에서 Param 반환 객체
	 * @param request
	 * @return
	 */
	
	public static String getRqeustParam(HttpServletRequest request, String key) {
		if ( request.getParameter(key) == null || request.getParameter(key) == ""){
			return null;
		} else {
			return request.getParameter(key);
		}
	}
	
	public static String getRqeustParam(HttpServletRequest request, String key, String defaultValue) {
		if ( request.getParameter(key) == null || request.getParameter(key) == ""){
			return defaultValue;
		} else {
			return request.getParameter(key);
		}
	}
	
	public static Timestamp getStartFormatDate(String startDateString){
		startDateString = startDateString + " 00:00:00";
		return Timestamp.valueOf(startDateString);
	}
	
	public static Timestamp getEndFormatDate(String endDateString){
		endDateString = endDateString + " 23:59:59";
		return Timestamp.valueOf(endDateString);
		
		/*endDateString = endDateString + "235959";
	    DateFormat df = new SimpleDateFormat("yyyyMMddHHMMSS");
	    try {
	        return df.parse(endDateString);
	    } catch (ParseException e) {
	        e.printStackTrace();
	        return null;
	    }*/
	}
	
	/**
	 * 1. 개요 : 입력 브라우져 체크
	 * 2. 처리내용 : 
	 * 3. 입력 Data : request
	 * 4. 출력 Data : String
	 * <pre>
	 * @Method Name : getBrowser
	 * </pre>
	 * @author : RedEye
	 * @param request
	 * @return
	 */
	public static String getBrowser(HttpServletRequest request) {
        String header =request.getHeader("User-Agent");
        if (header.contains("MSIE")) {
               return "MSIE";
        } else if(header.contains("Chrome")) {
               return "Chrome";
        } else if(header.contains("Opera")) {
               return "Opera";
        }
        return "Firefox";
	}
}
