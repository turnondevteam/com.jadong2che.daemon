package com.jadong2che.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jadong2che.config.BackofficeConstant;


public class TagUtil {
	
	/**
	 * 전화번호 뒷4자리 별표 출력
	 * @param telNum
	 * @return
	 */
	public static String protectTelNum(String telNum) {
		if(telNum.length() > 7) {
			return telNum.substring(0, 3) + "-" + telNum.substring(3, 7) + "-****";
		}
		
		return telNum;
	}
	
	/**
	 * 전화번호 출력
	 * @param telNum
	 * @return
	 */
	public static String telNum(String telNum) {
		telNum = telNum.replace("-", "");
		if(telNum.length()==9){
			return telNum.substring(0, 2) + "-" + telNum.substring(2, 5) + "-"+ telNum.substring(5, telNum.length());
		}
		else if(telNum.length() == 10) {
			return telNum.substring(0, 3) + "-" + telNum.substring(3, 6) + "-"+ telNum.substring(6,telNum.length());
		}
		else if(telNum.length() == 11){
			return telNum.substring(0, 3) + "-" + telNum.substring(3, 7) + "-"+ telNum.substring(7, telNum.length());
		}
		return telNum;
	}
	
	/**
	 * 성별 출력
	 * @param genderCode
	 * @return
	 */
	public static String selectGender(String genderCode) {
		if(genderCode.equals("1"))	return "남자";
		if(genderCode.equals("2"))	return "여자";
		return genderCode;
	}
	
	/**
	 *연결상태 출력
	 * @param ConnectedCode
	 * @return
	 */
	public static String selectConnected(Character ConnectedCode) {
		if(ConnectedCode.equals('Y'))	return "연결";
		if(ConnectedCode.equals('N'))	return "미연결";
		return ""+ConnectedCode;
	}
	
	/**
	 *지원여부 출력
	 * @param VldCode
	 * @return
	 */
	public static String selectVld(Character VldCode) {
		if(VldCode.equals('Y'))	return "지원";
		if(VldCode.equals('N'))	return "미지원";
		return "" + VldCode;
	}
	
	/**
	 * 호환여부 출력
	 * @param CmptCode
	 * @return
	 */
	public static String selectCmpt(Character CmptCode) {
		if(CmptCode.equals('Y'))	return "호환";
		if(CmptCode.equals('N'))	return "비호환";
		return "" + CmptCode;
	}
	
	/**
	 * 상태여부 출력
	 * @param statCode
	 * @return
	 */
	public static String selectStat(Character StatCode) {
		if(StatCode.equals('Y'))	return "사용중";
		if(StatCode.equals('N'))	return "단종";
		return ""+StatCode;
	}
	
	/**
	 * 날짜 출력
	 * @param date
	 * @return
	 */
	/*
	public static String date(String date){
		if(date.indexOf("-") > 1) {
			return date.substring(0, 10);
		}else if(date.length() > 7){
			return date.substring(0, 4) + "-" + date.substring(4, 6) + "-"+ date.substring(6, date.length());
		}else{
			return date;
		}
	}
	*/
	public static String date(String date){
//		if (date.length() == 8) date = "20"+date;
		if(date.indexOf(".") > 1) {
			return date.substring(0, 4) + "." + date.substring(5, 7) + "."+ date.substring(8, 10);
		}else if(date.indexOf("-") > 1) {
			return date.substring(0, 4) + "." + date.substring(5, 7) + "."+ date.substring(8, 10);
		}else if(date.indexOf("/") > 1) {
			return date.substring(0, 4) + "." + date.substring(5, 7) + "."+ date.substring(8, 10);
		}else if(date.length() > 7){
			return date.substring(0, 4) + "." + date.substring(4, 6) + "."+ date.substring(6, 8);
		}else{
			return date;
		}
	}
	
	public static String datetime(Date date){
		try{
			SimpleDateFormat transFormat = new SimpleDateFormat(BackofficeConstant.DATETIME_FORMAT);	
			return transFormat.format(date);
		}catch(Exception ex){
			return date.toString();
		}
	}
	
	/**
	 * 업데이트 타입 출력
	 * @param updTypeCode
	 * @return
	 */
	public static String selectUpdType(Character updTypeCode) {
		if(updTypeCode.equals('0'))	return "Major";
		if(updTypeCode.equals('1'))	return "Minor";
		if(updTypeCode.equals('2'))	return "Patch";
		return ""+updTypeCode;
	}
	
	/**
	 * 업데이트 구분 출력
	 * @param updTgtTypeCode
	 * @return
	 */
	public static String selectUpdTgtType(Character updTgtTypeCode) {
		if(updTgtTypeCode.equals('0'))	return "강제 업데이트";
		if(updTgtTypeCode.equals('1'))	return "선택 업데이트";
		if(updTgtTypeCode.equals('2'))	return "OTA 버전";
		return ""+updTgtTypeCode;
	}
	
	/**
	 * 버튼 권한 설정
	 * @param selectBtnPowerMsgByOnClick
	 * @return
	 */
	public static String selectBtnPowerMsgByOnClick(String powerMenuCode, String btnCode, String action) {
		// powerMenuCode(4자리) : 읽기(R) + 등록(W) + 수정(E)  + 삭제(D)
		// 읽기 권한
		String errorMsg = "javascript:alert('권한이 없습니다.');";
		//System.out.println("length:"+powerMenuCode.length());
		//System.out.println(btnCode.length() + "&"+btnCode+"&" +powerMenuCode.substring(2,3) );
		if(powerMenuCode.length() != 4) return errorMsg;
		if(btnCode.length() != 1) return errorMsg;
		if(btnCode.equals("R") && !powerMenuCode.substring(0, 1).equals("Y")) return errorMsg;
		else if(btnCode.equals("W") && !powerMenuCode.substring(1, 2).equals("Y")) return errorMsg;
		else if(btnCode.equals("E") && !powerMenuCode.substring(2, 3).equals("Y")) return errorMsg;
		else if(btnCode.equals("D") && !powerMenuCode.substring(3).equals("Y")) return errorMsg;
		return action;
	}
	
	public static String selectBtnPowerMsgByhref(String powerMenuCode, String btnCode, String url) {
		// powerMenuCode(4자리) : 읽기(R) + 등록(W) + 수정(E)  + 삭제(D)
		// 읽기 권한
		String errorMsg = "javascript:alert('권한이 없습니다.');";
		if(powerMenuCode.length() != 4) return errorMsg;
		if(btnCode.length() != 1) return errorMsg;
		if(btnCode.equals("R") && !powerMenuCode.substring(0, 1).equals("Y")) return errorMsg;
		else if(btnCode.equals("W") && !powerMenuCode.substring(1, 2).equals("Y")) return errorMsg;
		else if(btnCode.equals("E") && !powerMenuCode.substring(2, 3).equals("Y")) return errorMsg;
		else if(btnCode.equals("D") && !powerMenuCode.substring(3, 4).equals("Y")) return errorMsg;
		return url;
	}
	
	/**
	 * 우편번호 출력
	 * @param postNum
	 * @return
	 */
	public static String postNum(String postNum) {
		if(postNum.trim().length() == 6) {
			return postNum.substring(0, 3) + "-" + postNum.substring(3, 6) ;
		}
		return postNum;
	}
	
	/**
	 *사업자번호 출력
	 * @param rgnoNum
	 * @return
	 */
	public static String rgnoNum(String rgnoNum) {
		if(rgnoNum.length() == 10) {
			return rgnoNum.substring(0, 3) + "-" + rgnoNum.substring(3, 5)+ "-" + rgnoNum.substring(5, 10) ;
		}
		return rgnoNum;
	}
	
	/**
	 *사업자 종류 출력
	 * @param rgnoNum
	 * @return
	 */
	public static String selectCorp(Character isCorp) {
		if(isCorp.equals('Y'))	return "법인 사업자";
		if(isCorp.equals('N'))	return "개인 사업자";
		return ""+isCorp;
	}
	
	/**
	 *파일명 출력
	 * @param apkFilePth
	 * @return
	 */
	public static String getFileNme(String apkFilePth){
		if(apkFilePth.length()>0)
			return apkFilePth.substring(apkFilePth.lastIndexOf("/")+1,apkFilePth.length());
		return apkFilePth;
	}
	
	/**
	 *사용여부 출력
	 * @param isUse
	 * @return
	 */
	public static String selectUse(Character isUse) {
		if(isUse.equals('Y'))	return "사용";
		if(isUse.equals('N'))	return "미사용";
		return "" + isUse;
	}
	
	/**
	 *기재여부 출력
	 * @param isView
	 * @return
	 */
	public static String selectView(String isView) {
		if(isView.equals("Y"))	return "보임";
		if(isView.equals("N"))	return "안보임";
		return  isView;
	}
	
	/**
	 *사용자 그룹 출력
	 * @param mngrGrpType
	 * @return
	 */
	public static String selectMngrGrpType(String mngrGrpType) {
		if(mngrGrpType.equals("0"))	return "메뉴 그룹";
		if(mngrGrpType.equals("1"))	return "설치소 그룹";
		return  mngrGrpType;
	}
	
	/**
	 * 게재 기간 출력
	 * @param rgnoNum
	 * @return
	 * @throws ParseException 
	 */
	public static String selectDispDtm(String dispBgnDtm, String dispEndDtm) throws ParseException {	
		return date(dispBgnDtm) + " ~ " + date(dispEndDtm);
	}
	
	/**
	 * 노출 상태 출력
	 * @param rgnoNum
	 * @return
	 * @throws ParseException 
	 */
	public static String selectDispYn(String dispYn) throws ParseException {	
		if(dispYn.equals("Y"))	return "노출";
		if(dispYn.equals("N"))	return "노출안함";
		return dispYn;
	}
	
	/**
	 *N버튼 사용 여부 출력
	 * @param rgnoNum
	 * @return
	 */
	public static String selectIsBrdNtf(String isBrdNtf) {
		if(isBrdNtf.trim().equals("Y"))	return "사용";
		if(isBrdNtf.trim().equals("N"))	return "미사용";
		return isBrdNtf;
	}
	
	/**
	 * 컨텍츠 발행여부
	 * @param isVld
	 * @return
	 */
	public static String selectIssuYn(String isVld) {
		if(isVld.trim().equals("Y"))	return "발행가능";
		if(isVld.trim().equals("N"))	return "발행불가능";
		return isVld;
	}
	
	/**
	 * 버스타입
	 * @param busType
	 * @return
	 */
	
	public static String getBusType(String busType) {
		if(busType.equals("0"))	return "일반버스";
		if(busType.equals("1"))	return "저상버스";
		if(busType.equals("2"))	return "굴절버스";
		if(busType.equals("3"))	return "심야버스";
		return busType;
	}

}
