package com.jadong2che.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.Converter;

public class BeanUtilsListConvertor implements Converter {
	
	public BeanUtilsListConvertor() {}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object convert(Class arg0, Object arg1) {
		List arrayList = new ArrayList();
		if(arg1 instanceof List){
			arrayList.addAll((List)arg1);
		}else{
			arrayList.add(arg1);
		}
		return arrayList;
	}

}
