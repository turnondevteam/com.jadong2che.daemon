package com.jadong2che.util;

import com.jadong2che.common.domain.AuthMember;
import org.springframework.security.core.context.SecurityContextHolder;

public class SessionUtil {
	
	private static AuthMember authMember;

	public static AuthMember getAuthMember() {
		if(SecurityContextHolder.getContext().getAuthentication() != null) {
			authMember = (AuthMember) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}
		return authMember;
	}
	
}
