package com.jadong2che.util;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Component
public class ExcelUtil{

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <T> String excelDownLoad(String gbn, String fileName, String sheetName, String header, List<T> list) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		String resultPath = "/home/deploy/resource/download/excel/";
//		String resultPath = "/Users/jm.shin/Documents/99.Temp/excel/";
		if(fileName.equals(null)){
			resultPath = resultPath + gbn + "_" + System.currentTimeMillis() + ".xlsx";
		}else{
			resultPath = resultPath + fileName + ".xlsx";
		}

		if(header == null || header.equals("") || header.indexOf(",") < 1)
			return null;
		
		String[] head = header.split(",");
		
		// Create a new Workbook
		XSSFWorkbook workbook = new XSSFWorkbook();

		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet(sheetName);
		
		int count = 0;
		Row row = sheet.createRow(count++);
		
		// Header 생성
		for(int i = 0 ; i < head.length ; i++){
			row.createCell(i).setCellValue((String) head[i]);
		}
		
		if(list.size() == 0){
			row = sheet.createRow(count++);
			row.createCell(0).setCellValue((String) "정보가 존재하지 않습니다.");
		}
		
		// Data 생성
		for(Object data : list){
			row = sheet.createRow(count++);

			Field[] fields = data.getClass().getDeclaredFields();
			
			int fieldLength = fields.length;

			if(data.getClass().getName().equals("java.util.HashMap")){
				fieldLength = head.length;
			}
			if(data.getClass().getName().equals("java.util.LinkedHashMap")){
				fieldLength = head.length;
			}

			for (int i = 0; i < fieldLength; i++) {
				
				if(data.getClass().getName().equals("java.util.HashMap") || data.getClass().getName().equals("java.util.LinkedHashMap") ){


					HashMap map = (HashMap) data;

					Iterator<String> keys = map.keySet().iterator();

					int j=0;

			        while( keys.hasNext() ){

						String key = keys.next();
						String value = String.valueOf(map.get(key));

						if(value == null){
							value = " ";
						}

						row.createCell(j).setCellValue(value);

						j++;

			        }
					
				}else{
					Method method = data.getClass().getMethod("get" + fields[i].getName().substring(0, 1).toUpperCase() + fields[i].getName().substring(1));
					
					if(method.getReturnType().getName().equals("java.lang.String"))
						row.createCell(i).setCellValue((String)method.invoke(data));
					else if(method.getReturnType().getName().equals("int") || method.getReturnType().getName().equals("java.lang.Integer"))
						row.createCell(i).setCellValue((int)method.invoke(data));
					else
						row.createCell(i).setCellValue((String)method.invoke(data));
				}
			}
		}
		
		//Save the excel sheet

		File file = new File(resultPath);
		FileOutputStream fos = null;

		try{

	        fos = new FileOutputStream(file);
	        workbook.write(fos);
	        fos.close();

	    }catch(IOException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
			try {
                if(workbook!=null) workbook.close();
                if(fos!=null) fos.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

		}
		
		return resultPath;
	}
	
	public static ArrayList doExcelUpload(HttpServletRequest request, HttpServletResponse response, int startRow) throws Exception{
		ArrayList exceldataArr = new ArrayList();
		
		try{
			final MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			final Map<String, MultipartFile> files = multiRequest.getFileMap();
			
			Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();

			boolean isXml = false;
			MultipartFile file;
			InputStream is = null;

			if (itr.hasNext()) {
				Entry<String, MultipartFile> entry = itr.next();
				file = entry.getValue();
				
				if(file.getOriginalFilename().indexOf("xlsx") > -1 || file.getContentType().indexOf("xml") > -1) isXml = true;
				is = file.getInputStream();
			}
			
			Workbook wb = null;
			
			if(isXml){
				wb = new XSSFWorkbook(is);
				
			}else{
				wb = new HSSFWorkbook(is);
			}

			is.close();
			
			exceldataArr = readExcel(wb, startRow);    
			
		}catch(Exception e){
			System.out.println(e.toString());
			throw e;
		}
		return exceldataArr;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static ArrayList readExcel(Workbook wb, int startrow) throws Exception{
		ArrayList arrBeans = new ArrayList();
		HashMap data = null;

		try{
			int idx = 0;
			int startIdx = 1;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

			for (int i = 0; i < wb.getNumberOfSheets(); i++) {
				Sheet sheet = wb.getSheetAt(i);
			    
				int rowNum = sheet.getPhysicalNumberOfRows();

				for(int r=0; r<rowNum; r++ ){

					if(startIdx++ < startrow)continue;
					
					idx = 0;
					data = new HashMap();
					
					Row row = sheet.getRow(r);
					if(row != null){
						//도중에 빈값이 있을경우 row 셀갯수 에서 빈값 만큼 뺀 수를 리턴 
						
						//값이 비어있지 않는 가장 끝 셀 위치
						int cellNum = row.getLastCellNum();
						
						for(int c=0; c<cellNum; c++){
							Cell cell = row.getCell(c);
							
							if(cell != null){
								if(cell.getCellType() == 0){
									if (HSSFDateUtil.isCellDateFormatted(cell)){
										data.put((idx++)+"", sdf.format(cell.getDateCellValue()));
									}else{
										data.put((idx++)+"",  (long)cell.getNumericCellValue());
									}
								}else if(cell.getCellType() == 3){
									data.put((idx++)+"", "");
								}else{
									data.put((idx++)+"", cell.getStringCellValue());
								}
							} else {
								data.put((idx++)+"", "");
							}
						}
					}else {
						break;
					}
					arrBeans.add(data);
				}
			}
			return arrBeans;
		}catch(Exception e){
			throw e;
		}
	}

	public static <T> String csvDownload(String gbn, String fileName, String sheetName, String columnNames, List<T> list) throws Exception {


		String returnFileName = "";
		String savePath = "/home/deploy/resource/download/csv/";
//		String savePath = "/Users/jm.shin/Documents/99.Temp/csv/";
		String returnPath = "";

		if(fileName.equals(null)){
			returnFileName = gbn + "_" + System.currentTimeMillis() + ".csv";
		}else{
			returnFileName = fileName + ".csv";
		}

		returnPath = "/home/deploy/resource/download/csv/" + returnFileName;
//		returnPath = "/Users/jm.shin/Documents/99.Temp/csv/" + returnFileName;

		FileDownLoadUtil.makeDir(savePath);

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(returnPath), "MS949"));

		writer.write(columnNames + "\r\n");

		for (int i = 0; i < list.size(); i++) {

			HashMap map = (HashMap) list.get(i);

			Iterator<String> keys = map.keySet().iterator();

			int j=0;
			String value = "";

			while( keys.hasNext() ){

				String key = keys.next();

				if(j == 0){
					value += String.valueOf(map.get(key));
				}else{
					value += "," + String.valueOf(map.get(key));
				}

				j++;
			}

			writer.write(value + "\r\n");
		}

		writer.close();

		return returnPath;

	}

	public static ArrayList csvUpload(HttpServletRequest request) {

		ArrayList list = new ArrayList();

		try{

			final MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			final Map<String, MultipartFile> files = multiRequest.getFileMap();

			Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();

			MultipartFile file;
			InputStream is = null;

			if (itr.hasNext()) {
				Entry<String, MultipartFile> entry = itr.next();
				file = entry.getValue();

				is = file.getInputStream();
			}

			String result = null;
			ByteArrayOutputStream baos = null;

			baos = new ByteArrayOutputStream();
			byte[] byteBuffer = new byte[1024];
			byte[] byteData = null;
			int nLength = 0;

			while((nLength = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
				baos.write(byteBuffer, 0, nLength);
			}

			byteData = baos.toByteArray();

			result = new String(byteData,"ms949");


			String tempArray[] = result.split("\r\n");

			for(int i=0; i<tempArray.length; i++){
				list.add(tempArray[i]);
			}


		} catch (IOException e) {
			e.printStackTrace();
		}

		return list;
	}


}

