package com.jadong2che.util;
/*
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import kr.co.ipopcorn.backoffice.acommon.dto.SessionUserVO;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
*/
public class MenuUtil {
/*	
	*//**
	 * 메뉴코드 구하기
	 * @param request
	 * @return
	 *//*
	public static String getMenuCode(HttpServletRequest request) {
		return request.getServletPath().replaceAll(".do", "").replaceAll("/", "");
	}

	*//**
	 * 1Depth 메뉴
	 * @param sessionUserVO
	 * @return
	 *//*
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<HashMap> getMenuDepth1(SessionUserVO sessionUserVO, String menuCode) {
		JSONObject menuList_json = sessionUserVO.getMenuList();
		
		List<HashMap> depthMenuList = new ArrayList<HashMap>();
		
		try {
			JSONArray fullMenuList = (JSONArray) menuList_json.get("menuList");
			
			for(int i=0; i<fullMenuList.length(); i++) {
				JSONObject menuObj = (JSONObject) fullMenuList.get(i);
				if(menuObj.get("menuLvl").equals("1")) {
					HashMap menuMap = new ObjectMapper().readValue(menuObj.toString(), HashMap.class);									
					menuMap.put("iconClass", MenuUtil.getIconClass(menuMap.get("menuCde").toString()));
					// 선택표시 추가
					if(menuObj.get("menuCde").toString().substring(0, 1).equals(menuCode.substring(0, 1))) {
						menuMap.put("active", "active");
					}
					depthMenuList.add(menuMap);
				}
			}
		} catch(JSONException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		return depthMenuList;
	}
	
	*//**
	 * 2Depth 메뉴
	 * @param sessionUserVO
	 * @return
	 *//*
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<HashMap> getMenuDepth2(SessionUserVO sessionUserVO, String menuCode) {
		JSONObject menuList_json = sessionUserVO.getMenuList();
		
		List<HashMap> depthMenuList = new ArrayList<HashMap>();
		
		try {
			JSONArray fullMenuList = (JSONArray) menuList_json.get("menuList");
			
			for(int i=0; i<fullMenuList.length(); i++) {
				JSONObject menuObj = (JSONObject) fullMenuList.get(i);
				if(menuObj.get("menuLvl").equals("2")) {
					String depth1Code = menuObj.get("menuCde").toString().substring(0, 1);
					// FIMXE : 고쳐야 합니다.
					String currentPageDepth1Code = menuCode.substring(0, 1);
					
					if(currentPageDepth1Code.equals(depth1Code)) {
						HashMap menuMap = new ObjectMapper().readValue(menuObj.toString(), HashMap.class);
						depthMenuList.add(menuMap);
						menuMap.put("iconClass", MenuUtil.getIconClass(menuMap.get("menuCde").toString()));
						
						// 선택 추가
						if(menuObj.get("menuCde").toString().substring(0, 3).equals(menuCode.substring(0, 3))) {
							menuMap.put("active", "active");
						}
					}
				}
			}
		} catch(JSONException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		return depthMenuList;
	}

	*//**
	 * 메뉴 아이콘 CSS Class 값 구하기
	 * @param menuCode
	 * @return
	 *//*
	public static String getIconClass(String menuCode) {
		String iconClass = "";
		
		switch (menuCode) {
			case "C020000":
				iconClass = "icon-phone-square";
				break;
			default:
				iconClass = "icon-th-large";
				break;
			}
		
		return iconClass; 
	}
	
	*//**
	 * 
	 * @param sessionUserVO
	 * @param menuCode
	 * @return
	 *//*
	public static String[] getTopNavi(SessionUserVO sessionUserVO, String menuCode) {
		String[] topNaviArr = null;
		JSONObject menuList_json = sessionUserVO.getMenuList();
		
		try {
			JSONArray fullMenuList = (JSONArray) menuList_json.get("menuList");
			for(int i=0; i<fullMenuList.length(); i++) {
				JSONObject menuObj = (JSONObject) fullMenuList.get(i);
				if(menuObj.get("menuCde").toString().contains(menuCode)) {
					String menuPath = menuObj.get("menuPth").toString().substring(1);
					topNaviArr = menuPath.split("/");
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return topNaviArr;
	}
	
	*//**
	 * 
	 * @param sessionUserVO
	 * @param menuCode
	 * @return
	 *//*
	public static String getRigtCode(SessionUserVO sessionUserVO, String menuCode) {
		String powerCode = "";
		JSONObject menuList_json = sessionUserVO.getMenuList();
		
		try {
			JSONArray fullMenuList = (JSONArray) menuList_json.get("menuList");
			for(int i=0; i<fullMenuList.length(); i++) {
				JSONObject menuObj = (JSONObject) fullMenuList.get(i);
				if(menuObj.get("menuCde").toString().contains(menuCode)) {
					powerCode = menuObj.get("rigt").toString();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return powerCode;
	}
*/	
}
