package com.jadong2che.util;

public class LocalConstant {

	public static final int PAGINGUTIL_PAGE_SIZE = 10;
	
	public static final int PAGINGUTIL_PAGE_BLOCK_SIZE = 10;
	
	public static final String DATE_FORMAT = "yyyy.MM.dd";
	
	public static final String DATETIME_FORMAT = "yyyy.MM.dd hh:mm:ss";

}