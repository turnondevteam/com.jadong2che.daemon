package com.jadong2che.util.push;

public class PushResult {

	// 결과 코드
	//private String resultCode;
	
	// 결과 메시지
	private String resultMsg;
	
	//성공여부
	private String isSuccess;
	
	//pushToken
	private String pushToken;
	
	public PushResult(String isSuccess, String pushToken,String resultMsg) {
		super();
		
		this.isSuccess = isSuccess;
		this.pushToken = pushToken;
		this.resultMsg = resultMsg;
	}
	
	
	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}



	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getPushToken() {
		return pushToken;
	}

	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}
	
	
}
