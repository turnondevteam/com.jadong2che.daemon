package com.jadong2che.util.push;

import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;

public class FcmResult {
	@JsonIgnore
	private String groupId;
	
	private long multicast_id;
	private int success;
	private int failure;
	private int canonical_ids;
	
	private List<Map<String, String>> results;
	
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public void setMulticast_id(long multicast_id) {
		this.multicast_id = multicast_id;
	}
	public void setSuccess(int success) {
		this.success = success;
	}
	public void setFailure(int failure) {
		this.failure = failure;
	}
	public void setCanonical_ids(int canonical_ids) {
		this.canonical_ids = canonical_ids;
	}
	public void setResults(List<Map<String, String>> results) {
		this.results = results;
	}
	
	public String getGroupId() {
		return groupId;
	}
	public int getCanonical_ids() {
		return canonical_ids;
	}
	public int getFailure() {
		return failure;
	}
	public int getSuccess() {
		return success;
	}
	public long getMulticast_id() {
		return multicast_id;
	}
	public List<Map<String, String>> getResults() {
		return results;
	}
	
	
	public String getSuccessString() {
		
		if(success == 1)
			return "Y";
		else
			return "N";
	}
	
	public String getMessageDetail() {
		if(results != null && results.size() > 0) {
			if(success == 1) {
				return results.get(0).get("message_id");
			} else {
				return results.get(0).get("error");	
			}
		}
		return "";
	}
}
