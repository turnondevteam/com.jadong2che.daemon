package com.jadong2che.util.push;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import com.jadong2che.util.push.PushResult;

public class GcmSender {
	
	private Sender sender;
	
	public GcmSender(String gcmKey) {
		sender = new Sender(gcmKey); 
	}

	public PushResult send(Map<String, String> pushData, String pushId) throws Exception {
		Message message = createMessage(pushData);
		Result result = sender.send(message, pushId, 0);
		return getResult(result);
	}

	public List<PushResult> send(Map<String, String> pushData, List<String> pushIds) throws Exception {
		
		Message message = createMessage(pushData);
		MulticastResult mResult = sender.send(message, pushIds, 0);
		
		List<PushResult> pushResultList = null;
		
		if(mResult.getTotal() > 0) {
			pushResultList = new ArrayList<PushResult>();

			List<Result> resultList = mResult.getResults();
			for(Result result : resultList) {
				PushResult pushResult = getResult(result);
				pushResultList.add(pushResult);
			}
		}
		
		return pushResultList;
	}
	
	private Message createMessage(Map<String, String> pushData) {
		return new Message.Builder()
				.collapseKey(String.valueOf(Math.random() % 100 + 1))
				.addData("title", pushData.get("title"))				
				.addData("pushContents", pushData.get("contents"))	
				//.addData("publishOpt", pushData.get(""))
				//.addData("imgType", pushData.get(""))
				//.addData("imgUrl", pushData.get(""))
				//.addData("pushMsgType", pushData.get(""))
				//.addData("statusBarImgType", pushData.get(""))
				//.addData("statusBarImgUrl", pushData.get(""))
				//.addData("smryText", pushData.get(""))
				//.addData("trgtUrl", pushData.get(""))
				.addData("badge", "0")
				.delayWhileIdle(false)
				.timeToLive(3600)
				.build();
	}
	
	private PushResult getResult(Result result) {
		PushResult pushResult = null;
		
//		if(result.getErrorCodeName() == null) 	pushResult = new PushResult("SUCCESS", null);	
//		else									pushResult = new PushResult("FAIL", result.getErrorCodeName());	
		
		return pushResult;
	}
	
}

