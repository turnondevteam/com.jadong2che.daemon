package com.jadong2che.util.push;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class FcmSender2 {
	
	private static String fcmKey = "";
	private static String fcmUrl = "";
	
	public FcmSender2(String key , String url) {
		fcmKey = key;
		fcmUrl = url;
	}
	
	public int send2(String pushInfo) throws Exception {
		HttpsURLConnection con = null;
		URL obj = new URL(fcmUrl);
		con = (HttpsURLConnection) obj.openConnection();
		
		con.setRequestMethod("POST");
		
		// Set POST headers
		con.setRequestProperty("Authorization", "key="+fcmKey);
		con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
		
		// Send POST body
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));
		System.out.println("pushInfo" + pushInfo);
		System.out.println("pushInfo" + pushInfo.toString());
		writer.write(pushInfo);
		writer.close();
		wr.close();

		wr.flush();
		wr.close();
		
		int resultCode = con.getResponseCode();
		con.disconnect();
		System.out.println("code = "+con.getResponseCode());
		return resultCode;
	}
}
