package com.jadong2che.util.push;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;

import org.codehaus.jackson.JsonParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class FcmSender {
	
	private static final Logger logger = LoggerFactory.getLogger(FcmSender.class);
	
	private static String fcmKey = "";
	private static String fcmUrl = "";
	final static private String FCM_URL = "https://fcm.googleapis.com/fcm/send";


	public FcmSender(String key , String url) {
		fcmKey = key;
		fcmUrl = url;
	}
	
	/**
	 * Json 형식을 개별 발송.
	 * @param pushInfo
	 * @return
	 * @throws Exception
	 */
	public FcmResult send(String pushInfo, String groupId) throws Exception {
		HttpsURLConnection con = null;
		URL obj = new URL(fcmUrl);
		con = (HttpsURLConnection) obj.openConnection();
		
		con.setRequestMethod("POST");
		
		// Set POST headers
		con.setRequestProperty("Authorization", "key="+fcmKey);
		con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
		
		// Send POST body
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));
		System.out.println("pushInfo" + pushInfo);
		System.out.println("pushInfo" + pushInfo.toString());
		writer.write(pushInfo);
		writer.close();
		wr.close();

		wr.flush();
		wr.close();
		
		InputStream           is   = null;
		ByteArrayOutputStream baos = null;
		String response = null;
		FcmResult fcmResult = null;
		
		int responseCode = con.getResponseCode();
		if(responseCode == HttpsURLConnection.HTTP_OK) {
			
			is = con.getInputStream();
			baos = new ByteArrayOutputStream();
			byte[] byteBuffer = new byte[1024];
			byte[] byteData = null;
			int nLength = 0;
			while((nLength = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
				baos.write(byteBuffer, 0, nLength);
			}
			byteData = baos.toByteArray();

			try {
				ObjectMapper mapper = new ObjectMapper();
				response = new String(byteData);
				fcmResult = mapper.readValue(response, FcmResult.class);
				fcmResult.setGroupId(groupId);
				
			} catch (JsonParseException e) {
				logger.info("error : "+e.toString());
			} catch (JsonMappingException e) {
				logger.info("error : "+e.toString());
			} catch (IOException e) {
				logger.info("error : "+e.toString());
			}
		}

		con.disconnect();		
		return fcmResult;
	}


	public void testFCM(String[] args) {

		//Just I am passed dummy information
		// DeviceID's

		List<String> putIds;
		String tokenId1 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
		String tokenId  = "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy";
		String server_key ="<Server Key>" ;
		String message = "Welcome alankit Push Service.";

		putIds= new ArrayList<>();
		putIds.add(tokenId1);
		putIds.add(tokenId);

		// for Group

		send_FCM_NotificationMulti(putIds,tokenId,server_key,message);

		//for indevidual

		send_FCM_Notification( tokenId,server_key,message);
	}

	public void send_FCM_Notification(String tokenId, String server_key, String message){


		try{
			// Create URL instance.
			URL url = new URL(FCM_URL);
			// create connection.
			HttpURLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			//set method as POST or GET
			conn.setRequestMethod("POST");
			//pass FCM server key
			conn.setRequestProperty("Authorization","key="+server_key);
			//Specify Message Format
			conn.setRequestProperty("Content-Type","application/json");
			//Create JSON Object & pass value
			JSONObject infoJson = new JSONObject();

			infoJson.put("title","Alankit");
			infoJson.put("body", message);

			JSONObject json = new JSONObject();
			json.put("to",tokenId.trim());
			json.put("notification", infoJson);

			System.out.println("json :" +json.toString());
			System.out.println("infoJson :" +infoJson.toString());

			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();
			int status = 0;
			if( null != conn ){
				status = conn.getResponseCode();
			}
			if( status != 0){

				if( status == 200 ){
					//SUCCESS message
					BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					System.out.println("Android Notification Response : " + reader.readLine());

				}else if(status == 401){
					//client side error
					System.out.println("Notification Response : TokenId : " + tokenId + " Error occurred :");
				}else if(status == 501){
					//server side error
					System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
				}else if( status == 503){
					//server side error
					System.out.println("Notification Response : FCM Service is Unavailable  TokenId : " + tokenId);
				}
			}
		}catch(MalformedURLException mlfexception){
			// Prototcal Error
			System.out.println("Error occurred while sending push Notification!.." + mlfexception.getMessage());
		}catch(Exception mlfexception){
			//URL problem
			System.out.println("Reading URL, Error occurred while sending push Notification!.." + mlfexception.getMessage());
		}

	}

	public void send_FCM_NotificationMulti(List<String> putIds2, String tokenId, String server_key, String message){
		try{
			// Create URL instance.
			URL url = new URL(FCM_URL);
			// create connection.
			HttpURLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			//set method as POST or GET
			conn.setRequestMethod("POST");
			//pass FCM server key
			conn.setRequestProperty("Authorization","key="+server_key);
			//Specify Message Format
			conn.setRequestProperty("Content-Type","application/json");
			//Create JSON Object & pass value

			JSONArray regId = null;
			JSONObject objData = null;
			JSONObject data = null;
			JSONObject notif = null;

			regId = new JSONArray();
			for (int i = 0; i < putIds2.size(); i++) {
				regId.add(putIds2.get(i));
			}
			data = new JSONObject();
			data.put("message", message);
			notif = new JSONObject();
			notif.put("title", "Alankit Universe");
			notif.put("text", message);

			objData = new JSONObject();
			objData.put("registration_ids", regId);
			objData.put("data", data);
			objData.put("notification", notif);
			System.out.println("!_@rj@_group_PASS:>"+ objData.toString());


			System.out.println("json :" +objData.toString());
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

			wr.write(objData.toString());
			wr.flush();
			int status = 0;
			if( null != conn ){
				status = conn.getResponseCode();
			}
			if( status != 0){

				if( status == 200 ){
					//SUCCESS message
					BufferedReader reader = new BufferedReader(new
							InputStreamReader(conn.getInputStream()));
					System.out.println("Android Notification Response : " +
							reader.readLine());
				}else if(status == 401){
					//client side error
					System.out.println("Notification Response : TokenId : " + tokenId + "Error occurred :");
				}else if(status == 501){
					//server side error
					System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : " + tokenId);
				}else if( status == 503){
					//server side error
					System.out.println("Notification Response : FCM Service is Unavailable TokenId : " + tokenId);
				}
			}
		}catch(MalformedURLException mlfexception){
			// Prototcal Error
			System.out.println("Error occurred while sending push Notification!.." +
					mlfexception.getMessage());
		}catch(IOException mlfexception){
			//URL problem
			System.out.println("Reading URL, Error occurred while sending push Notification!.." + mlfexception.getMessage());
		}catch (Exception exception) {
			//General Error or exception.
			System.out.println("Error occurred while sending push Notification!.." +
					exception.getMessage());
		}

	}


}
