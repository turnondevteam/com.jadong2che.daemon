package com.jadong2che.util.push;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import javapns.devices.Device;
import javapns.devices.implementations.basic.BasicDevice;
import javapns.notification.AppleNotificationServer;
import javapns.notification.AppleNotificationServerBasicImpl;
import javapns.notification.PushNotificationManager;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.PushedNotifications;

import com.jadong2che.util.push.PushResult;

public class ApnsSender {
	
	public PushNotificationManager pushManager = null;
	
	public ApnsSender() {
	}

	public ApnsSender(String p12Path, String p12Passwd) {
		 try {
             InputStream in = new FileInputStream(p12Path);
             AppleNotificationServer server = new AppleNotificationServerBasicImpl(in, p12Passwd, true);
             pushManager = new PushNotificationManager();
             pushManager.initializeConnection(server);
         } catch (Exception e) {
             pushManager = null;
             e.printStackTrace();
         }
	}
	
	public List<PushResult> sendIOSPush(List<String> receiverIdList, Map<String, String> pushData) {
        int result = 0;
        try {
            PushNotificationPayload payload = PushNotificationPayload.complex();
 
            //payload.addAlert(msg);
            //payload.addBadge(1);
            //payload.getPayload().put("customMsg", "customMsg");
            
        	payload.addAlert(pushData.get("title"));
    		payload.addAlert(pushData.get("contents"));
//    		payload.addCustomAlertBody(pushData.get("contents"));
            payload.addCustomDictionary("custom1", "1");
            payload.addCustomDictionary("custom2", 2);
            payload.addBadge(1);
 
            List<Device> deviceList = new ArrayList<Device>(receiverIdList.size());
            for (String receiverId : receiverIdList) {
                Device device = new BasicDevice();
                device.setToken(receiverId);
                deviceList.add(device);
            }
 
            PushedNotifications notifications = pushManager.sendNotifications(payload, deviceList);
            if (notifications != null && notifications.getSuccessfulNotifications() != null) {
            	return getResult(notifications);
                //result = notifications.getFailedNotifications().size();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<PushResult>();
    }
	
	private List<PushResult> getResult(PushedNotifications pushedNotifications) {
		List<PushResult> result = null;
		
		if(!pushedNotifications.isEmpty()) {
			result = new ArrayList<PushResult>();
			PushResult pushApnsResult = null;
			
			for(PushedNotification notification : pushedNotifications) {				
//				String resMsg = notification.getResponse().getMessage();
//				
				if(notification.isSuccessful())	pushApnsResult = new PushResult("Y", notification.getDevice().getToken(), "");	// 성공
				else 							pushApnsResult = new PushResult("N", notification.getDevice().getToken(), "");	// 실패
//				
				result.add(pushApnsResult);
			}
		}
		
		return result;
	}
}
