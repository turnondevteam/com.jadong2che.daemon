package com.jadong2che.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

import com.jadong2che.common.service.CommonService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;

public class PushUtil {

    // Method to send Notifications from server to client end.

    @Autowired
    private static CommonService commonService;

    public final static String AUTH_KEY_FCM = "AAAA4XxnkjY:APA91bGBLFxBz63oJte8kMZmJxk3_oPEmivcyAwfNmLOerD4_upibrxLiIBEa5dfNJhYeTH3oMWVsWRVeEGzskh0G5CRuG56c84PnX1U0MaxOA3WNH78Op1rgEIXGtVCT91ybZOxt8Jp";
//    public final static String AUTH_KEY_FCM = "AIzaSyCHQVOG9vgTR3ejG80UWEJ_hGIhTJWhWbY";
    public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    // userDeviceIdKey is the device id you will query from your database

    public static HashMap<String,Object> pushFCMNotification(String userDeviceIdKey, String title, String contents, String messageCode, String pushIndex, String userIndex, String scheme, String osType, String pushTitle) throws Exception {
//    public static HashMap<String,Object> pushFCMNotification(String userDeviceIdKey, String title, String contents, String messageCode, String pushIndex, String userIndex, String scheme, String osType) throws Exception {
        String authKey = AUTH_KEY_FCM; // You FCM AUTH key
        String FMCurl = API_URL_FCM;

        URL url = new URL(FMCurl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization", "key=" + authKey);
        conn.setRequestProperty("Content-Type", "application/json");

        String appUrl = "jadong2che://push/web?";
        String baseUrl = "https://jadong2che-fd5a7.firebaseapp.com/";

        String tempUrl = "";

        /*jadong2che://push/web?title=타이틀&url=https://jadong2che-fd5a7.firebaseapp.com/reading/info?selfmeterDetailIndex=25*/
        /*appUrl = jadong2che://push/web?title=%EC%9E%90%EB%8F%99%EC%9D%B4%EC%B2%B4&url=https%3A%2F%2Fjadong2che-fd5a7.firebaseapp.com%2Fapplication%2Fdetail%3FrequestIndex%3D1169*/

        if(pushTitle != null && !pushTitle.equals("")){
            if(scheme !=null && !scheme.equals("")){
                tempUrl="title=" + URLEncoder.encode(pushTitle,"UTF-8") + "&url=" + URLEncoder.encode(baseUrl + scheme,"UTF-8");
            }else{
                tempUrl="title=" + URLEncoder.encode(pushTitle,"UTF-8");
            }

        }else{
            if(scheme !=null && !scheme.equals("")){
                tempUrl="url=" + URLEncoder.encode(baseUrl + scheme,"UTF-8");
            }
        }

        System.out.println("=================================================================================================================");
        System.out.println("tempUrl = " + tempUrl);
        System.out.println("=================================================================================================================");

        appUrl = appUrl + tempUrl;

        System.out.println("osType = " + osType);

        JSONObject json = new JSONObject();
        JSONObject infoA = new JSONObject();

        infoA.put("title", title);
        infoA.put("body", contents);

        if(tempUrl != null && !tempUrl.equals("")){
            infoA.put("scheme",appUrl);
        }

        System.out.println("=================================================================================================================");
        System.out.println("appUrl = " + appUrl);
        System.out.println("=================================================================================================================");

        json.put("notification", infoA);
        json.put("data", infoA);


        json.put("to", userDeviceIdKey.trim()); // deviceID
        json.put("click_action", "OPEN_ACTIVITY");

        try(OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream())){
        //try(OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8")){ 인코딩을 변경해준다.

            wr.write(json.toString());
            wr.flush();

        }catch(Exception e){
        }

        if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

        String output;

        System.out.println("Output from Server .... \n");

        HashMap<String,Object> result = new HashMap<String, Object>();

        while ((output = br.readLine()) != null) {
            System.out.println(output);

            	JSONParser parser = new JSONParser();
				Object obj = parser.parse(output);
				JSONObject jsonObj = (JSONObject) obj;

                result.put("success",jsonObj.get("success"));
        }

        conn.disconnect();

        return result;
    }
}