package com.jadong2che;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "local")
public class PasswordEncoderTest {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Test
	public void test() {
		System.out.println(passwordEncoder.encode("000000"));
	}
	
}
